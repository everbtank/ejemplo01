
@extends('base')
@section('title', 'Curso
 estudiante - Instituto Itcenco')
@section('body')
<main id="main" >
    <article class="section-profile-module">
        <div class="container">
            <div class="content-dashboard">
                <div class="menu-sidebar">
                    <div class="box-profile">
                        <figure>
                            <img src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}" alt="{{Auth::user()->name}}">
                        </figure>
                        <p class="text-name">{{Auth::user()->name}}</p>
                        <div class="button-change-profile">
                            <button>
                                <img src="assets/img/course/plus.svg" alt="icon plus">
                            </button>
                        </div>
                    </div>
                    <div class="menu-list-dashboard">
                        <ul>
                            <li>
                                <a href="perfil" class="link active">Perfil</a>
                            </li>
                            <li>
                                <a href="aulavirtual1" class="link">Aula virtual</a>
                            </li>
                            <li>
                                <a href="certificado" class="link">Certificados</a>
                            </li>
                            <li>
                                <a href="pagos" class="link">Pagos</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-body">
                    <div class="content-course-list">
                        <div class="header-list">
                            <h3 class="dashboard-title">Cursos</h3>
                        </div>
                        <div class="body-profile">
                         @foreach($classrooms as $classroom)
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{asset($classroom['classroom']['photo_url'])}}" alt="course">
                                </figure>
                                <div class="info-course">
                                    <div class="text-information">
                                        <h3>{{$classroom['classroom']['title']}}</h3>
                                        <p>ESPECIALIZACION</p>
                                    </div>
                                    <div class="icon-information">
                                        <div class="left-icon">
                                            <img src="assets//img/course/users.svg" alt="">
                                            <p>120</p>
                                        </div>
                                        <p class="date-text">Inicio: {{$classroom['classroom']['class_start']}}</p>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>
@endsection