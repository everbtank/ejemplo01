@extends('base')
@section('title', 'Perfil estudiante - Instituto Itcenco')
@section('body')
<main id="main" >
    <article class="section-profile-module">
        <div class="container">
            <div class="content-dashboard">
                <div class="menu-sidebar">
                    <div class="box-profile">
                        <figure>
                            <img src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}" alt="">
                        </figure>
                        <p class="text-name">{{Auth::user()->name}}</p>
                        <div class="button-change-profile">
                            <Upload max-height="25%" max-widht="50%"></Upload>
                            <img src="assets/img/course/plus.svg" alt="icon plus">
                        </div>
                    </div>
                    <div class="menu-list-dashboard">
                        <ul>
                            <li>
                                <a href="perfil" class="link active">Perfil</a>
                            </li>
                            <li>
                                <a href="aulavirtual1" class="link">Aula virtual</a>
                            </li>
                            <li>
                                <a href="certificado" class="link">Certificados</a>
                            </li>
                            <li>
                                <a href="pagos" class="link">Pagos</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-body">
                    <div class="content-form-data">
                        <div class="header-profile">
                            <h3 class="dashboard-title">Mis datos</h3>
                            <p>Actualiza tus datos para poder valiadar su informacion</p>
                        </div>
                        <div class="form-profile">
                            <form method="POST" action="{{route('user-profile-information.update')}}">
                                <div class="form-group">
                                    <label for="name">Nombre completo</label>
                                    <input class="input-control" value="{{Auth::user()->name}}" type="text" id="name" placeholder="jhon doe" name="name">
                                    
                                    @error('name', 'updateProfileInformation')
                                    <span class="input-text-error">* {{$message}}</span>
                                    @enderror

                                </div>
                                <div class="form-group">
                                    <label for="dni">DNI</label>
                                    <input class="input-control" value="{{Auth::user()->identify}}" type="number" placeholder="928378273" id="dni" name="dni">
                                      
                                    @error('identify', 'updateProfileInformation')
                                    <span class="input-text-error">* {{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo</label>
                                    <input class="input-control"  value="{{Auth::user()->email}}" type="email" id="email" name="email" placeholder="usuario@gmail.com">
                                    
                                    @error('email', 'updateProfileInformation')
                                        <span class="input-text-error">* {{$message}}</span>
                                    @enderror
                                </div>
                                <!--
                                <div class="form-group">
                                    <label for="phone">Celular</label>
                                    <input class="input-control" type="number" name="phone" id="phone" placeholder="+51 9983783783">
                                </div>-->
                                <div class="flex flex-wrap -mx-3 mb-6">
                                    <div class="w-full px-3">
                                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="avatar">
                                        Cambiar Avatar
                                    </label>
                                    <Upload max-height="25%" max-widht="50%"></Upload>
                                    @error('image')
                                    <p class="text-red-500 text-xs italic">{{$message}}</p>
                                    @enderror
                                    </div>
                                </div>
                                <div class="form-group-btn">
                                    <button class="btn btn-secondary animate-scale" type="submit">Guardar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>
@endsection