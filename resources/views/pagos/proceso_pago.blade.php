@extends('base')
@section('title', 'Certificados - Itcenco')
@section('body')
<main id="main" >
        <section class="container-small">
            <div class="contain-payment-process">
                <div class="content-progress-bar" id="progress-payment">
                    <div class="step">
                        <div class="number ">
                            <span>1</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">Pago</p>
                    </div> 
                    <div class="step">
                        <div class="number">
                            <span>2</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">Confirmar</p>
                    </div>
                    <div class="step">
                        <div class="number">
                            <span>3</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">!Realizado¡</p>
                    </div>
                </div>
                <div class="container-body-progress">
                    <div class="slide-content">
                        <div class="page page-payment">
                            <div class="content-page-process">
                                <div class="grid-left">
                                    
                                </div>
                                <div class="grid-rigth">
                                    
                                </div>
                            </div>
                            <div class="section-button">
                                <button class="btn btn-secondary py-8 px-16" id="return-address-page">Anterior</button>
                                <button class="btn btn-primary py-8 px-16" id="chose-confirmation">Siguiente</button>
                            </div>
                        </div>
                        
                        
                        <div class="page page-confirmation">
                            <div class="content-page-process">
                                <div class="grid-left">
                                    
                                </div>
                                <div class="grid-rigth">
                                    
                                </div>
                            </div>
                            <div class="section-button">
                                <button class="btn btn-secondary py-8 px-16" id="return-payment-page">Anterior</button>
                                <button class="btn btn-primary py-8 px-16" id="chose-success">Siguiente</button>
                            </div>
                        </div>
                        
                        <div class="page page-success">
    
                            <div class="contain-success">
                                <figure>
                                    <img src="{{ asset('assets/img/payment_check.png')}}" alt="check success">
                                </figure>
                                <p class="text-success">Compra realizada exitosamente</p>
                                <p class="text-thanks">Gracias por tu compra, pronto recibiás un correo de confirmación.</p>
                                <button class="btn btn-primary" id="button-finaly">Regresar a la tienda</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section> 
    </main>
@endsection
