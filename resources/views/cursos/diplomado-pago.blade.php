@extends('base')
@section('title', 'Certificados - Itcenco')
@section('body')
<main id="main" >
        <section class="container-small">
            <div class="contain-payment-process">
                <div class="content-progress-bar" id="progress-payment">
                    <div class="step">
                        <div class="number ">
                            <span>1</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">Pago</p>
                    </div> 
                    <div class="step">
                        <div class="number">
                            <span>2</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">Confirmar</p>
                    </div>
                    <div class="step">
                        <div class="number">
                            <span>3</span>
                        </div>
                        <div class="icono d-none">
                            <i class="las la-check"></i>
                        </div>
                        <p class="text-step">!Realizado¡</p>
                    </div>
                </div>
                <div class="container-body-progress">
                    <div class="slide-content">
                        <div class="page page-payment">
                            <div class="content-page-process">
                                <div class="grid-left">
                                    
                                </div>
                                <div class="grid-rigth">
                                    
                                </div>
                            </div>
                            <section>
                    <form  method="POST" action="{{route('diplomado-suscription')}}" enctype="multipart/form-data">
                    @csrf  
                    {{--@method('PUT')--}}
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="student_name" id="student_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Documento:</label>
                        <div class="input-group">
                            <input type="number"   class="input-control" name="student_dni" id="student_dni">
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                       <div class="form-group">
                        <label for="descripcion">Curso:</label>
                        <div class="input-group">
                            <input type="email"  placeholder="@correo" class="input-control" name="course_name" id="course_name">
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Especializacion:</label>
                        <div class="input-group">
                            <input type="text"  value="" class="input-control" name="course_name" id="course_name">
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Celular:</label>
                            <div class="input-group">
                                <input type="text"  class="input-control" name="celular" id="celular">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">País:</label>
                            <div class="input-group">
                                <input type="text"  class="input-control"  name="course_time" id="course_time">
                            </div>
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input  type="file" name="imagen" id="imagen" class="input-control" >
                        </div>
                    </div>
                  
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
                </section>
                <section>
                <h2>Inscripción del Diplomado</h2>
                <span>Para incribirte al diplomado necesitas haber realizado el pago correspondiente, luego puedes adjuntar el recibo del pago en el formulario</span>
          
                 <div class="section-button">
                       <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="hover:outline-none outline-none">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="itcencoeduperu@gmail.com">
                            <input type="hidden" name="lc" value="US">
                            <input type="hidden" name="item_name" value="Diplomado">
                            <input type="hidden" name="item_number" value="diplomados">
                            <input type="number" class="input-control" name="amount"  min="1" max="1000" step="any" value="197.00" required="true">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type="hidden" name="button_subtype" value="services">
                            <input type="hidden" name="no_note" value="0">
                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
                            <button  type="submit" class="btn btn-primary py-8 px-16" id="chose-confirmation">Payu</button>
                            
                        </form>
                      
                    </div>
                </section>
                <!--<section>
                    <div class="section-button">
                        <button class="btn btn-secondary py-8 px-16" id="return-address-page">Anterior</button>
                        <button class="btn btn-primary py-8 px-16" id="chose-confirmation">Payu</button>
                    </div>
                </section>-->
                </div>
                        
                        
                        <div class="page page-confirmation">
                            <div class="content-page-process">
                                <div class="grid-left">
                                    
                                </div>
                                <div class="grid-rigth">
                                    
                                </div>
                            </div>
                            <div class="section-button">
                                <button class="btn btn-secondary py-8 px-16" id="return-payment-page">Anterior</button>
                                <button class="btn btn-primary py-8 px-16" id="chose-success">Siguiente</button>
                            </div>
                        </div>
                        
                        <div class="page page-success">
    
                            <div class="contain-success">
                                <figure>
                                    <img src="{{ asset('assets/img/payment_check.png')}}" alt="check success">
                                </figure>
                                <p class="text-success">Compra realizada exitosamente</p>
                                <p class="text-thanks">Gracias por tu compra, pronto recibiás un correo de confirmación.</p>
                                <button class="btn btn-primary" id="button-finaly">Regresar a la tienda</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
        
    </main>
@endsection
