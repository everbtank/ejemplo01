@extends('base')
@section('title', $infocursos['title'])
@section('body')
<main id="main" >
        <section class="content-diplomat-info">
            <div class="container">
                <div class="section-diplomat">
                    <section class="product__header">
                        <div class="levels">
                            <div class="level__item">
                                <a href="{{ route('home') }}" class="text">Inicio</a>
                                <img src="{{ asset('assets/img/arrow_menu.png')}}" alt="Arrow left">
                            </div>
                            <div class="level__item">
                                <a href="../cursos" class="text">Cursos</a>
                                <img src="{{ asset('assets/img/arrow_menu.png')}}" alt="Arrow left">
                            </div>
                            <div class="level__item">
                                <a href="#" class="text">{{ $infocursos['title']}}</a>
                            </div>
                        </div>
                    </section>
                    
<!--                    <section class="prinpcipal-content-info">
                        <div class="content-img">
                            <figure>
                                <img src="{{asset($infocursos['photo_url'])}}" alt="{{asset($infocursos['title'])}}">
                            </figure>
                        </div>
                        <div class="extra-info">
                            <h2 class="title&#45;&#45;principal-info">
                                Elaboración de expedientes técnicos en obra publica con la metodología BIM
                            </h2>
                            <div class="count-student">
                                <div class="row-item">
                                    <figure class="icon-student">
                                        <img src="{{ asset('assets/img/info/users.png')}}" alt="">
                                    </figure>
                                    <p class="text-student">12 Estudiantes</p>
                                </div>
                                <div class="row-item">
                                    <figure class="icon-student">
                                        <img src="{{ asset('assets/img/info/check.png')}}" alt="">
                                    </figure>
                                    <p class="text-student">Calificacion: 5.0</p>
                                    <i class="las la-star"></i>
                                </div>
                            </div>
                            <div class="price-and-bottom">
                                <div class="text-price">

                                    <p>
                                        Suscribete para acceder a este y otros 80 cursos especializados en arquitectura, diseño y construcción creados por profesionales de todo el mundo.
                                    </p>
                                </div>
                                @auth
                                   <div class="button-subscribe">
                                    <a href="{{route('diplomado-suscription')}}" class="btn btn-primary">
                                        <span>Accede desde solo {{$infocursos['precio']}} $ USD</span>
                                    </a>
                                </div>
                                @endauth 
                                @guest
                                   <div class="button-subscribe">
                                    <a href="{{route('diplomado-suscription')}}" class="btn btn-primary">
                                        Accede desde solo {{$infocursos['precio']}} $ USD
                                    </a>
                                </div>
                                @endguest
                                
                            </div>

                        </div>
                    </section>-->
                    <section class="second-section-info-course">
                        <div class="left-content-sections">
                            <div class="content-img">
                                <figure>
                                    <img src="{{asset($infocursos['photo_url'])}}" alt="{{asset($infocursos['title'])}}">
                                </figure>
                                <div class="container-float-header-card">
                                    <a href="" class="discount">Inicio 18 abril 2021</a>
                                </div>
                            </div>

                            <div class="extra-info show-mobile">
                                <h2 class="title--principal-info">
                                    Elaboración de expedientes técnicos en obra publica con la metodología BIM
                                </h2>
                                <div class="count-student">
                                    <div class="row-item">
                                        <figure class="icon-student">
                                            <img src="{{ asset('assets/img/info/users.png')}}" alt="">
                                        </figure>
                                        <p class="text-student">12 Estudiantes</p>
                                    </div>
                                    <div class="row-item">
                                        <figure class="icon-student">
                                            <img src="{{ asset('assets/img/info/check.png')}}" alt="">
                                        </figure>
                                        <p class="text-student">Calificacion: 5.0</p>
                                        <i class="las la-star"></i>
                                    </div>
                                </div>
                                <div class="price-and-bottom">
                                    <div class="text-price">

                                        <p>
                                            Suscribete para acceder a este y otros 80 cursos especializados en arquitectura, diseño y construcción creados por profesionales de todo el mundo.
                                        </p>
                                    </div>
                                    @auth
                                        <div class="button-subscribe">
                                            <a href="../carrito" class="btn btn-primary">
                                                <span>Accede desde solo {{$infocursos['precio']}} $ USD</span>
                                            </a>
                                        </div>
                                    @endauth
                                    @guest
                                        <div class="button-subscribe">
                                            <a href="../registro" class="btn btn-primary">
                                                Accede desde solo {{$infocursos['precio']}} $ USD
                                            </a>
                                        </div>
                                    @endguest

                                </div>

                            </div>

                            <div class="second-content-only-text">
                                <h3>{{$infocursos['title']}}</h3>
                                <p>
                                   {{$infocursos['description']}}
                                </p>
                            </div>
                            <div class="contentido-diploma">
                                <h2>Contenido del Diplomado</h2>
                                <div class="list-items-diploma">
                                    <ul class="list-items-dropdown" id="list_content_theme_course">
                                        <?php
                                        $i = 1;
                                        ?>
                                        @foreach($productos_cursos as  $content)
                                            <li class="list-content">
                                                <a href="#" class="link-active">
                                                    <figure class="icon-init">
                                                        <img src="{{ asset('assets/img/info/icon_contenido_diploma.png')}}" alt="">
                                                    </figure>
                                                    <span>{{$i++}}. {{ $content['titulo']}}</span>
                                                    <figure class="icon-arrow">
                                                        <img src="{{ asset('assets/img/arrow_menu.png')}}" alt="">
                                                    </figure>
                                                </a>
                                                <ul class="list-this-link">
                                                    <li class="line-option">
                                                        <a href="#" class="link-course">
                                                            <strong>Clase 1:</strong>
                                                            <span>Filosofía del Diseño Sismorresistente </span>
                                                            <i class="las la-lock"></i>
                                                        </a>
                                                    </li>
                                                    <li class="line-option">
                                                        <a href="#" class="link-course">
                                                            <strong>Clase 2:</strong>
                                                            <span>Filosofía del Diseño Sismorresistente </span>
                                                            <i class="las la-lock"></i>
                                                        </a>
                                                    </li>
                                                    <li class="line-option">
                                                        <a href="#" class="link-course">
                                                            <strong>Clase 3:</strong>
                                                            <span>Filosofía del Diseño Sismorresistente </span>
                                                            <i class="las la-lock"></i>
                                                        </a>
                                                    </li>
                                                    <li class="line-option">
                                                        <a href="#" class="link-course">
                                                            <strong>Clase 4:</strong>
                                                            <span>Filosofía del Diseño Sismorresistente </span>
                                                            <i class="las la-lock"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="questions-frecuently show-mobile">
                                <h2>Preguntas Frecuentes</h2>
                                <div class="list-items-diploma">
                                    <ul class="list-items-dropdown" id="list_content_theme_course">
                                        <li class="list-content">
                                            <a href="#" class="link-active">
                                                <span>¿Cuál es el proceso de compra?</span>
                                                <figure class="icon-arrow plus">
                                                    <img class="icon-plus" src="../img/plus.svg" alt="">
                                                </figure>
                                                <figure class="icon-arrow minus d-none">
                                                    <img class="icon-minus" src="../img/minus.svg" alt="">
                                                </figure>
                                            </a>
                                            <ul class="list-this-link">
                                                <li class="line-option">
                                                    <span>Primero, deberás realizar el pago del curso que deseas adquirir a través del método de pago de tu elección, después de ello tendrás acceso al curso correspondiente</span>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-content">
                                            <a href="#" class="link-active">
                                                <span>¿Todos los cursos incluyen certificado?</span>
                                                <figure class="icon-arrow plus">
                                                    <img class="icon-plus" src="../img/plus.svg" alt="">
                                                </figure>
                                                <figure class="icon-arrow minus d-none">
                                                    <img class="icon-minus" src="../img/minus.svg" alt="">
                                                </figure>
                                            </a>
                                            <ul class="list-this-link">
                                                <li class="line-option">
                                                    <span>No todos los cursos incluyen certificado, sólo los cursos PREMIUM y algunos de los cursos gratuitos. </span>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="first-right-conent show-mobile">
                                <div class="top-text-only">
                                    <h2 class="maybe-title">Certificación </h2>
                                    <p>
                                        Una vez finalizado satisfactoriamente el diplomado “Elaboración de expediente en obras publicas el la metodología BIM", el Instituto Itcenco y el Colegio de Ingenieros del Perú emite las diplomas, el cual contiene:
                                    </p>
                                </div>
                                <div class="list-check">
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Datos personales del alumno</p>
                                    </div>
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Fotografía actual.</p>
                                    </div>
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Plan de estudios</p>
                                    </div>
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>420 horas lectivas invertidas </p>
                                    </div>
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Nota final.</p>
                                    </div>
                                    <div class="item-line-text">
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Rúbrica de las instituciones competentes.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="teachers-from-course">
                                <h2 class="title-teacher">Docentes de la especialización</h2>
                                <div class="item-teacher">
                                    <figure class="image-profile-teacher">
                                        <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                    </figure>
                                    <div class="info-teacher-rigrht">
                                        <h3>Guliana nicho gomez</h3>
                                        <p>BIM Manager Certificada de la Universidad Peruana de
                                            Ciencias Aplicadas.
                                            Egresada de la Carrera de Ingeniería Civil.
                                           Arquitecta UNPRG – 07 años de experiencia en Gestión
                                           Pública.
                                           Especialista BIM Manager y BIM Management UPC Per</p>
                                    </div>
                                </div>
                                <!--<div class="item-teacher">
                                    <figure class="image-profile-teacher">
                                        <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                    </figure>
                                    <div class="info-teacher-rigrht">
                                        <h3>Guliana nicho gomez</h3>
                                        <p>BIM Manager Certificada de la Universidad Peruana de
                                            Ciencias Aplicadas.
                                            Egresada de la Carrera de Ingeniería Civil.
                                           Arquitecta UNPRG – 07 años de experiencia en Gestión
                                           Pública.
                                           Especialista BIM Manager y BIM Management UPC Per</p>
                                    </div>
                                </div>
                                <div class="item-teacher">
                                    <figure class="image-profile-teacher">
                                        <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                    </figure>
                                    <div class="info-teacher-rigrht">
                                        <h3>Guliana nicho gomez</h3>
                                        <p>BIM Manager Certificada de la Universidad Peruana de
                                            Ciencias Aplicadas.
                                            Egresada de la Carrera de Ingeniería Civil.
                                           Arquitecta UNPRG – 07 años de experiencia en Gestión
                                           Pública.
                                           Especialista BIM Manager y BIM Management UPC Per</p>
                                    </div>
                                </div>-->
                            </div>
                             <div class="first-conent-box">
                                <h2 class="top-text-only">Te brindamos </h2>
                                <div class="container-card-item">
                                    <div class="item-box">
                                        <figure class="left-image">
                                            <img src="{{ asset('assets/img/course/icon01.png')}}" alt="">
                                        </figure>
                                        <div class="right-text">
                                            <h3>Seguridad</h3>
                                            <p>Garantizamos la vigencia de sus diplomas
                                                o certificados a nivel nacional e
                                                internacional.</p>
                                        </div>
                                    </div>
                                    <div class="item-box">
                                        <figure class="left-image">
                                            <img src="{{ asset('assets/img/course/icon02.png')}}" alt="">
                                        </figure>
                                        <div class="right-text">
                                            <h3>Rendimiento</h3>
                                            <p>Nuestros cursos estan diseñados
                                                para que puedas obtener el mejor
                                                rendimiento y logres tus metas.</p>
                                        </div>
                                    </div>
                                    <div class="item-box">
                                        <figure class="left-image">
                                            <img src="{{ asset('assets/img/course/icon03.png')}}" alt="">
                                        </figure>
                                        <div class="right-text">
                                            <h3>Alumnos</h3>
                                            <p>Nuestros ex alumnos te recomiendan</p>
                                        </div>
                                    </div>
                                    <div class="item-box">
                                        <figure class="left-image">
                                            <img src="{{ asset('assets/img/course/icon04.png')}}" alt="">
                                        </figure>
                                        <div class="right-text">
                                            <h3>Logros</h3>
                                            <p>Cumple tus metas con nosotros, te ayudaremos a cumplirlos.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section class="section-calificacion">
                                <h2 class="text--ratings-title">Calificación del diplomado</h2>
                                <div class="ratings-box">
                                    <div class="big-number">
                                        <p class="number-rating">5.0</p>
                                        <div class="start-total-rating">
                                            <i class="las la-star"></i>
                                            <i class="las la-star"></i>
                                            <i class="las la-star"></i>
                                            <i class="las la-star"></i>
                                            <i class="las la-star"></i>
                                        </div>
                                    </div>
                                    <div class="list-total-start">
                                        <div class="item-total full">
                                            <hr>
                                            <div class="start-row">
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                            </div>
                                            <span>100%</span>
                                        </div>
                                        <div class="item-total">
                                            <hr>
                                            <div class="start-row">
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                            </div>
                                            <span>0%</span>
                                        </div>
                                        <div class="item-total">
                                            <hr>
                                            <div class="start-row">
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                            </div>
                                            <span>0%</span>
                                        </div>
                                        <div class="item-total">
                                            <hr>
                                            <div class="start-row">
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                            </div>
                                            <span>0%</span>
                                        </div>
                                        <div class="item-total">
                                            <hr>
                                            <div class="start-row">
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                                <i class="las la-star"></i>
                                            </div>
                                            <span>0%</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="content-comments-section">
                                    <div class="comment-item">
                                        <figure class="image-profile">
                                            <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                        </figure>
                                        <div class="right-info">
                                            <p class="text--name">Daniel Medina Sanchez</p>
                                            <div class="extra-info-comment">
                                                <div class="start-total-rating">
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                </div>
                                                <p>hace 1 día</p>
                                                <figure class="image-country">
                                                    <img src="{{ asset('assets/img/course/colombia.png')}}" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="comment-item">
                                        <figure class="image-profile">
                                            <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                        </figure>
                                        <div class="right-info">
                                            <p class="text--name">Daniel Medina Sanchez</p>
                                            <div class="extra-info-comment">
                                                <div class="start-total-rating">
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                </div>
                                                <p>hace 1 día</p>
                                                <figure class="image-country">
                                                    <img src="{{ asset('assets/img/course/colombia.png')}}" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comment-item">
                                        <figure class="image-profile">
                                            <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                        </figure>
                                        <div class="right-info">
                                            <p class="text--name">Daniel Medina Sanchez</p>
                                            <div class="extra-info-comment">
                                                <div class="start-total-rating">
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                    <i class="las la-star"></i>
                                                </div>
                                                <p>hace 1 día</p>
                                                <figure class="image-country">
                                                    <img src="{{ asset('assets/img/course/colombia.')}}png" alt="">
                                                </figure>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                            </section>
                        </div>
                        <div class="right-content-sections hide-desktop">
                            <div class="extra-info">
                                <h2 class="title--principal-info">
                                    Elaboración de expedientes técnicos en obra publica con la metodología BIM
                                </h2>
                                <div class="count-student">
                                    <div class="row-item">
                                        <figure class="icon-student">
                                            <img src="{{ asset('assets/img/info/users.png')}}" alt="">
                                        </figure>
                                        <p class="text-student">12 Estudiantes</p>
                                    </div>
                                    <div class="row-item">
                                        <figure class="icon-student">
                                            <img src="{{ asset('assets/img/info/check.png')}}" alt="">
                                        </figure>
                                        <p class="text-student">Calificacion: 5.0</p>
                                        <i class="las la-star"></i>
                                    </div>
                                </div>
                                <div class="price-and-bottom">
                                    <div class="text-price">

                                        <p>
                                            Suscribete para acceder a este y otros 80 cursos especializados en arquitectura, diseño y construcción creados por profesionales de todo el mundo.
                                        </p>
                                    </div>
                                    @auth
                                        <div class="button-subscribe">
                                            <a href="../carrito" class="btn btn-primary">
                                                <span>Accede desde solo {{$infocursos['precio']}} $ USD</span>
                                            </a>
                                        </div>
                                    @endauth
                                    @guest
                                        <div class="button-subscribe">
                                            <a href="../registro" class="btn btn-primary">
                                                Accede desde solo {{$infocursos['precio']}} $ USD
                                            </a>
                                        </div>
                                    @endguest

                                </div>

                            </div>
                            <div class="questions-frecuently">
                                <h2>Preguntas Frecuentes</h2>
                                <div class="list-items-diploma">
                                    <ul class="list-items-dropdown" id="list_content_theme_course">
                                        <li class="list-content">
                                            <a href="#" class="link-active">
                                                <span>¿Cuál es el proceso de compra?</span>
                                                <figure class="icon-arrow plus">
                                                    <img class="icon-plus" src="../img/plus.svg" alt="">
                                                </figure>
                                                <figure class="icon-arrow minus d-none">
                                                    <img class="icon-minus" src="../img/minus.svg" alt="">
                                                </figure>
                                            </a>
                                            <ul class="list-this-link">
                                                <li class="line-option">
                                                    <span>Primero, deberás realizar el pago del curso que deseas adquirir a través del método de pago de tu elección, después de ello tendrás acceso al curso correspondiente</span>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="list-content">
                                            <a href="#" class="link-active">
                                                <span>¿Todos los cursos incluyen certificado?</span>
                                                <figure class="icon-arrow plus">
                                                    <img class="icon-plus" src="../img/plus.svg" alt="">
                                                </figure>
                                                <figure class="icon-arrow minus d-none">
                                                    <img class="icon-minus" src="../img/minus.svg" alt="">
                                                </figure>
                                            </a>
                                            <ul class="list-this-link">
                                                <li class="line-option">
                                                    <span>No todos los cursos incluyen certificado, sólo los cursos PREMIUM y algunos de los cursos gratuitos. </span>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="first-right-conent">
                                <div class="top-text-only">
                                    <h2 class="maybe-title">Certificación </h2>
                                    <p>
                                        Una vez finalizado satisfactoriamente el diplomado “Elaboración de expediente en obras publicas el la metodología BIM", el Instituto Itcenco y el Colegio de Ingenieros del Perú emite las diplomas, el cual contiene:
                                    </p>
                                </div>
                                <div class="list-check">
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Datos personales del alumno</p>
                                    </div>
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Fotografía actual.</p>
                                    </div>
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Plan de estudios</p>
                                    </div>
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>420 horas lectivas invertidas </p>
                                    </div>
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Nota final.</p>
                                    </div>
                                    <div class="item-line-text"> 
                                        <figure class="icon-check">
                                            <img src="{{ asset('assets/img/info/check-square.png')}}" alt="">
                                        </figure>
                                        <p>Rúbrica de las instituciones competentes.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="second-right-images">
                                <figure class="img-diploma">
                                    <img src="{{ asset('assets/img/info/diploma.png')}}" alt="">
                                </figure>
<!--                                <figure class="img-diploma">
                                    <img src="{{ asset('assets/img/info/diploma_itc_02.png')}}" alt="">
                                </figure>-->
                            </div>
                        </div>
                    </section>


                </div>

                <section class="container-list-resource-info">
                    <div class="slide-course-relation owl-carousel" id="slide-course-info">
                        @foreach($cursos as  $curso_info)
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{asset($curso_info['photo_url'])}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>{{$curso_info['title']}}</h4>
                                   
                                </div>
                                <div class="price-section">
                                                @if($curso_info['tipo_id']==1)
                                                <p class="body-text-name">Diplomados</p>
                                                @elseif($curso_info['tipo']==2)
                                                <p class="body-text-name">Especialización</p>
                                                @elseif($curso_info['tipo']==3)
                                                <p class="body-text-name">Cursos</p>
                                                @endif
                                                @if($curso_info['modalidad_id']==1)
                                                   <p class="body-text-price--real">Online</p>
                                                @elseif($curso_info['tipo']==2)
                                                <p class="body-text-price--real">Presencial</p>
                                                @elseif($curso_info['tipo']==3)
                                                <p class="body-text-price--real">En vivo</p>
                                                @endif
                                            </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </section>

            </div>
        </section>
    </main>
@endsection
