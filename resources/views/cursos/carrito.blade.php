@extends('base')
@section('Carrito cursos', 'Diploamdos - Instituto Itcenco')
@section('body')
     <main id="main" >
        <section class="content-card-">
            <div class="container">
                <section class="module-content-list-card">
                    <p class="text-count-card">2 productos en mi carrito</p>
                    <div class="card-payment">
                        <div class="list-card-container">

                            <div class="item-course">
                                <figure>
                                    <img src="{{ asset('assets/img/course/course.png')}}" alt="">
                                </figure>
                                <div class="box-info">
                                    <h3>Lectura de planos en edificación</h3>
                                    <p>Especialización</p>
                                    <a href="#">
                                        <i class="las la-trash-alt"></i>
                                        <span>Eliminar</span>
                                    </a>
                                </div>
                                <div class="box-price">
                                    <p class="text--price">$190.00</p>
                                </div>
                            </div>
                            <div class="item-course">
                                <figure>
                                    <img src="{{ asset('assets/img/course/course.png')}}" alt="">
                                </figure>
                                <div class="box-info">
                                    <h3>Lectura de planos en edificación</h3>
                                    <p>Especialización</p>
                                    <a href="#">
                                        <i class="las la-trash-alt"></i>
                                        <span>Eliminar</span>
                                    </a>
                                </div>
                                <div class="box-price">
                                    <p class="text--price">$190.00</p>
                                </div>
                            </div>
                            <div class="item-course">
                                <figure>
                                    <img src="{{ asset('assets/img/course/course.png')}}" alt="">
                                </figure>
                                <div class="box-info">
                                    <h3>Lectura de planos en edificación</h3>
                                    <p>Especialización</p>
                                    <a href="#">
                                        <i class="las la-trash-alt"></i>
                                        <span>Eliminar</span>
                                    </a>
                                </div>
                                <div class="box-price">
                                    <p class="text--price">$190.00</p>
                                </div>
                            </div>
                            <div class="item-course">
                                <figure>
                                    <img src="{{ asset('assets/img/course/course.png')}}" alt="">
                                </figure>
                                <div class="box-info">
                                    <h3>Lectura de planos en edificación</h3>
                                    <p>Especialización</p>
                                    <a href="#">
                                        <i class="las la-trash-alt"></i>
                                        <span>Eliminar</span>
                                    </a>
                                </div>
                                <div class="box-price">
                                    <p class="text--price">$190.00</p>
                                </div>
                            </div>

                        </div>
                        <div class="subtotal-card-contain">
                            <p class="text--title">
                                Resumen de compra
                            </p>
                            <div class="total-buy-resumen">
                                <div class="item">
                                    <strong>Subtotal</strong>
                                    <p>$356.00</p>
                                </div>
                                <div class="item">
                                    <strong>Total(IGV incluido)</strong>
                                    <p>$356.00</p>
                                </div>
                            </div>
                            <div class="total-payment">
                                <p>$356.00 (USD)</p>
                            </div>
                            <div class="btn-row">
                                <button type="button" class=" btn-buy btn btn-primary">Comprar</button>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="container-list-resource-info">
                    <div class="slide-course-relation owl-carousel" id="slide-course-info">
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slide-product">
                            <div class="slide-header">
                                <a href="" class="link-image">
                                    <img class="img-product" src="{{ asset('assets/img/course.jpeg')}}" alt="">
                                </a>
                               
                            </div>
                            <div class="slide-body-text">
                                <div class="info-section">
                                    <h4>Lectura de planos en edificios</h4>
                                    <p class="body-text-name">Nombre del producto</p>
                                </div>
                                
                                <div class="icons-section">
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/users.svg')}}" alt="usuario">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/heart.svg')}}" alt="numero de megusta">
                                        </figure>
                                        <p>120</p>
                                    </div>
                                    <div class="item">
                                        <figure>
                                            <img src="{{ asset('assets/img/course/start.png')}}" alt="">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </main>
@endsection