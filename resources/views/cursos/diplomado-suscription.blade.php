@extends('base')
@section('title', 'Inscripcion - Instituto Itcenco')
@section('body')
<div class="bg-gray-100 w-full">
    <div class="container flex flex-wrap py-8">
        <div class="flex flex-col w-full mx-auto md:w-1/2">
            <div class="flex justify-center md:justify-start md:pl-12 w-full hidden">
                <a class="p-4 text-xl font-bold text-white bg-black">
                    Inscripción
                </a>s
            </div>
            <div class="flex flex-col justify-center px-6 md:justify-start md:mt-4 md:px-16 lg:px-24">
                <h1 class="text-3xl text-center uppercase">
                    FORMULARIO DE INSCRIPCION
                </h1>
                <form class="flex flex-col pt-3" method="POST" action="{{route('diplomado-suscription')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="name" class="text-gray-700">
                                Nombre(s) y Apellido(s)
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="text" class="@error('name') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="name" placeholder="Nombre completo" required="true" autocomplete="name" value="{{ old('name') }}" />
                            @error('name')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('name')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="email" class="text-gray-700">
                                Correo Electronico
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="email" class="@error('email') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="email" placeholder="Correo electronico" required="true" autocomplete="email" value="{{ old('email') }}" />
                            @error('email')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('email')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="document" class="text-gray-700">
                                Documento de Identidad
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="text" class="@error('document') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="document" placeholder="Documento de Identidad" required="true" autocomplete="document" value="{{ old('document') }}" />
                            @error('document')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('document')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="phone" class="text-gray-700">
                                Celular
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="text" class="@error('phone') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="phone" placeholder="Celular" required="true" autocomplete="phone" value="{{ old('phone') }}" />
                            @error('phone')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('phone')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="country" class="text-gray-700">
                                Pais
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="text" class="@error('country') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="country" placeholder="Pais" required="true" autocomplete="country" value="{{ old('country') }}" />
                            @error('country')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('country')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4">
                        <div class="relative">
                            <label for="special" class="text-gray-700">
                                Especialización
                                <span class="text-red-500 required-dot"> *</span>
                            </label>
                            <input type="text" class="@error('special') border-red-500 focus:border-purple-600 focus:ring-0 @enderror focus:ring-gray-200 focus:ring-offset-2 focus:ring-2  rounded-lg flex-1 appearance-none border-2 border-gray-200 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 focus:placeholder-gray-500 shadow-sm text-base focus:outline-none" name="special" placeholder="Especialización" required="true" autocomplete="special" value="{{ old('special') }}" />
                            @error('special')
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="absolute text-red-500 right-2 bottom-3" viewBox="0 0 1792 1792">
                                <path d="M1024 1375v-190q0-14-9.5-23.5t-22.5-9.5h-192q-13 0-22.5 9.5t-9.5 23.5v190q0 14 9.5 23.5t22.5 9.5h192q13 0 22.5-9.5t9.5-23.5zm-2-374l18-459q0-12-10-19-13-11-24-11h-220q-11 0-24 11-10 7-10 21l17 457q0 10 10 16.5t24 6.5h185q14 0 23.5-6.5t10.5-16.5zm-14-934l768 1408q35 63-2 126-17 29-46.5 46t-63.5 17h-1536q-34 0-63.5-17t-46.5-46q-37-63-2-126l768-1408q17-31 47-49t65-18 65 18 47 49z">
                                </path>
                            </svg>
                            @enderror
                        </div>
                        @error('special')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-col mt-4 justify-center px-5 pt-5 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                        <div x-data="{total: 0}" class="space-y-1 text-center">
                            <div class="flex justify-center text-sm text-gray-600">
                                <label for="files" class="relative cursor-pointer bg-transparent rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                    <span x-text="total > 0 ? total + ' archivos seleccionados' : 'Adjuntar Recibo'"></span>
                                    <input id="files" multiple="true" name="files" x-on:change="total = $event.target.files.length" type="file" class="sr-only" required="true">
                                </label>
                            </div>
                            <p class="text-xs text-gray-500">
                                PDF, PNG, JPG up to 5MB
                            </p>
                        </div>
                        @error('files')
                        <p class="text-sm text-red-500 -bottom-6">
                            {{$message}}
                        </p>
                        @enderror
                    </div>
                    @if ($message = Session::get('success'))
                    <p class="text-sm text-green-500 mt-3">{{__($message)}}</p>
                    @endif
                    <button type="submit" class="w-full px-4 py-2 mt-6 text-base font-semibold text-center text-white transition duration-200 ease-in bg-black shadow-md hover:text-black hover:bg-white focus:outline-none focus:ring-2">
                        <span class="w-full">
                            Enviar
                        </span>
                    </button>
                </form>
            </div>
        </div>
        <div class="md:w-1/2 shadow-2xl my-auto">
            <div class="md:flex md:justify-center my-4">
                <div class="md:w-10/12 xl:w-8/12 text-center">
                    <h1 class="text-3xl md:text-4xl mb-4">Inscripción del Diplomado</h1>
                    <p class="text-xl font-light">Para incribirte al diplomado necesitas haber realizado el pago correspondiente, luego puedes adjuntar el recibo del pago en el formulario</p>
                    <div class="mt-6 flex justify-center items-center">
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="hover:outline-none outline-none">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="itcencoeduperu@gmail.com">
                            <input type="hidden" name="lc" value="US">
                            <input type="hidden" name="item_name" value="Diplomado">
                            <input type="hidden" name="item_number" value="diplomados">
                            <input type="number" class="py-2 px-4" name="amount" min="1" max="1000" step="any" value="197.00" required="true">
                            <input type="hidden" name="currency_code" value="USD">
                            <input type="hidden" name="button_subtype" value="services">
                            <input type="hidden" name="no_note" value="0">
                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
                            <button type="submit" class="bg-indigo-600 border-2 border-solid border-indigo-600 hover:bg-indigo-700 hover:border-indigo-700 text-white rounded-full py-3 px-8 transition-colors duration-300 mx-2 mt-4 focus:outline-none">Pagar</button>
                        </form>
                        <a class="hidden" href="{{route('curso')}}">
                            <button class="border-2 border-solid border-indigo-600 text-indigo-600 hover:bg-indigo-600 hover:text-white rounded-full py-3 px-8 transition-colors duration-300 mx-2 mt-4 focus:outline-none">Ir a diplomados</button>
                        </a>
                    </div>
                </div>
            </div>
            <img class="p-3 w-full shadow-md mt-5" src="{{asset('/images/diplomado instco.jpg')}}"/>
        </div>
    </div>
</div>
@endsection
