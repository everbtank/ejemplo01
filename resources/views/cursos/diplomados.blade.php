@extends('base')
@section('title', 'Diploamdos - Instituto Itcenco')
@section('body')
    <main id="main" >
        <div class="container-courses">
            <div class="container">
                <div class="course-filter-section">
                    <section class="product__header">
                        <div class="levels">
                            <div class="level__item">
                                <a href="#" class="text">Inicio</a>
                                <img src="assets/img/arrow_menu.png" alt="Arrow left">
                            </div>
                            <div class="level__item">
                                <a href="#" class="text">Diplomados</a>
                            </div>
                        </div>
                    </section>
                    <section class="container-filter-products " >
                        <div class="filter-product-content" id="contenedor-filtro-producto">
                            <div class="title-filter">
                                <h3>Filtros</h3>
                            </div>
                            
                            <div class="filtro-contenedor-dropdown">
                            <div class="box-dropdown">
                                    <input type="checkbox" class="d-none open-option"  id="filter_alternativo">
                                    <label for="filter_alternativo" class="label-first">
                                        <p>Modalidad</p>
                                        <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg class="d-none" width="16" height="9" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 7L7 1L1 7" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <div class="option box-filter-marca">
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-01" >
                                            <label for="check-01">Online</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-02" >
                                            <label for="check-02">Presencial</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-03" >
                                            <label for="check-03">En vivo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-dropdown">
                                    <input type="checkbox" class="d-none open-option"  id="filter_alternativo">
                                    <label for="filter_alternativo" class="label-first">
                                        <p>Categorías</p>
                                        <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg class="d-none" width="16" height="9" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 7L7 1L1 7" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <div class="option box-filter-marca">
                                        
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-01" >
                                            <label for="check-01">Arquitectura</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-02" >
                                            <label for="check-02">Civíl</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-03" >
                                            <label for="check-03">General</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-dropdown">
                                    <input type="checkbox" class="d-none open-option"  id="filter_marca">
                                    <label for="filter_marca" class="label-first">
                                        <p>Diplomados</p>
                                        <svg class="icon-down" width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg class="d-none" width="16" height="9" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 7L7 1L1 7" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <div class="option box-filter-marca">
                                        
                                        <div class="item-input-content">
                                           
                                            <a href="">Lectura de planos</a>
                                        </div>
                                        <div class="item-input-content">
                                            <a href="">Autocad</a>
                                        </div>
                                        <div class="item-input-content">
                                            <a href="#">Civil</a>
                                        </div>
                                        <div class="btn-content-expand">
                                            <button class="more-item">Ver más</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-dropdown">
                                    <input type="checkbox" class="d-none open-option"  id="filter_precio">
                                    <label for="filter_precio" class="label-first">
                                        <p>Especialidades</p>
                                        <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg class="d-none" width="16" height="9" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 7L7 1L1 7" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <div class="option box-filter-marca">
                                        <div class="item-input-content">
                                            <input type="radio" name="price_between" id="radio-01" >
                                            <label for="radio-01">Elaboración de expediente
                                                tecnico en obras públicas</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="radio" name="price_between" id="radio-02" >
                                            <label for="radio-02">Elaboración de expediente tecncio 3d modelado</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="radio" name="price_between" id="radio-03" >
                                            <label for="radio-03">Elaboración de expediente tecncio 3d modelado</label>
                                        </div>
                                        <div class="btn-content-expand">
                                            <button class="more-item">Ver más</button>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="box-dropdown">
                                    <input type="checkbox" class="d-none open-option"  id="filter_nivel">
                                    <label for="filter_nivel" class="label-first">
                                        <p>Nivel</p>
                                        <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                        <svg class="d-none" width="16" height="9" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M13 7L7 1L1 7" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </label>
                                    <div class="option box-filter-marca">
                                        
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-01" >
                                            <label for="check-01">Básico</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-02" >
                                            <label for="check-02">Intermedio</label>
                                        </div>
                                        <div class="item-input-content">
                                            <input type="checkbox" id="check-03" >
                                            <label for="check-03">Avanzado</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="filter_content_button">
                                <button  class="btn btn-secondary" id="btn-cancle-filter-content">Cancelar</button>
                                <button  class="btn btn-primary">Aceptar</button>
                            </div>
                        </div>
                        <div class="list-product-content">
                            <div class="header-product-container">
                                <div class="title-course-list">
                                     <figure>
                                         <img src="assets/img/Machine_Learning.png" alt="">
                                     </figure>
                                     <span>Diplomados</span>
                                </div>
                                <div class="btn-filter" id="open-container-filter">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" aria-hidden="true" focusable="false"><path d="M5,7.6h5.7c0.4,1.2,1.5,2,2.8,2s2.4-0.8,2.8-2H19c0.6,0,1-0.4,1-1s-0.4-1-1-1h-2.7c-0.4-1.2-1.5-2-2.8-2s-2.4,0.8-2.8,2H5 c-0.6,0-1,0.4-1,1S4.4,7.6,5,7.6z M13.5,5.6c0.6,0,1,0.4,1,1s-0.4,1-1,1s-1-0.4-1-1S12.9,5.6,13.5,5.6z"></path><path d="M19,11.1h-7.7c-0.4-1.2-1.5-2-2.8-2s-2.4,0.8-2.8,2H5c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h0.7c0.4,1.2,1.5,2,2.8,2 s2.4-0.8,2.8-2H19c0.6,0,1-0.4,1-1C20,11.5,19.6,11.1,19,11.1z M8.5,13.1c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1s1,0.4,1,1 C9.5,12.7,9.1,13.1,8.5,13.1z"></path><path d="M19,16.6h-1.2c-0.4-1.2-1.5-2-2.8-2s-2.4,0.8-2.8,2H5c-0.6,0-1,0.4-1,1s0.4,1,1,1h7.2c0.4,1.2,1.5,2,2.8,2s2.4-0.8,2.8-2 H19c0.6,0,1-0.4,1-1S19.6,16.6,19,16.6z M15,18.6c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S15.6,18.6,15,18.6z"></path></svg>
                                    <p>Todos los filtros</p>
                                </div>
                                <!-- <div class="sort-by-content">
                                    <input type="checkbox" id="sort_by_product">
                                    <div class="label_dropdown">
                                        <label for="sort_by_product"> <strong>Ordenar por: </strong> <span>Destacados</span> </label>
                                        <svg class="icon-down" width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15 1L8 8L1 1" stroke="#181818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div> -->
                            </div>
                            <div class="rigth-content-products">
                                <div class="cards-content-products">
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="infocurso" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <div class="container-float-header-card">
                                                <a href="" class="discount">Nuevo</a>
                                            </div>
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="infocurso" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios nombres mas largos</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a  href="info"class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios para contar historias de terror</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="infocurso" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="infocurso" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name"> Lorem ipsum dolor sit amet. Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide-product">
                                        <div class="slide-header">
                                            <a href="" class="link-image">
                                                <img class="img-product" src="assets/img/course.jpeg" alt="">
                                            </a>
                                            <!--<div class="container-float-header-card">
                                                <a href="" class="discount">-20%</a>
                                                <a href="" class="like-header-icon">
                                                    <img src="assets/img/heart.svg" alt="">
                                                </a>
                                            </div>-->
                                        </div>
                                        <div class="slide-body-text">
                                            <div class="info-section">
                                                <h4>Lectura de planos en edificios</h4>
                                                <p class="body-text-name">Nombre del producto</p>
                                            </div>
                                            <!--<div class="price-section">
                                                <p class="body-text-price--discount">S/19.00</p>
                                                <p class="body-text-price--real">S/19.00</p>
                                            </div>-->
                                            <div class="icons-section">
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/users.svg" alt="usuario">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/heart.svg" alt="numero de megusta">
                                                    </figure>
                                                    <p>120</p>
                                                </div>
                                                <div class="item">
                                                    <figure>
                                                        <img src="assets/img/course/start.png" alt="">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="content-btn-more">
                                    <!-- <img src="assets/img/producto/btn_arrow_clircle_down.svg" alt="">
                                    <span>Ver más resultados</span> -->
                                    <div class="content-paginate">
                                        <div class="btn-left click">
                                            <a href="#">
                                                <img src="assets/img/arrow_left.png" alt="">
                                            </a>
                                        </div>
                                        <div class="items">
                                            <div class="number">
                                                <p>1</p>
                                            </div>
                                            <div class="number">
                                                <p>2</p>
                                            </div>
                                            <div class="point">
                                                <p>...</p>
                                            </div>
                                        </div>
                                        
                                        <div class="btn-right click active">
                                            <a href="#">
                                                <img src="assets/img/arrow_right_active.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </section>


                </div> 
            </div>
        </div>
    </main>
@endsection