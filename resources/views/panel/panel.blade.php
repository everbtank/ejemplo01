@extends('base')
@section('title', 'Panel de control - Itcenco')
@section('body')
<div class="py-12 bg-gray-100">
  <div class="container flex flex-col items-center justify-center px-4 pt-2 pb-8 mx-auto sm:px-6 lg:px-8">
    <h2 class="flex justify-center font-sans text-3xl font-bold leading-none tracking-tight text-center text-gray-900 b-6 sm:text-4xl md:mx-auto">
      <svg class="w-8 h-8 mt-1 mr-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
        <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z" />
        <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"/>
      </svg>
      <span class="relative">Panel de control</span> </h2>
      <div class="grid max-w-lg gap-5 mx-auto mt-12 md:grid-cols-2 lg:grid-cols-3 md:max-w-none">
        <div class="flex flex-col overflow-hidden rounded-lg shadow-lg">
          <div class="relative flex-shrink-0">
            <img class="object-cover w-full h-56" src="/assets/wallpapers/panel-classroom.jpg" />
            <span class="absolute bottom-0 right-0 inline-flex items-center px-3 py-1 mr-4 -mb-3 text-xs font-medium leading-tight text-gray-800 bg-gray-100 border rounded-full hover:text-gray-900 hover:font-bold">{{$classrooms->count()}} Aulas</span>
          </div>
          <div class="flex flex-col justify-between flex-1">
            <div class="flex flex-col justify-between flex-1 p-6 bg-white">
              <div>
                <a href="{{route('panel-classroom')}}" class="block text-xl font-semibold leading-7 text-gray-900 hover:text-blue-600 hover:font-black">Aulas Virtuales</a>
                <p class="mt-3 text-base leading-6 text-gray-500">
                  ¿Necesitas un grupo de aula virtual? Aqui puedes crear y manejar todas las aulas virtuales.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-col overflow-hidden rounded-lg shadow-lg">
          <div class="relative flex-shrink-0">
            <img class="object-cover w-full h-56" src="/assets/wallpapers/panel-certificate.jpg" alt="Cover image for creating a laravel package course."/>
            <span class="absolute bottom-0 right-0 inline-flex items-center px-3 py-1 mr-4 -mb-3 text-xs font-medium leading-tight text-gray-800 bg-gray-100 border rounded-full hover:text-gray-900 hover:font-bold">{{$certificates->count()}} Certificados</span>
          </div>
          <div class="flex flex-col justify-between flex-1">
            <div class="flex flex-col justify-between flex-1 p-6 bg-white">
              <div>
                <a href="{{route('panel-certificate')}}" class="block text-xl font-semibold leading-7 text-gray-900 hover:text-blue-600 hover:font-black">Certificados</a>
                <p class="mt-3 text-base leading-6 text-gray-500">
                  Administra la lista de certificados asi como quitar y agregar nuevos certificados a los alumnos.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-col overflow-hidden rounded-lg shadow-lg hover:shadow-xl">
          <div class="relative flex-shrink-0">
            <img class="object-cover w-full h-56" src="/assets/wallpapers/panel-student.jpg" alt="Cover image for laravel dusk course."/>
            <span class="absolute bottom-0 right-0 inline-flex items-center px-3 py-1 mr-4 -mb-3 text-xs font-medium leading-tight text-gray-800 bg-gray-100 border rounded-full hover:text-gray-900 hover:font-bold">{{$users->count()}} Alumnos</span>
          </div>
          <div class="flex flex-col justify-between flex-1">
            <div class="flex flex-col justify-between flex-1 p-6 bg-white">
              <div>
                <a href="#" class="text-xl font-semibold leading-7 text-gray-900 hover:text-blue-600 hover:font-black">Alumnos</a>
                <p class="mt-3 text-base leading-6 text-gray-500">
                  Maneja la lista de todos los alumnos asi como tambien agregar nuevos alumnos mediante sus información personal.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection