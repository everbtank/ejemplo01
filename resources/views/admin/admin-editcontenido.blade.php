@extends('base')
@section('title', 'Contenido - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                        <div class="menu-list-content">
                                <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <div class="body-producto">
                    <div class="container-options write-comment">
                    <div class="header-modal">
                        <div class="row-group">
                            <div class="form-group">
                                <a href="{{ route('admin-contenidos') }}" >
                                    <h2>Anterior</h2>
                                </a>
                               
                            </div>
                            <div class="form-group">
                                 <h2>Editar Producto</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <form action="{{route('editcontenidos', ['id' => $contenidos->id])}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" value="{{$contenidos->title}}" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Body:</label>
                        <div class="input-group">
                            <textarea name="body"  value="{{$contenidos->body}}"  class="body" id="description" cols="30" rows="4"></textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                  
                  
                    <div class="form-group">
                        <label for="imagen">Subir archivo:</label>
                        <div class="input-group">
                            <input type="file"  value="{{$contenidos->file}}"  class="input-control" name="file" id="file">
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
                        
                </div>
            </div>
        </article>
    </main>
@endsection