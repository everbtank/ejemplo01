@extends('base')
@section('title', 'Nosotros - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                        <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Analytics</h3>
                        </div>
                        <div class="principal-content">
                            <div class="top-dashboard">
                                <div class="card-dashboard">
                                    <p>Productos cursos</p>
                                    <span>1</span>
                                </div>
                                <div class="card-dashboard">
                                    <p>Cursos</p>
                                    <span>0</span>
                                </div>
                                <div class="card-dashboard">
                                    <p>Estudiantes</p>
                                    <span>600</span>
                                </div>
                                <div class="card-dashboard">
                                    <p>Ganancias</p>
                                    <span>$ 0 </span>
                                </div>
                            </div>

                            <div class="content-graphic">
                                <div class="statistics">
                                    <div class="top-info-statistics">
                                        <h3>Grafica de inscritos</h3>
                                        <i class="las la-bars"></i>
                                    </div>
                                    <div class="figure-graphic">
                                        <figure>
                                            <img src="" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="recent-students">
                                    <h3>Últimos alumnos</h3>
                                    <div class="list-students">
                                        <div class="student">
                                            <figure>
                                                <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                            </figure>
                                            <div class="text-group">
                                                <p>Juan de la torre</p>
                                                <span>juan@gmail.com</span>
                                            </div>
                                        </div>
                                        <div class="student">
                                            <figure>
                                                <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                            </figure>
                                            <div class="text-group">
                                                <p>Juan de la torre</p>
                                                <span>juan@gmail.com</span>
                                            </div>
                                        </div>
                                        <div class="student">
                                            <figure>
                                                <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                            </figure>
                                            <div class="text-group">
                                                <p>Juan de la torre</p>
                                                <span>juan@gmail.com</span>
                                            </div>
                                        </div>
                                        <div class="student">
                                            <figure>
                                                <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                            </figure>
                                            <div class="text-group">
                                                <p>Juan de la torre</p>
                                                <span>juan@gmail.com</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>
@endsection
