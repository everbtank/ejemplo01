@extends('base')
@section('title', 'Cursos - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Cursos</h3>
                             <a href="" class="btn btn-primary" id="open-productos-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-cursos')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Imagen</th>
                                            <th>Curso</th>
                                            <th>Descripción</th>
                                            <th>Inicio</th>
                                            <th>Zoom</th>
                                            <th>Meet</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                       @if(count($clases)<=0)
                                          <tr>
                                          <td colspan="8"> ho hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($clases as $clase)
                                        <tr>
                                        
                                            <td>{{$clase->id}}</td>
                                            <td>
                                                <figure>
                                                    <img src="{{ asset('assets/img/1.jpg')}}" alt="">
                                                </figure>
                                            </td>
                                            <td>{{$clase->titulo}}</td>
                                            <td>
                                                {{$clase->description}}
                                            </td>
                                            <td>
                                                {{$clase->class_start}}
                                            </td>
                                            <td>
                                                {{$clase->zoom}}
                                            </td>
                                             <td>
                                                {{$clase->meet}}
                                            </td>
                                            
                                            <td>
                                                <a href="eliminar" class="link-action">
                                                  <i class="las la-share-alt"></i>eliminar
                                                </a>
                                                <a href="borrar" class="link-action">
                                                  <i class="las la-share-alt"></i>borrar
                                                </a>
                                            </td>
                                        </tr>
                                     @endforeach
                                     @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>

@endsection