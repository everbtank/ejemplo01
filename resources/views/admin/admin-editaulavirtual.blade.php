@extends('base')
@section('title', 'Certificado - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                        <div class="menu-list-content">
                               <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <div class="body-producto">
                    <div class="container-options write-comment">
                    <div class="header-modal">
                        <div class="row-group">
                            <div class="form-group">
                                <a href="{{ route('admin-aulavirtual') }}" >
                                    <h2>Anterior</h2>
                                </a>
                               
                            </div>
                            <div class="form-group">
                                 <h2>Editar Aulavirtual</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <form action="{{route('putaulavirtual', ['id' => $certificados->id])}}" method="post">
                    @csrf  
                    {{--@method('PUT')--}}
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <div class="input-group">
                            <input type="text" value="{{$certificados->student_name}}" class="input-control" name="student_name" id="student_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Documento:</label>
                        <div class="input-group">
                            <input type="number"  value="{{$certificados->student_dni}}" class="input-control" name="student_dni" id="student_dni">
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Curso:</label>
                        <div class="input-group">
                            <input type="text"  value="{{$certificados->course_name}}" class="input-control" name="course_name" id="course_name">
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Codigo:</label>
                            <div class="input-group">
                                <input type="text" value="{{$certificados->course_code}}"  class="input-control" name="course_code" id="course_code">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Instituto:</label>
                            <div class="input-group">
                               <select type="text" value="{{$certificados->school}}"   class="input-control" name="school" id="school">
                                    <option value="">Select</option>
                                    <option value="Instituto itcenco">Instituto Itcenco</option>
                                    <option value="Eca peru">Eca peru</option>
                                    <option value="Cype peru">Cype peru</option>
                                    <option value="CIP">CIP</option>
                                </select>
                            </div>
                        </div>
                      
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de finalizacion:</label>
                        <div class="input-group">
                            <input type="date" value="{{$certificados->course_end}}" class="input-control" name="course_end" id="course_end">
                        </div>
                    </div>
                    
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Nota total:</label>
                            <div class="input-group">
                                <input type="text" value="{{$certificados->course_score}}" class="input-control" name="course_score" id="course_score">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Horas:</label>
                            <div class="input-group">
                                <input type="text"  value="{{$certificados->course_time}}" class="input-control"  name="course_time" id="course_time">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fecha_inicio">Certificado virtual(URL):</label>
                        <div class="input-group">
                            <input type="text" value="{{$certificados->url_download}}" class="input-control" name="url_download" id="url_download">
                        </div>
                    </div>

                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
                        
                </div>
            </div>
        </article>
    </main>
@endsection