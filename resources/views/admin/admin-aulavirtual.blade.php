@extends('base')
@section('title', 'Aulavirtual - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Classrooms (Aula virtual anterior)</h3>
                             <a href="" class="btn btn-primary" id="open-aulavirtual-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-aulavirtual')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Foto</th>
                                            <th>Title</th>
                                            <th>Descripción</th>
                                            <th>Inicio</th>
                                            <th>Fin</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     @if(count($classrooms)<=0)
                                          <tr>
                                          <td colspan="8"> No hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($classrooms as $classroom)
                                        <tr>
                                        
                                            <td>{{$classroom->id}}</td>
                                            <td>
                                                
                                                    <img src="{{asset($classroom->photo_url ?? '/images/avatar.png')}}" alt="">
                                                
                                            </td>
                                            <td>{{$classroom->title}}</td>
                                            <td>
                                                {{$classroom->description}}
                                            </td>
                                             <td>
                                                {{$classroom->class_start}}
                                            </td>
                                            <td>
                                                {{$classroom->class_end}}
                                            </td>

                                          
                                            <td>
                                                <a href="{{route('editaulavirtual', ['id' => $classroom->id])}}" class="btn btn-primary">
                                             
                                                    <i class="las la-pen"></i>
                                                </a>
                                            </td>
                                            <td>
                                            <form action="{{route('destroyaulavirtual', ['id' => $classroom->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="las la-trash-alt"></i>
                                                </button>
                                            </form>
                                            </td>
                                        </tr>
                                      
                                     @endforeach
                                     @endif
                                      
                                    </tbody>
                                </table>
                            </div>
                              {{ $classrooms->links('vendor.pagination.default') }}
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>

 <!-- Modal agregar -->
    <div class="container-modal close-modal" id="modal_aulavirtual">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Agregar Aula virtual </h2>
                <a href="#" id="close_form_aulavirtual">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                <form action="{{route('storecertificados')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Titulo:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="title" id="title" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción:</label>
                        <div class="input-group">
                            <input type="number" class="input-control" name="description" id="description" required>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">URL meet(Opcional)</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="meet_url" id="meet_url" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">URL zoom(Opcional)</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="zoom_url" id="zoom_url" >
                        </div>
                    </div>

                     <div class="form-group">
                        <label for="fecha_inicio">URL mstm(Opcional)</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="mstm_url" id="mstm_url" >
                        </div>
                    </div>

                    
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Inicio:</label>
                            <div class="input-group">
                                <input type="date" class="input-control" name="course_score" id="course_score" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Final:</label>
                            <div class="input-group">
                                <input type="date" class="input-control" name="course_time" id="course_time"  required>
                            </div>
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label for="imagen">Subir foto:</label>
                        <div class="input-group">
                            <input type="file" class="input-control" name="potho_url" id="potho_url">
                        </div>
                    </div>

                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection