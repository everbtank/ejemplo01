@extends('base')
@section('title', 'Productos - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Productos cursos</h3>
                             <a href="" class="btn btn-primary" id="open-productos-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-productos')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Foto</th>
                                            <th>Curso</th>
                                            <th>Descripción</th>
                                            <th>Inicio</th>
                                            <th>Duración</th>
                                            <th>Precio</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @if(count($productos)<=0)
                                          <tr>
                                          <td colspan="8"> No hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($productos as $producto)
                                        <tr>
                                        
                                            <td>{{$producto->id}}</td>
                                            <td>
                                              
                                                <img src="{{asset($producto->photo_url)}}" alt="">
                                              
                                            </td>
                                            <td>{{$producto->title}}</td>
                                            <td>
                                                {{$producto->description}}
                                            </td>
                                            <td>
                                               {{$producto->inicio}}
                                            </td>
                                             <td>
                                               {{$producto->duracion}}
                                            </td>
                                            <td>
                                                {{$producto->precio}}
                                            </td>
                                            <td>
                                                <a href="{{route('editproductos', ['id' => $producto->id])}}" class="btn btn-primary">
                                             
                                                    <i class="las la-pen"></i>
                                                </a>
                                            </td>
                                            <td>
                                            <form action="{{route('destroyproductos', ['id' => $producto->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="las la-trash-alt"></i>
                                                </button>
                                            </form>
                                            </td>
                                        </tr>
                                      
                                     @endforeach
                                     @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>


 <!-- Modal agregar -->
    <div class="container-modal close-modal" id="modal_productos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Agregar producto curso</h2>
                <a href="#" id="close_form_productos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                <form action="{{route('storeproductos')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción de producto:</label>
                        <div class="input-group">
                            <textarea name="description" class="input-control" id="description" cols="30" rows="4"></textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha de inicio:</label>
                            <div class="input-group">
                                <input type="date" class="input-control" name="inicio" id="inicio">
                            </div>
                        </div>
                        
                    </div>
                  
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Precio:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Moneda:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" name="moneda" id="moneda">
                                    <option value="USD">USD</option>
                                    <option value="PEN">PEN</option>
                                    <option value="EUR">EUR</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Duración:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Diplomas:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" name="school" id="school">
                                <option value="Instituto Itcenco">Instituto Itcenco</option>
                                <option value="Ecca Peru">Ecca Perú</option>
                                <option value="Cype Peru">Cype Perú</option>
                                <option value="CIP">Colegio de Ingenieros</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Tipo:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="tipo" id="tipo">
                                    @foreach($tipos as  $tipo)
                                
                                    <option name="{$tipo['id']}}" value="{{$tipo['id']}}">{{$tipo['tipo']}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Categoria:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="categoria" id="categoria">
                                @foreach($categorias as  $categoria)
                                
                                    <option name="{$categoria['id']}}" value="{{$categoria['id']}}">{{$categoria['categoria']}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                     <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Modalidad:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="modalidad" id="modalidad">
                                    @foreach($modalidades as  $modalidad)
                                
                                    <option name="{$modalidad['id']}}" value="{{$modalidad['id']}}">{{$modalidad['modalidad']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Nivel:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="nivel" id="nivel">
                                    @foreach($niveles as  $nivel)
                                
                                    <option name="{$nivel['id']}}" value="{{$nivel['id']}}">{{$nivel['nivel']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input  type="file" name="imagen" id="imagen" class="input-control" >
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection