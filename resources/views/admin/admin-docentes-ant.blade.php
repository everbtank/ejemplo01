@extends('base')
@section('title', 'Productos - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-contenido') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificado') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Alumnos</h3>
                             <a href="" class="btn btn-primary" id="open-productos-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-productos')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Foto</th>
                                            <th>Curso</th>
                                            <th>Descripción</th>
                                            <th>Inicio</th>
                                            <th>Tipo</th>
                                            <th>Precio</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @if(count($alumnos)<=0)
                                          <tr>
                                          <td colspan="8"> ho hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($alumnos as $alumno)
                                        <tr>
                                        
                                            <td>{{$alumno->id}}</td>
                                            <td>
                                                <figure>
                                                    <img src="{{asset($alumno->photo_url ?? '/images/avatar.png')}}" alt="">
                                                </figure>
                                            </td>
                                            <td>{{$alumno->name}}</td>
                                            <td>
                                                {{$alumno->email}}
                                            </td>
                                            <td>
                                               {{$alumno->identify}}
                                            </td>
                                            <td>
                                                {{$alumno->created_at}}
                                            </td>
    
                                            <td>
                                                <a href="{{route('editproductos', ['id' => $alumno->id])}}" class="btn btn-primary">
                                             
                                                    <i class="las la-pen"></i>
                                                </a>
                                            </td>
                                            <td>
                                            <form action="{{route('destroyproductos', ['id' => $alumno->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="las la-trash-alt"></i>
                                                </button>
                                            </form>
                                            </td>
                                        </tr>
                                      
                                     @endforeach
                                     @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>


 <!-- Modal agregar -->
    <div class="container-modal close-modal" id="modal_productos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Agregar producto curso</h2>
                <a href="#" id="close_form_productos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                <form action="{{route('storeproductos')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción de producto:</label>
                        <div class="input-group">
                            <textarea name="description" class="input-control" id="description" cols="30" rows="4"></textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de inicio:</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="inicio" id="inicio">
                        </div>
                    </div>
                  
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Precio:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Moneda:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" name="moneda" id="moneda">
                                    <option value="USD">USD</option>
                                    <option value="PEN">PEN</option>
                                    <option value="EUR">EUR</option>

                                </select>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Tipo:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="fecha_horario" id="fecha_inicio">
                                    {{--@foreach($tipos as  $tipo)
                                
                                    <option name="{$tipo['id']}}" value="{{$tipo['id']}}">{{$tipo['tipo']}}</option>
                                    @endforeach--}}
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Categoria:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="categoria" id="categoria">
                                @foreach($categorias as  $categoria)
                                
                                    <option name="{$categoria['id']}}" value="{{$categoria['id']}}">{{$categoria['categoria']}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                     <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Modalidad:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="modalidad" id="modalidad">
                                    @foreach($modalidades as  $modalidad)
                                
                                    <option name="{$modalidad['id']}}" value="{{$modalidad['id']}}">{{$modalidad['modalidad']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Nivel:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="nivel" id="nivel">
                                    @foreach($niveles as  $nivel)
                                
                                    <option name="{$nivel['id']}}" value="{{$nivel['id']}}">{{$nivel['nivel']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input type="file" class="input-control" name="potho_url" id="potho_url">
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


 

<!-- Modal eliminar -->
    <div class="container-modal close-modal" id="modal_deleteproductos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Editar Producto </h2>
                <a href="#" id="close_form_deleteproductos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                <form action="{{route('storeproductos')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Deseas eliminar </label>
                       
                    </div>

                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Modal editar -->
    <input type="hidden" id="id_producto" name="id_producto" value="{{$producto->id}}">
    <div class="container-modal close-modal" id="modal_editproductos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Editar Producto </h2>
                <a href="#" id="close_form_editproductos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
               
                <form action="{{route('editproductos', ['id' => $producto->id])}}" method="POST">
                    @csrf
                     @method('PUT')
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" value="{{$producto->title}}" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción de producto:</label>
                        <div class="input-group">
                            <textarea name="description"  value="{{$producto->description}}" class="input-control" id="description" cols="30" rows="4"></textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de inicio:</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="inicio" id="inicio">
                        </div>
                    </div>
                  
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Precio:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Moneda:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" name="moneda" id="moneda">
                                    <option value="USD">USD</option>
                                    <option value="PEN">PEN</option>
                                    <option value="EUR">EUR</option>

                                </select>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Tipo:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="fecha_horario" id="fecha_inicio">
                                    {{--@foreach($tipos as  $tipo)
                                
                                    <option name="{$tipo['id']}}" value="{{$tipo['id']}}">{{$tipo['tipo']}}</option>
                                    @endforeach--}}
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Categoria:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="categoria" id="categoria">
                                @foreach($categorias as  $categoria)
                                
                                    <option name="{$categoria['id']}}" value="{{$categoria['id']}}">{{$categoria['categoria']}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                     <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Modalidad:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="modalidad" id="modalidad">
                                    @foreach($modalidades as  $modalidad)
                                
                                    <option name="{$modalidad['id']}}" value="{{$modalidad['id']}}">{{$modalidad['modalidad']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Nivel:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="nivel" id="nivel">
                                    @foreach($niveles as  $nivel)
                                
                                    <option name="{$nivel['id']}}" value="{{$nivel['id']}}">{{$nivel['nivel']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input type="file" class="input-control" name="potho_url" id="potho_url">
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection