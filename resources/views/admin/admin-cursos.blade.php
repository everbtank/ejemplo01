@extends('base')
@section('title', 'Cursos - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Cursos</h3>
                             <a href="" class="btn btn-primary" id="open-cursos-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-cursos')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Imagen</th>
                                            <th>Curso</th>
                                            <th>Descripción</th>
                                            <th>Inicio</th>
                                            <th>Final</th>
                                            <th>Zoom</th>
                                            <th>Meet</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                       @if(count($cursos)<=0)
                                          <tr>
                                          <td colspan="8"> ho hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($cursos as $curso)
                                        <tr>
                                        
                                            <td>{{$curso->id}}</td>
                                            <td>
                                                <figure>
                                                    <img src="{{asset($curso->imagen)}}" alt="">
                                                </figure>
                                            </td>
                                            <td>{{$curso->title}}</td>
                                            <td>
                                                {{$curso->description}}
                                            </td>
                                            <td>
                                                {{$curso->class_start}}
                                            </td>
                                             <td>
                                                {{$curso->class_end}}
                                            </td>
                                            <td>
                                                {{$curso->zoom}}
                                            </td>
                                             <td>
                                                {{$curso->meet}}
                                            </td>
                                            <td>
                                                <a href="{{route('editcursos', ['id' => $curso->id])}}" class="btn btn-primary">
                                             
                                                    <i class="las la-pen"></i>
                                                </a>
                                            </td>
                                            <td>
                                            <form action="{{route('destroycursos', ['id' => $curso->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="las la-trash-alt"></i>
                                                </button>
                                            </form>
                                            </td>
                                        </tr>
                                     @endforeach
                                     @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>

    
 <!-- Modal agregar -->
    <div class="container-modal close-modal" id="modal_cursos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Agregar curso</h2>
                <a href="#" id="close_form_cursos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                  <form action="{{route('storecursos')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                            <label for="fecha_inicio">Producto curso:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="producto" id="producto">
                                    @foreach($productos as  $producto)
                              
                                    <option name="{$producto['id']}}" value="{{$producto['id']}}">{{$producto['title']}}</option>
                                    @endforeach
                                </select>
                            </div>
                    </div>
           
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción de producto:</label>
                        <div class="input-group">
                            <textarea name="description" class="input-control" id="description" cols="30" rows="4">
                             {{$producto->description}}
                            </textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha de inicio:</label>
                            <div class="input-group">
                                <input type="date" class="input-control" name="class_start" id="class_start">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha de finalización:</label>
                            <div class="input-group">
                                <input type="date" class="input-control" name="class_end" id="class_end">
                            </div>
                        </div>
                    </div>
                 
                    
                    <div class="form-group">
                        <label for="fecha_inicio">Link(Classroom):</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="link" id="link">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">Meet(link):</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="meet" id="meet">
                        </div>
                    </div>

                     <div class="form-group">
                        <label for="fecha_inicio">Zoom(link):</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="zoom" id="zoom">
                        </div>
                    </div>
               
                    <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input  type="file" name="imagen" id="imagen" class="input-control" >
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection