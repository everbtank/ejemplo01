@extends('base')
@section('title', 'Docentes - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                        <div class="menu-list-content">
                                <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <div class="body-producto">
                    <div class="container-options write-comment">
                    <div class="header-modal">
                        <div class="row-group">
                            <div class="form-group">
                                <a href="{{ route('admin-productos') }}" >
                                    <h2>Anterior</h2>
                                </a>
                               
                            </div>
                            <div class="form-group">
                                 <h2>Editar Producto</h2>
                            </div>
                        </div>
                    </div>
                    <br>
                 
                     <form action="{{route('editproductos', ['id' => $producto->id])}}" method="POST">
                     @csrf
                     @method('PUT')
                    <div class="form-group">
                        <label for="nombre">Título de producto:</label>
                        <div class="input-group">
                            <input type="text" value="{{$producto->title}}" class="input-control" name="title" id="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción de producto:</label>
                        <div class="input-group">
                            <textarea  value="{{$producto->description}}"  name="description"  class="input-control" id="description" cols="30" rows="4">
                            {{$producto->description}}
                            </textarea>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de inicio:</label>
                        <div class="input-group">
                            <input type="date" value="{{$producto->inicio}}" class="input-control" name="inicio" id="inicio">
                        </div>
                    </div>
                  
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Precio:</label>
                            <div class="input-group">
                                <input type="text" value="{{$producto->precio}}" class="input-control" name="precio" id="precio">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Moneda:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" value="{{$producto->moneda}}" name="moneda" id="moneda">
                                    <option value="USD">USD</option>
                                    <option value="PEN">PEN</option>
                                    <option value="EUR">EUR</option>

                                </select>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Tipo:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="tipo" id="tipo">
                                    @foreach($tipos as  $tipo)
                                        <option name="{$tipo['id']}}" value="{{$tipo['id']}}">{{$tipo['tipo']}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Categoria:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="categoria" id="categoria">
                                @foreach($categorias as  $categoria)
                                
                                    <option name="{$categoria['id']}}" value="{{$categoria['id']}}">{{$categoria['categoria']}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                     <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Modalidad:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="modalidad" id="modalidad">
                                    @foreach($modalidades as  $modalidad)
                                
                                    <option name="{$modalidad['id']}}" value="{{$modalidad['id']}}">{{$modalidad['modalidad']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Nivel:</label>
                            <div class="input-group">
                                <select type="text" class="input-control" name="nivel" id="nivel">
                                    @foreach($niveles as  $nivel)
                                
                                    <option name="{$nivel['id']}}" value="{{$nivel['id']}}">{{$nivel['nivel']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="imagen">Subir imagen:</label>
                        <div class="input-group">
                            <input type="file" value="{{$producto->imagen}}" class="input-control" name="potho_url" id="potho_url">
                        </div>
                    </div>
                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
                        
                   
                </div>
            </div>
        </article>
    </main>
@endsection
