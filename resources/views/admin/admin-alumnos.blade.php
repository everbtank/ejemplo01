@extends('base')
@section('title', 'Alumnos - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                          <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Alumnos</h3>
                           
                            <br>
                             <form method="GET" action="{{route('admin-alumnos')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Foto</th>
                                            <th>Nombre</th>
                                            <th>Email</th>
                                            <th>Identificación</th>
                                            <th>fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @if(count($alumnos)<=0)
                                          <tr>
                                          <td colspan="8"> ho hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($alumnos as $alumno)
                                        <tr>
                                        
                                            <td>{{$alumno->id}}</td>
                                            <td>
                                                <figure>
                                                    <img src="{{asset($alumno->photo_url ?? '/images/avatar.png')}}" alt="">
                                                </figure>
                                            </td>
                                            <td>{{$alumno->name}}</td>
                                            <td>
                                                {{$alumno->email}}
                                            </td>
                                            <td>
                                               {{$alumno->identify}}
                                            </td>
                                            <td>
                                                {{$alumno->created_at}}
                                            </td>
    
                                           
                                        </tr>
                                      
                                     @endforeach
                                     @endif
                                    </tbody>

                                </table>
                               {{ $alumnos->links('vendor.pagination.default') }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>

@endsection