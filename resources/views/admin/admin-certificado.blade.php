@extends('base')
@section('title', 'Certificados - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                         <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="{{ route('admin-home') }}" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                  <li class="first-line">
                                    <a href="{{ route('admin-productos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Producto cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-cursos') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                 <li class="first-line">
                                    <a href="{{ route('admin-clases') }}" class="first-link">
                                        <i class="las la-table"></i>
                                        <span>Clases</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-aulavirtual') }}" class="first-link">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-contenidos') }}" class="first-link">
                                        <i class="las la-file"></i>
                                        <span>Contenido</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-certificados') }}" class="first-link">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-alumnos') }}" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumnos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="{{ route('admin-docentes') }}" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                        <div class="header-panel-admin">
                            <h3>Certificados</h3>
                             <a href="" class="btn btn-primary" id="open-productos-modal">
                                <i class="las la-plus-circle"></i>
                                <span>Añadir</span>
                            </a>
                            <br>
                             <form method="GET" action="{{route('admin-certificados')}}" class="form-admin">
                               <div class="search-input">
                                <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                                </div>
                            </form>
                        </div>
                        <div class="table-list-cotainer"> 
                            <div class="table-responsive" id="table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Nombre</th>
                                            <th>Escuela</th>
                                            <th>Dni</th>
                                            <th>Curso</th>
                                            <th>Code Curso</th>
                                            <th>Nota</th>
                                            <th>URL</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @if(count($certificados)<=0)
                                          <tr>
                                          <td colspan="8"> No hay resultados</td>
                                          </tr>
                                       @else
                                         @foreach($certificados as $certificado)
                                        <tr>
                                        
                                            <td>{{$certificado->id}}</td>
                                       
                                            <td>{{$certificado->student_name}}</td>
                                            <td>
                                                {{$certificado->school}}
                                            </td>
                                            <td>
                                                {{$certificado->student_dni}}
                                            </td>
                                            <td>
                                               {{$certificado->course_name}}
                                            </td>
                                            <td>
                                                {{$certificado->course_code}}
                                            </td>
                                            <td>
                                                {{$certificado->course_score}}
                                            </td>
                                             <td>
                                                <a href="{{$certificado->url_download}}">{{$certificado->url_download}}</a>
                                            </td>
                                            <td>
                                                <a href="{{route('editcertificados', ['id' => $certificado->id])}}" class="btn btn-primary">
                                             
                                                    <i class="las la-pen"></i>
                                                </a>
                                            </td>
                                            <td>
                                            <form action="{{route('destroycertificados', ['id' => $certificado->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger">
                                                    <i class="las la-trash-alt"></i>
                                                </button>
                                            </form>
                                            </td>
                                        </tr>
                                      
                                     @endforeach
                                     @endif
                                    </tbody>    
                                </table>
                               
                            </div>
                            {{ $certificados->links('vendor.pagination.default') }}
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </main>


 <!-- Modal agregar -->
    <div class="container-modal close-modal" id="modal_productos">
        <div class="modal container-options write-comment">
            <div class="header-modal">
                <h2>Agregar producto curso</h2>
                <a href="#" id="close_form_productos">
                    <i class="las la-times-circle"></i>
                </a>
            </div>
            <div class="body-modal">
                <form action="{{route('storecertificados')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="student_name" id="student_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Documento:</label>
                        <div class="input-group">
                            <input type="number" class="input-control" name="student_dni" id="student_dni" required>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Curso:</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="course_name" id="course_name" required>
                            <!-- <input type="text" class="input-control" name="nombre" id="nombre"> -->
                        </div>
                    </div>
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Codigo:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="course_code" id="course_code" required>
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="fecha_inicio">Instituto:</label>
                            <div class="input-group">
                               <select type="text" class="input-control" name="school" id="school" >
                                    <option value="Instituto itcenco">Instituto Itcenco</option>
                                    <option value="Eca peru">Eca peru</option>
                                    <option value="Cype peru">Cype peru</option>
                                    <option value="CIP">CIP</option>
                                </select>
                            </div>
                        </div>
                      
                    </div>

                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de finalizacion:</label>
                        <div class="input-group">
                            <input type="date" class="input-control" name="course_end" id="course_end" required>
                        </div>
                    </div>
                    
                    <div class="row-group">
                        <div class="form-group">
                            <label for="fecha_inicio">Nota total:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="course_score" id="course_score" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_inicio">Horas:</label>
                            <div class="input-group">
                                <input type="text" class="input-control" name="course_time" id="course_time"  required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fecha_inicio">Certificado virtual(URL):</label>
                        <div class="input-group">
                            <input type="text" class="input-control" name="url_download" id="url_download" required>
                        </div>
                    </div>

                    <div class="group-btn">
                        <button class="btn btn-primary" type="submit" >Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection