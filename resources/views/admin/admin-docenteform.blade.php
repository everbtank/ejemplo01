@extends('base')
@section('title', 'Docentes - Instituto Itcenco')
@section('body')
 <main id="main" >
        <article class="module-panel-admin">
            <div class="container">
                <div class="content-panel">
                    <div class="left-sidebar-menu">
                        <div class="top-title-panel">
                            <h3>Panel de control</h3>
                        </div>
                        <div class="menu-list-content">
                            <ul class="first-list">
                                <li class="first-line">
                                    <a href="admin-home" class="first-link">
                                        <i class="las la-chart-bar"></i>
                                        <span>Dashboard</span>
                                    </a>
                                    <ul class="d-none">

                                    </ul>
                                </li>
                                 <li class="first-line">
                                    <a href="admin-cursos" class="first-link">
                                        <i class="las la-books"></i>
                                        <span>Cursos</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="admin-aulavirtual" class="first-link ">
                                        <i class="las la-desktop"></i>
                                        <span>Aula virtual</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="admin-certificado" class="first-link active">
                                        <i class="las la-certificate"></i>
                                        <span>Certificado</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="admin-alumnos" class="first-link">
                                        <i class="las la-user-alt"></i>
                                        <span>Alumno</span>
                                    </a>
                                </li>
                                <li class="first-line">
                                    <a href="admin-docentes" class="first-link">
                                        <i class="las la-user-graduate"></i>
                                        <span>Docentes</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="body-panel-content">
                      
                       <div class="content-form-data">
                        <div class="header-profile">
                            <h3 class="dashboard-title">Agregar nuevo docente</h3>
                            <p>Para agregar a un nuevo docente rellener los campos y dale en guardar *</p>
                        </div>
                <div class="form-profile">
               <form action="{{ route('create-docenteform') }}"  method="POST">
                
                @csrf
                <div class="form-group">
                    <label for="full_name">Nombre completo</label>
                    <div class="row-input">
                        <input class="input-control" type="text" name="name" id="name" placeholder="Nombre completo">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>

                @error('name')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="email">Correo</label>
                    <div class="row-input">
                        <input class="input-control" type="email" name="email" id="email" placeholder="example@gmail.com">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>
                @error('email')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="email">Documento de identidad</label>
                    <div class="row-input">
                        <input class="input-control" id="identify" type="number" name="identify" placeholder="Ingrese documento de identidad">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>
                @error('identify')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <div class="row-input">
                        <input class="input-control" type="password" name="password" id="password" placeholder="*******">
                    </div>
                </div>
                @error('password')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="password">Contraseña Confirmar</label>
                    <div class="row-input">
                        <input class="input-control"  id="password-confirm" type="password" name="password_confirmation" placeholder="*******">
                    </div>
                </div>
              
                <div class="btn-group">
                    <button   type="submit" class="btn btn-primary animate-scale">Guardar</button>
                </div>
            </form>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>
@endsection