<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{__('Page Not Found')}}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
 
</head>

<body class="bg-white">
    <div class="flex flex-col md:flex-row items-center h-screen p-5 mx-5 md:mx-10">
        <div class="w-full flex justify-center py-5">
            <img src="/assets/wallpapers/error_404.gif" width=600px height=450px/>
        </div>
        <div class="w-full flex flex-col items-center py-5">
            <h1 class="font-black text-9xl text-gray-900">404</h1>
            <div class="w-5/6 space-y-2">
                <h4 class="text-gray-600 text-2xl font-bold pt-2">{{__('Sorry, the page you are looking for could not be found.')}}</h4>
                <p class="text-xl font-light">No te preocupes. Como usted es valioso para nosotros, lo devolveremos a un lugar seguro.</p>
            </div>
            <a class="pt-8" href="{{ app('router')->has('home') ? route('home') : url('/') }}">
                <button class="transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 bg-blue-600 hover:bg-red-600 text-white font-semibold py-3 px-8 rounded-md">{{ __('Go Home') }}</button>
            </a>
        </div>
    </div>

</body>
</html>