@extends('base')
@section('title', 'Nosotros - Instituto Itcenco')
@section('body')

 <main id="main" >
        <div class="list-section-blog">
            <div class="container">
                <h2 class="title-blog">Blog</h2>
                <div class="section-blog">

                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <!--<div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>
                    <div class="item-blog">
                        <a href="#">
                            <figure class="image-blog">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <h3>Estructuras de construcción</h3>
                            <p>El museo de Soumaya, se trata 
                                de un edificio plateado y 
                                brillante de 46 metros de altura, 
                                cubierto con más...</p>
                            <a href="#">Leer más</a>
                        </div>
                    </div>-->
                   

                </div>
            </div>
        </div>
    </main>
@endsection