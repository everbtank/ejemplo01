@extends('base')
@section('title', 'Nosotros - Instituto Itcenco')
@section('body')
<main id="main" >
        <div class="content-hero-image-event">
            <div class="container">
                <div class="content-event-blog">
                    <h2>Descubre los mejores eventos de
                        arquitectura, diseño y construcción </h2>
                </div>
            </div>
        </div>

        <div class="list-section-event">
            <div class="container">
                <h2 class="title-event">Próximos eventos</h2>
                <div class="section-event">
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción 2</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 diciembre</p>
                        </div>
                    </div>
                    <!--<div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>-->
                    
                </div>
                <div class="more-container">
                    <a href="#">Ver más</a>
                </div>
            </div>
        </div>
        <div class="list-section-event">
            <div class="container">
                <h2 class="title-event">Eventos anteriores</h2>
                <div class="section-event">
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <!--<div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>
                    <div class="item-event">
                        <a href="#">
                            <figure class="image-event">
                                <img src="{{ asset('assets/img/blog/blog.jpg')}}" alt="image blog">
                            </figure>
                        </a>
                        <div class="body-text">
                            <strong>Conferencia</strong>
                            <h3>Estructuras de construcción</h3>
                            <a href="#">Colegio de ingenieros</a>
                            <p>Viernes 13 julio</p>
                        </div>
                    </div>-->
                    
                </div>
                <div class="more-container">
                    <a href="#">Ver más</a>
                </div>
            </div>
        </div>
    </main>
@endsection