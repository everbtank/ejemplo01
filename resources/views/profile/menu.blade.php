@extends('base')
@section('title', 'Perfil - Itcenco')
@section('body')
<section class="text-gray-600 body-font bg-gray-50">
  <div class="container px-5 py-24 flex flex-wrap flex-col">
    <div class="mx-auto grid grid-cols-2 md:grid-cols-4 mb-10"><!--"flex mx-auto flex-col md:flex-row mb-10"-->
      <a href="{{route('profile')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium bg-gray-100 inline-flex items-center justify-center text-center leading-none border-indigo-500 text-indigo-500 tracking-wider rounded-t">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" />
        </svg>
        <p>TU PERFIL</p>
      </a>
      <a class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </svg>
        <p>TUS GRUPOS</p>
      </a>
      <a href="{{route('profile-password')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
        </svg>
        <p>CAMBIAR CONTRASEÑA</p>
      </a>
      <a href="{{route('profile-account')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <p>TU INFORMACION</p>
      </a>
    </div>    
    <div class="mx-auto text-gray-600 body-font">
      <div class="px-5 py-24">
        <div class="flex flex-col text-center w-full mb-20">
          <h1 class="text-2xl font-bold title-font mb-4 text-gray-900 tracking-widest">Tus Aulas Virtuales</h1>
          <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Se estan mostrando las aulas virtuales donde tienes acceso<br>Cualquier problema con tus aulas virtuales puedes ponerte en contacto con <a class="underline" href="{{route('contact')}}">Nosotros</a>.</p>
        </div>
        <div class="mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
          @foreach($classrooms as $classroom)
          <div class="mx-auto mt-8 max-w-sm rounded overflow-hidden shadow-lg place-self-start">
            @isset($classroom['classroom']['photo_url'])
            <a href="{{route('classroom', ['id' => $classroom['classroom_id']])}}" target="_blank">
              <img class="w-full object-cover object-center sm:mb-0 mb-4 bg-gradient-to-tr bg-gradient-to-bl bg-blue-600" src="{{asset($classroom['classroom']['photo_url'])}}" alt="">
            </a>
            @endisset
            <div class="px-6 py-4">
              <a href="{{route('classroom', ['id' => $classroom['classroom_id']])}}" target="_blank">
                <div class="font-bold text-xl mb-2 line-clamp-2">{{$classroom['classroom']['title']}}</div>
              </a>
              <p class="text-grey-darker text-base leading-normal line-clamp-5">{{$classroom['classroom']['description']}}</p>
            </div>
            <div class="flex justify-between items-center px-6 py-4">
              <div class="flex gap-2">
                <div class="flex bg-grey-lighter rounded-full px-2 py-1 text-sm font-semibold text-grey-darker select-none">
                  <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                  </svg>
                  <p class="font-medium"> {{count($classroom->classroom->users)}}</p>
                </div>
                <form method="POST" action="{{route('room-favorite', ['id' => $classroom['classroom_id']])}}" class="flex bg-grey-lighter rounded-full px-2 py-1 text-sm font-semibold text-grey-darker select-none">
                  @csrf
                  <button type="submit" class="focus:outline-none">
                    @if($classroom->classroom->isFavorite)
                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                    </svg>
                    @else
                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                    </svg>
                    @endif
                  </button>
                  <p class="font-medium"> {{count($classroom->classroom->favorites)}}</p>
                </form>
              </div>
              <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker select-none">{{ $classroom->classroom->created_at->diffInDays() > 30 ? $classroom->classroom->created_at->toFormattedDateString() : $classroom->classroom->created_at->diffForHumans() }}</span>
              
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@endsection