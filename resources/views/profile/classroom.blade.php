@extends('base')
@section('title', $classroom['title'])
@section('body')
<section class="p-2" style="background-image: url('/assets/background/dust_scratches.png');">
  <div class="container">
    <div class="p-4">
      <div class="flex flex-col w-full rounded-md border border-gray-300">

        @isset($classroom['photo_url'])
        <img class="w-full max-h-sm object-cover rounded-t-lg" src="{{asset($classroom['photo_url'])}}" alt="">
        @endisset
        
        <div class="flex flex-col rounded-b-lg p-4 items-center relative">
          <button onclick="toggleDropdown('content')" class="focus:outline-none flex flex-col items-center group">
            <span class="font-bold text-center text-lg md:text-2xl lg:text-3xl uppercase break-all sm:break-normal my-2 group-hover:text-gray-500">{{$classroom['title']}}</span>
            <svg class="h-6 w-6 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 13l-7 7-7-7m14-8l-7 7-7-7" />
            </svg>
          </button>
          <div id="content" class="p-3 bg-cover rounded-b-lg hidden">
            <p class="font-bold font-light font-mono text-xl whitespace-pre-wrap">{!! $classroom['description'] !!}</p>
            <div class="flex justify-between items-center py-4">
              <div class="flex gap-2">
                <div class="flex justify-center items-center px-2 py-1 text-sm font-semibold select-none">
                  <svg class="h-8 w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                  </svg>
                  <p class="font-medium"> {{count($classroom->users)}}</p>
                </div>
                <form method="POST" action="{{route('room-favorite', ['id' => $classroom['id']])}}" class="flex justify-center items-center px-2 py-1 text-sm font-semibold select-none">
                  @csrf
                  <button type="submit" class="focus:outline-none">
                    @if($classroom->isFavorite)
                    <svg class="h-8 w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                    </svg>
                    @else
                    <svg class="h-8 w-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                    </svg>
                    @endif
                  </button>
                  <p class="font-medium"> {{count($classroom->favorites)}}</p>
                </form>
              </div>
              <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-md font-semibold text-grey-darker select-none">{{ $classroom->created_at->diffInDays() > 30 ? $classroom->created_at->toFormattedDateString() : $classroom->created_at->diffForHumans() }}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="flex flex-col md:flex-row">
      <div class="w-full md:w-1/3">
        <div class="flex flex-col items-center p-3">
          <div class="p-2 w-full">
            <div class="border rounded-t-md border-gray-300">
              <div class="flex flex-col justify-center w-full border-b p-2 bg-blue-500 text-white space-y-2">
                <h2 class="text-sm font-semibold leading-none uppercase py-1 text-center">Clases En Vivo</h2>
                <span class="text-xs leading-none text-center py-1">Estos enlaces te llevaran a la clase en vivo.</span>
              </div>
              <div class="p-4 space-y-2">
                @isset($classroom['meet_url'])
                <a href="{{$classroom['meet_url']}}" target="_blank" class="flex items-center hover:text-blue-600">
                  <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 p-1.5 mr-2.5 rounded-lg border border-gray-200 dark:border-gray-800 fill-current text-green-400" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path d="M12 0C6.28 0 1.636 4.641 1.636 10.364c0 5.421 4.945 9.817 10.364 9.817V24c6.295-3.194 10.364-8.333 10.364-13.636C22.364 4.64 17.72 0 12 0zM7.5 6.272h6.817a1.363 1.363 0 0 1 1.365 1.365v1.704l2.728-2.727v7.501l-2.726-2.726v1.703a1.362 1.362 0 0 1-1.365 1.365H7.5c-.35 0-.698-.133-.965-.4a1.358 1.358 0 0 1-.4-.965V7.637A1.362 1.362 0 0 1 7.5 6.272z"/></svg>
                  Google Meet
                </a>
                @endisset
                @isset($classroom['zoom_url'])
                <a href="{{$classroom['zoom_url']}}" target="_blank" class="flex items-center hover:text-blue-600">
                  <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 p-1.5 mr-2.5 rounded-lg border border-gray-200 dark:border-gray-800" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none"><path d="M0 8a8 8 0 0 1 8-8h8a8 8 0 0 1 8 8v8a8 8 0 0 1-8 8H8a8 8 0 0 1-8-8V8z" fill="#3984FD"/><path d="M5 9a1 1 0 0 1 1-1h6a3 3 0 0 1 3 3v4a1 1 0 0 1-1 1H8a3 3 0 0 1-3-3V9z" fill="#fff"/><path d="M15.5 11.752a2 2 0 0 1 .495-1.318l1.69-1.932c.457-.52 1.315-.198 1.315.494v6.008c0 .693-.858 1.015-1.314.494l-1.691-1.932a2 2 0 0 1-.495-1.317v-.498z" fill="#fff"/></g></svg>
                  Google Zoom
                </a>
                @endisset
                @isset($classroom['mstm_url'])
                <a href="{{$classroom['mstm_url']}}" target="_blank" class="flex items-center hover:text-blue-600">
                  <svg class="w-8 h-8 p-1.5 mr-2.5 rounded-lg border border-gray-200 dark:border-gray-800" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                  width="100" height="100"
                  viewBox="0 0 48 48"
                  style=" fill:#000000;"><path fill="#5059c9" d="M44,22v8c0,3.314-2.686,6-6,6s-6-2.686-6-6V20h10C43.105,20,44,20.895,44,22z M38,16  c2.209,0,4-1.791,4-4c0-2.209-1.791-4-4-4s-4,1.791-4,4C34,14.209,35.791,16,38,16z"></path><path fill="#7b83eb" d="M35,22v11c0,5.743-4.841,10.356-10.666,9.978C19.019,42.634,15,37.983,15,32.657V20h18  C34.105,20,35,20.895,35,22z M25,17c3.314,0,6-2.686,6-6s-2.686-6-6-6s-6,2.686-6,6S21.686,17,25,17z"></path><circle cx="25" cy="11" r="6" fill="#7b83eb"></circle><path d="M26,33.319V20H15v12.657c0,1.534,0.343,3.008,0.944,4.343h6.374C24.352,37,26,35.352,26,33.319z" opacity=".05"></path><path d="M15,20v12.657c0,1.16,0.201,2.284,0.554,3.343h6.658c1.724,0,3.121-1.397,3.121-3.121V20H15z" opacity=".07"></path><path d="M24.667,20H15v12.657c0,0.802,0.101,1.584,0.274,2.343h6.832c1.414,0,2.56-1.146,2.56-2.56V20z" opacity=".09"></path><linearGradient id="DqqEodsTc8fO7iIkpib~Na_zQ92KI7XjZgR_gr1" x1="4.648" x2="23.403" y1="14.648" y2="33.403" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#5961c3"></stop><stop offset="1" stop-color="#3a41ac"></stop></linearGradient><path fill="url(#DqqEodsTc8fO7iIkpib~Na_zQ92KI7XjZgR_gr1)" d="M22,34H6c-1.105,0-2-0.895-2-2V16c0-1.105,0.895-2,2-2h16c1.105,0,2,0.895,2,2v16  C24,33.105,23.105,34,22,34z"></path><path fill="#fff" d="M18.068,18.999H9.932v1.72h3.047v8.28h2.042v-8.28h3.047V18.999z"></path></svg>
                  Team Microsoft
                </a>
                @endisset
              </div>
            </div>
            @if(isset($classroom['class_time']) || isset($classroom['class_start']))
            <div class="mt-2 border rounded-t-md border-gray-300">
              <div class="flex flex-col justify-center w-full border-b py-4 bg-blue-500 text-white space-y-2">
                <h2 class="text-sm font-semibold leading-none uppercase text-center">Horario de Clases</h2>
                @isset($classroom['class_start'])
                <span class="text-xs leading-none text-center">Inicio de clase: {{$classroom['class_start']}}</span>
                @endisset
              </div>
              <div class="p-4 space-y-2">
                <a class="whitespace-pre-wrap">{{$classroom['class_time']}}</a>
              </div>
            </div>
            @endif
            <div class="mt-2 border rounded-t-md border-gray-300">
              <div class="flex items-center justify-center w-full h-16 px-4 border rounded-t-md border-gray-300 bg-blue-500 text-white">
                <h2 class="text-sm font-semibold leading-none uppercase text-center pt-2">Clases grabadas y material de trabajo</h2>
              </div>
              <div class="flex z-10 flex-col items-start w-full shadow-lg">
                @foreach($classroom->lessons as $lesson)
                <a class="w-full px-4 py-3 text-left hover:bg-blue-600 hover:text-white" target="_blank" href="{{$lesson['lesson_url']}}">{{$lesson['lesson_name']}}</a>
                @endforeach
              </div>
            </div>

            <div class="flex flex-col p-2 w-full space-y-2 rounded-md bg-gray-100">
              <span class="font-medium uppercase text-center border-b-2 border-gray-800 px-2 my-2">
                Tus Aulas virtuales
              </span>
              @forelse($classroom->recomendations(Auth::user()->email, $classroom->id) as $menu)
              <div class="flex flex-nowrap shadow-md rounded hover:shadow-lg hover:bg-blue-600 items-center group">
                <img class="w-14 h-14 flex-shrink bg-cover rounded-l md:bg-center" src="{{ asset($menu->classroom->photo_url) }}">
                <a href="{{route('classroom', ['id' => $menu->classroom->id])}}" class="px-2 line-clamp-2 font-light text-gray-800 group-hover:text-white text-lg font-serif cursor-pointer">{{$menu->classroom->title}}</a>
              </div>
              @empty
              <div class="flex flex-nowrap shadow-md rounded hover:shadow-lg hover:bg-blue-600 items-center group">
                <a class="p-2 line-clamp-2 font-light text-gray-800 group-hover:text-white text-lg font-serif cursor-not-allowed">No tenemos recomendaciones para ti.</a>
              </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
      <div class="w-full md:w-2/3">
        <div class="flex flex-col items-center p-3">
          @if(Auth::user()->hasRole('admin'))
          <div class="flex flex-col rounded-md border border-gray-100 w-full py-4 px-6">
            <span class="pb-2 font-serif text-xl">Publicar un Contenido</span>
            <div class="border border-gray-300 bg-gray-200 rounded">
              <form method="POST" action="{{ route('room-create-post', ['id' => $classroom['id']]) }}" enctype="multipart/form-data">
                @csrf
                <div class="flex flex-col p-4">
                  <input class="py-3 px-3 text-gray-700 border border-gray-300 bg-gray-100 rounded-t-md w-full" type="text" value="{{ old('title') }}" autocomplete="off" name="title" placeholder="TITULO" required="true">
                  @error('title')
                  <p class="text-red-500 p-2 font-light">{{$message}}</p>
                  @enderror
                  <textarea class="py-3 px-3 text-gray-700 border border-gray-300 bg-gray-100 rounded-b-md w-full" value="{{ old('body') }}" name="body" placeholder="CONTENIDO DEL MENSAJE" cols="" rows="4" required="true"></textarea>
                  @error('body')
                  <p class="text-red-500 p-2 font-light">{{$message}}</p>
                  @enderror
                  @error('file')
                  <p class="text-red-500 p-2 font-light">{{$message}}</p>
                  @enderror
                  @if(session('status') == 1)
                  <p class="text-green-500 p-2 font-light">{{ __('El contenido fue eliminado correctamente.') }}</p>
                  @endif
                </div>
                <div class="flex flex-col md:flex-row border-t border-gray-300 rounded-b justify-center md:justify-between">
                  <div class="bg-gray-200 py-2 md:px-5 w-full md:w-2/3 truncate">
                    <input type="file" name="files[]" multiple="multiple" class="text-gray-700">
                  </div>
                  <button type="submit" class="flex flex-row justify-center border-l border-gray-300 items-center p-2 bg-blue-600 hover:bg-blue-500 text-white w-full md:w-1/3">
                    <span class="text-sm font-semibold leading-none uppercase text-center">Publicar</span>
                    <i class="flex-shrink flex items-center justify-center h-6 w-6">
                      <svg class="h-4 w-4 transform rotate-90 fill-current text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                      </svg>
                    </i>
                  </button>
                </div>
              </form>
            </div>
          </div>
          @endif
          <div class="p-4 w-full">
            <div class="flex flex-col flex-grow justify-center space-y-2">
              @foreach($classroom->posts as $post)
              <div class="border rounded-t border-gray-300">
                <div class="relative flex items-center justify-between w-full h-16 px-4 border rounded-t-md border-gray-300 hover:bg-blue-600 hover:text-white">
                  <button class="absolute left-0 flex items-stretch p-4 focus:outline-none" onclick="toggleDropdown('{{$post['id']}}')">
                    <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                    </svg>
                  </button>
                  <h2 class="text-sm font-semibold leading-none uppercase text-center px-8 py-2">{{ $post['title'] }}</h2>

                  @if(Auth::user()->hasRole('admin'))
                  <form class="absolute right-0 flex items-stretch mr-8" method="POST" action="{{ route('room-remove-post', ['id' => $post['id']]) }}">
                    @csrf
                    <button class="text-sm font-semibold focus:outline-none"><i class="fa fa-trash fa-lg hover:text-gray-600" aria-hidden="true"></i></button>
                  </form>
                  @endif
                </div>
                <div class="border-t border-gray-700 hidden" id="{{$post['id']}}">
                  <div class="flex flex-col md:flex-row md:justify-between items-center p-4">
                    <div class="rounded-full text-xs uppercase font-semibold text-center">
                      Fecha de publicación
                    </div>
                    <div class="text-xs font-semibold text-center">
                      <p>{{ $post->created_at->diffInDays() > 30 ? $post->created_at->toFormattedDateString() : $post->created_at->diffForHumans() }}</p>
                    </div>
                  </div>
                  <div class="flex flex-col">
                    <div class="text-xs bg-gray-100 rounded px-1 py-4 font-semibold font-sans">
                      <p class="pl-4 text-sm whitespace-pre-wrap break-words">{!! $post['body'] !!}</p>
                    </div>
                    @if(count($post->files) > 0)
                    <div class="flex flex-col px-1 py-2 px-4 space-y-2">
                      @foreach($post->files as $file)
                      @if($file['file_path'] && in_array(strtolower(pathinfo($file['file_path'], PATHINFO_EXTENSION)), ['gif', 'jpg', 'jpeg', 'png']))
                      <div class="flex items-center justify-center h-32 md:h-64 bg-cover bg-center" style="background-image: url('{{asset($file['file_path'])}}');">
                      </div>
                      @elseif($file['file_path'])
                      <a href="/download/{{$file['id']}}/{{$file['uuid']}}">
                        <p class="text-blue-600 text-sm font-arial shadow-lg px-3 border border-1 border-blue-500 rounded-sm hover:bg-blue-600 hover:text-white py-1 w-auto"><i class="fa fa-cloud-download" aria-hidden="true"></i>{{$file['file_name']}}</p>
                      </a>
                      @endif
                      @endforeach
                    </div>
                    @endif
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          <div class="p-4 w-full">
            <div class="flex flex-col rounded-md border border-gray-100 w-full space-y-2 py-4">
              <span class="font-serif text-xl">Comentarios</span>
              <form method="POST" action="{{route('room-create-message', ['id' => $classroom['id']])}}">
                @csrf
                <div class="flex border-2 border-gray-300 rounded-sm p-1">
                  <img class="h-10 w-10 rounded flex-shrink-0 bg-gray-300 bg-cover" src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}">
                  <textarea class="w-full text-sm px-3 border-b resize-none mx-2" name="message" rows="1"></textarea>
                  <div class="flex items-center">
                    <button type="submit" class="flex-shrink flex items-center justify-center h-6 w-6 rounded hover:bg-blue-600 hover:text-white">
                      <svg class="h-4 w-4 transform rotate-90" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                      </svg>
                    </button>
                  </div>
                </div>
              </form>
              <div class="flex flex-col overflow-auto max-h-screen border border-gray-300 rounded shadow-lg">
                @forelse($classroom->chats->sortByDesc('created_at') as $chat)
                @if($chat->user)
                <div class="flex p-4 relative">
                  <img class="h-10 w-10 rounded flex-shrink-0 bg-gray-300 bg-cover" src="{{asset($chat->user->photo_url ?? '/images/avatar.png')}}">
                  <div class="ml-2">
                    <div class="-mt-1">
                      <span class="text-sm font-semibold">{{$chat->user->name}}</span>
                      <span class="ml-1 text-xs text-gray-500">{{$chat->created_at->diffInDays() > 30 ? $chat->created_at->toFormattedDateString() : $chat->created_at->diffForHumans() }}</span>
                    </div>
                    <p class="text-sm break-all">{{$chat->body}}</p>
                  </div>
                  @if(Auth::user()->hasRole('admin') || Auth::user()->id == $chat->user_id)
                  <div class="absolute inset-y-0 right-0 mr-4 mt-2">
                    <button class="text-gray-600 hover:text-gray-800 focus:outline-none" onclick="toggleDropdown('msg-{{$chat->id}}')">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical w-4 h-4"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                    </button>
                  </div>
                  <div class="absolute right-0 flex flex-col bg-white py-2 px-4 mr-4 mt-3 rounded shadow-lg hidden" id="msg-{{$chat->id}}">
                    <form method="POST" action="{{route('room-remove-message', ['id' => $chat['id']])}}">
                      @csrf
                      <button class="flex flex-row items-center focus:outline-none hover:text-gray-500">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4 mr-2">
                          <polyline points="3 6 5 6 21 6"></polyline>
                          <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                        </svg>
                        <p class="font-light font-serif">Eliminar</p>
                      </button>
                    </form>
                  </div>
                  @endif
                </div>
                @endif
                @empty
                <h2 class="text-sm font-semibold leading-none text-center p-5">No hay comentarios, sé el primero en dejar un comentario.</h2>
                @endforelse
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection