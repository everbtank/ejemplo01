@extends('base')
@section('title', 'Perfil - Itcenco')
@section('body')
<section class="text-gray-600 body-font bg-gray-50">
  <div class="container px-5 py-24 flex flex-wrap flex-col">
    <div class="mx-auto grid grid-cols-2 md:grid-cols-4 mb-10">
      <a href="{{route('profile')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" />
        </svg>
        <p>TU PERFIL</p>
      </a>
      <a class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
        </svg>
        <p>TUS GRUPOS</p>
      </a>
      <a href="{{route('profile-password')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium bg-gray-100 inline-flex items-center justify-center text-center leading-none border-indigo-500 text-indigo-500 tracking-wider rounded-t">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
        </svg>
        <p>CAMBIAR CONTRASEÑA</p>
      </a>
      <a href="{{route('profile-account')}}" class="sm:px-5 py-3 w-full sm:justify-start border-b-2 title-font font-medium inline-flex items-center justify-center text-center leading-none border-gray-200 hover:text-gray-900 tracking-wider">
        <svg class="w-5 h-5 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <p>TU INFORMACION</p>
      </a>
    </div>
    <form class="container w-full max-w-lg" method="POST" action="{{route('user-password.update')}}">
      @csrf
      @method('PUT')
      <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full px-3">
          <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="current_password">
            Contraseña Actual
          </label>
          <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded-full py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" value="" name="current_password" type="password" required="true" placeholder="Contraseña" autocomplete="off">
          @error('current_password', 'updatePassword')
          <p class="text-red-500 text-xs italic">{{$message}}</p>
          @enderror
        </div>
      </div>
      <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full px-3">
          <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            Nueva Contraseña
          </label>
          <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded-full py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" value="" name="password" type="password" required="true" placeholder="Nueva Contraseña" autocomplete="off">
          @error('password', 'updatePassword')
          <p class="text-red-500 text-xs italic">{{$message}}</p>
          @enderror
        </div>
      </div>
      <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full px-3">
          <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            Confirmar Contraseña
          </label>
          <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded-full py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" value="" type="password" required="true" placeholder="Confirmar Contraseña" autocomplete="off">
          @if(session('status') == 'password-updated')
          <p class="text-green-500 text-xs italic">{{__('La contraseña se ha cambiado.')}}</p>
          @endif
        </div>
      </div>
      <div class="w-full px-3 mb-6 md:mb-0">
        <div class="flex flex-wrap w-full -mx-3 mb-6">
          <button class="shadow-none bg-blue-700 hover:bg-blue-400 focus:shadow-outline focus:outline-none text-white font-bold py-4 px-10 rounded-full" type="submit">
            Actualizar
          </button>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection