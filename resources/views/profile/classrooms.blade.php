@extends('base')
@section('title', 'Aula virtual - Itcenco')
@section('body')
<div class="bg-gray-50">
    <div class="container font-sans leading-tight min-h-screen bg-grey-lighter p-8">
        <div class="mx-auto text-gray-600 body-font">
            <div class="px-5 py-24">
                <div class="flex flex-col text-center w-full mb-20">
                    <h1 class="text-2xl font-bold title-font mb-4 text-gray-900 tracking-widest">Tus Aulas Virtuales</h1>
                    <p class="lg:w-2/3 mx-auto leading-relaxed text-base">Se estan mostrando las aulas virtuales donde tienes acceso<br>Cualquier problema con tus aulas virtuales puedes ponerte en contacto con <a class="underline" href="{{route('contact')}}">Nosotros</a>.</p>
                </div>
                <div class="mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5">
                    @foreach($classrooms as $classroom)
                    <div class="mx-auto mt-8 max-w-sm rounded overflow-hidden shadow-lg place-self-start">
                        @isset($classroom['classroom']['photo_url'])
                        <a href="{{route('classroom', ['id' => $classroom['classroom_id']])}}" target="_blank">
                            <img class="w-full object-cover object-center sm:mb-0 mb-4 bg-gradient-to-tr bg-gradient-to-bl bg-blue-600" src="{{asset($classroom['classroom']['photo_url'])}}" alt="">
                        </a>
                        @endisset
                        <div class="px-6 py-4">
                            <a href="{{route('classroom', ['id' => $classroom['classroom_id']])}}" target="_blank">
                                <div class="font-bold text-xl mb-2 line-clamp-2">{{$classroom['classroom']['title']}}</div>
                            </a>
                            <p class="text-grey-darker text-base leading-normal line-clamp-5">{{$classroom['classroom']['description']}}</p>
                        </div>
                        <div class="flex justify-between items-center px-6 py-4">
                            <div class="flex gap-2">
                                <div class="flex bg-grey-lighter rounded-full px-2 py-1 text-sm font-semibold text-grey-darker select-none">
                                    <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                                    </svg>
                                    <p class="font-medium"> {{count($classroom->classroom->users)}}</p>
                                </div>
                                <form method="POST" action="{{route('room-favorite', ['id' => $classroom['classroom_id']])}}" class="flex bg-grey-lighter rounded-full px-2 py-1 text-sm font-semibold text-grey-darker select-none">
                                    @csrf
                                    <button type="submit" class="focus:outline-none">
                                        @if($classroom->classroom->isFavorite)
                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clip-rule="evenodd" />
                                        </svg>
                                        @else
                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                                        </svg>
                                        @endif
                                    </button>
                                    <p class="font-medium"> {{count($classroom->classroom->favorites)}}</p>
                                </form>
                            </div>
                            <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker select-none">{{ $classroom->classroom->created_at->diffInDays() > 30 ? $classroom->classroom->created_at->toFormattedDateString() : $classroom->classroom->created_at->diffForHumans() }}</span>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection