@extends('base')
@section('title', 'Registro  - Instituto Itcenco')
@section('body')

<main id="main">
    <div class="container-access">
        <div class="header-access">
            <figure>
                <img src="{{ asset('assets/img/icono.png')}}" alt="logo">
            </figure>
            <h3>Registrarme</h3>
        </div>
        <div class="body-access">
            <form action="{{ route('register') }}"  method="POST">
                @csrf
                <div class="form-group">
                    <label for="full_name">Nombre completo</label>
                    <div class="row-input">
                        <input class="input-control" type="text" name="name" id="name" placeholder="Nombre completo">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>

                @error('name')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="email">Correo</label>
                    <div class="row-input">
                        <input class="input-control" type="email" name="email" id="email" placeholder="example@gmail.com">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>
                @error('email')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="email">Documento de identidad</label>
                    <div class="row-input">
                        <input class="input-control" id="identify" type="number" name="identify" placeholder="Ingrese documento de identidad">
                        <!-- <span class="text--error">* correo no válido</span> -->
                    </div>
                </div>
                @error('identify')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <div class="row-input">
                        <input class="input-control" type="password" name="password" id="password" placeholder="*******">
                    </div>
                </div>
                @error('password')
                <div class="content-errors">
                    <span class="text--error">{{$message}}</span>
                </div> 
                @enderror

                <div class="form-group">
                    <label for="password">Contraseña Confirmar</label>
                    <div class="row-input">
                        <input class="input-control"  id="password-confirm" type="password" name="password_confirmation" placeholder="*******">
                    </div>
                </div>
              
                <div class="btn-group">
                    <button   type="submit" class="btn btn-primary animate-scale">Registrarme</button>
                </div>
            </form>
            <div class="content-separate-option">
                <hr>
                <span>Ó</span>
                <hr>
            </div>
            <div class="content-buttons">
                <a href="{{ route('google') }}" class="social-button">
                    <figure>
                        <img src="{{ asset('assets/img/google.pn')}}g" alt="google">
                    </figure>
                    <span>Registrarme con Google</span>
                </a>
                <a href="{{ route('facebook') }}" class="social-button">
                    <figure>
                        <img src="{{ asset('assets/img/facebook_')}}access.png" alt="facebook">
                    </figure>
                    <span>Registrarme con Facebook</span>
                </a>
            </div>
            <div class="text-options">
                <p> Ya tengo una cuenta <a href="{{route('login')}}" class="color-main">Iniciar sesión</a> </p>
            </div>
        </div>
    </div>
</main>
@endsection