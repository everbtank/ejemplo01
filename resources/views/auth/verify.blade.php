@extends('base')
@section('title', 'Verificacion - Itcenco')
@section('body')
<div class="bg-gray-50">
  <div class="container sm:max-w-sm w-full">
    <section class="pt-20 pb-48">
      <div class="container mx-auto px-4">
        <div class="flex flex-wrap justify-center text-center py-4 border border-solid border-gray-200">
          <div class="w-full lg:w-6/12 px-4">
            @if (session('status') == 'verification-link-sent')
            <div class="block text-sm text-green-600 bg-green-200 border border-green-400 flex items-center m-4 rounded-sm p-1" role="alert">
              {{__('Se ha enviado un nuevo enlace de verificación a la dirección de correo electrónico que proporcionó durante el registro.')}}
              <button type="button" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.remove();">
                <span class="absolute top-0 bottom-0 right-0 text-1xl px-3 py-1 hover:text-green-900" aria-hidden="true" >×</span>
              </button>
            </div>
            @endif
            <h2 class="text-4xl font-semibold">¡Gracias por registrarte!</h2>
            <p class="text-lg leading-relaxed m-4 text-gray-600">Antes de comenzar, ¿podría verificar su dirección de correo electrónico haciendo clic en el enlace que le acabamos de enviar? Si no recibió el correo electrónico, con gusto le enviaremos otro.</p>
            <form action="{{ route('verification.send') }}" method="POST">
              @csrf 
              <div class="flex items-center justify-center">
                <button type="submit" class="flex items-center justify-center focus:outline-none text-white text-sm sm:text-base bg-blue-600 hover:bg-blue-700 rounded p-2  transition duration-150 ease-in">
                  <span class="mr-2 uppercase">Reenviar codigo</span>
                  <span>
                    <svg class="h-6 w-6 animate-pulse" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                      <path d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                  </span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
@endsection