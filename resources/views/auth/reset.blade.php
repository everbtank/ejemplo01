@extends('base')
@section('title', 'Cambiar Contraseña - Instituto Itcenco')
@section('body')

<div class="bg-gray-50">
  <div class="container sm:max-w-sm w-full">
    <div class="py-5">
      <div class="flex items-center justify-center">
        <div class="bg-transparent border border-solid border-gray-200 px-12 py-8 rounded" type="button" style="transition: all .15s ease">
          <div class="items-center">
            <form action="{{ route('password.update') }}" method="POST">
              @csrf
              <input type="hidden" name="token" value="{{ $request->route('token') }}">
              @if (session('status'))
              <div class="block text-sm text-green-600 bg-green-200 border border-green-400 h-6 my-1 flex items-center p-4 rounded-sm relative" role="alert">{{ session('status') }}
                <button type="button" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.remove();">
                  <span class="absolute top-0 bottom-0 right-0 text-1xl px-3 py-1 hover:text-green-900" aria-hidden="true" >×</span>
                </button>
              </div>
              @endif

              @error('email')
              <div class="block text-sm text-red-600 bg-red-200 border border-red-400 h-6 my-1 flex items-center p-4 rounded-sm relative" role="alert">{{$message}}
                <button type="button" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.remove();">
                  <span class="absolute top-0 bottom-0 right-0 text-1xl px-3 py-1 hover:text-red-900" aria-hidden="true" >×</span>
                </button>
              </div>
              @enderror
              <input id="email" type="email" name="email" value="{{ $request->email }}" required autocomplete="email" placeholder="" class="hidden px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-full pr-10 @error('email') border border-red-400 text-red-600 @enderror"/>

              <label for="password" class="mb-1 text-xs sm:text-sm tracking-wide text-gray-600">Nueva Contraseña:</label>
              @error('password')
              <div class="block text-sm text-red-600 bg-red-200 border border-red-400 h-6 my-1 flex items-center p-4 rounded-sm relative" role="alert">{{$message}}
                <button type="button" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.remove();">
                  <span class="absolute top-0 bottom-0 right-0 text-1xl px-3 py-1 hover:text-red-900" aria-hidden="true" >×</span>
                </button>
              </div>
              @enderror
              <div class="relative flex w-full flex-wrap items-stretch mb-3">
                <input id="password" type="password" name="password" required autocomplete="new-password" placeholder="Contraseña" class="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-full pr-10 @error('password') border border-red-400 text-red-600 @enderror"/>
                <span class="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent rounded text-base items-center justify-center w-8 right-0 pr-3 py-3 @error('password') text-red-600 @enderror">
                  <i class="fa fa-key"></i>
                </span>
              </div>

              <label for="password_confirmation" class="mb-1 text-xs sm:text-sm tracking-wide text-gray-600">Repetir contraseña:</label>
              <div class="relative flex w-full flex-wrap items-stretch mb-3">
                <input id="password_confirmation" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="Repetir contraseña" class="px-3 py-3 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow-outline w-full pr-10 @error('password') border border-red-400 text-red-600 @enderror"/>
                <span class="z-10 h-full leading-snug font-normal absolute text-center text-gray-400 absolute bg-transparent rounded text-base items-center justify-center w-8 right-0 pr-3 py-3 @error('password') text-red-600 @enderror">
                  <i class="fa fa-key"></i>
                </span>
              </div>

              <div class="flex w-full">
                <button type="submit" class="flex items-center justify-center focus:outline-none text-white text-sm sm:text-base bg-blue-600 hover:bg-blue-700 rounded p-2 transition duration-150 ease-in">
                  <span class="mr-2 uppercase">Cambiar contraseña</span>
                  <span>
                    <svg class="h-6 w-6 animate-pulse" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                      <path d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                  </span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection