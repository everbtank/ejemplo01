@extends('base')
@section('title', 'Iniciar Sesión - Instituto Itcenco')
@section('body')
    <main id="main">
        <div class="container-access">
            <div class="header-access">
                <figure>
                    <img src="{{ asset('assets/img/icono.png')}}" alt="logo">
                </figure>
                <h3>Inicio de sesión</h3>
            </div>
            <div class="body-access">
                <form action="{{ route('login') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="email">Correo</label>
                        <div class="row-input">
                            <input class="input-control" type="email" name="email" id="email" placeholder="example@gmail.com">
                            <!-- <span class="text--error">* correo no válido</span> -->
                        </div>
                    </div>
                    @error('email')
                    <div class="content-errors">
                        <span class="text--error">{{$message}}</span>
                    </div>
                    @enderror

                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <div class="row-input">
                            <input class="input-control" type="password" name="password" id="password" placeholder="*******">
                        </div>
                    </div>

                    @error('password')
                    <div class="content-errors">
                        <span class="text--error">{{$message}}</span>
                    </div>
                    @enderror
                    <div class="btn-group">
                        <button class="btn btn-primary animate-scale">Iniciar sesión</button>
                    </div>
                </form>
                <div class="content-separate-option">
                    <hr>
                    <span>O</span>
                    <hr>
                </div>
                <div class="content-buttons">
                    <a href="{{ route('google') }}" class="social-button">
                        <figure>
                            <img src="{{ asset('assets/img/google.png')}}" alt="google">
                        </figure>
                        <span>Iniciar con Google</span>
                    </a>
                    <a href="{{ route('facebook') }}" class="social-button">
                        <figure>
                            <img src="{{ asset('assets/img/facebook_access.png')}}" alt="facebook">
                        </figure>
                        <span>Iniciar con Facebook</span>
                    </a>
                </div>
                <div class="text-options">
                    <p> ¿Aun no tienes cuenta? <a href="registro" class="color-main">Registrate</a> </p>
                    <a href="{{route('forgot')}}" class="color-main">Olvidé mi contraseña</a>
                </div>
            </div>
        </div>
    </main>
@endsection