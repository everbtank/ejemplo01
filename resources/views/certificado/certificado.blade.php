@extends('base')
@section('title', 'Iniciar Sesión - Instituto Itcenco')
@section('body')
<main id="main" >
    <section class="section-search-certificate">
        <div class="container">
            <div class="content-search-certificate">
                <div class="title-content">
                    <h3 class="text--title-section">Consulta en linea tu certificado o diploma</h3>
                    <p class="text--info-section">
                        Verifica si cuentas con un certificado en Ecca Perú
                    </p>
                </div>
                <div class="search-input">
                    <form method="GET" action="{{route('certification')}}" class="search--submit">
                        <select class="select-school" name="school" id="school">
                            <option value="Instituto Itcenco">Instituto Itcenco</option>
                            <option value="Ecca Peru">Ecca Perú</option>
                            <option value="Cype Peru">Cype Perú</option>
                            <option value="CIP">CIP</option>
                        </select>
                        <div class="input-icon">
                            <figure>
                                <img src="{{asset("assets/img/search.png")}}" alt="search icon">
                            </figure>
                            <input type="search" name="search" id="search" placeholder="Ingrese su DNI" class="input-control">
                        </div>
                        <button class="btn btn-primary animate-scale" type="submit" >Buscar</button>
                    </form>
                </div>
                <div class="table-list-cotainer">
                    @isset($certificados)
                    @foreach($certificados as $certificado)
                    <div class="table-list-header">
                   
                        <div class="left-header">
                            <figure>
                                <img src="{{asset('assets/img/sombrero.png')}}" alt="">
                            </figure>
                            <p>{{$certificado->student_name}}</p>
                        </div>
                        <div class="right-header">
                            <p>Documento de identidad:</p>
                            <span>{{$certificado->student_dni}}</span>
                        </div>
                    </div>
                    @endforeach
                    @endisset

                    <div class="table-responsive" id="table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Code</th>
                                    <th>Curso</th>
                                    <th>Duración</th>
                                    <th>Nota</th>
                                    <th>Download</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                @isset($certificados)
                                    @foreach($certificados as $certificado)
                                        <tr>
                                            <td>{{$certificado->id}}</td>
                                            <td>{{$certificado->course_code}}</td>
                                            <td>{{$certificado->course_name}}</td>
                                            <td>{{$certificado->course_time}}</td>
                                            <td>{{$certificado->course_score}}</td>
                                            <td>
                                                <a href="{{$certificado->url_download}}" class="btn btn-primary">
                                                    <i class="las la-download"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8"> No hay resultados</td>
                                    </tr>
                                @endisset
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>            
    </section>
</main>
@endsection
