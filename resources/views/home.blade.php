@extends('base')
@section('title', 'Instituto Itcenco')
@section('body')
<main id="main" >
        <section class="hero-image-container color-light-01">
            <div class="container">
                <div class="hero-image">
                    <div class="hero--top">
                        <div class="left">
                            <h3 class="title">Capacitate y 
                                Especializate</h3>
                            <p class="text">Logra una certificación internacional en los 
                                cursos y diplomas de mayor demanda.
                                </p>
                            <a href="{{route('register')}}" class="btn btn-primary">Comienza ahora</a>
                        </div>
                        <div class="right">
                            <figure>
                                <img src="{{ asset('assets/img/hero.png')}}" alt="">
                            </figure>
                        </div>
                    </div>
                    <div class="hero--button">
                        <div class="left">
                            <div class="list-profiles">
                                <figure>
                                    <img src="{{ asset('assets/img/estudiantes/estudiante1.jpeg')}}" alt="">
                                </figure>
                                <figure>
                                    <img src="{{ asset('assets/img/estudiantes/estudiante3.jpeg')}}" alt="">
                                </figure>
                            </div>
                            <p class="text--info">Somos más de <span>2350+</span> alumnos</p>
                        </div>
                        <div class="right">
                            <div class="social-icons">
                                <a href="https://www.youtube.com/channel/UC5kBsOigJAA2V2EjjHYD9hw" class="icon animate-scale">
                                    <img src="{{ asset('assets/img/youtube.svg')}}" alt="">
                                </a>
                                <a href="https://www.instagram.com/instituto_itcenco/?hl=es-la" class="icon animate-scale">
                                    <img src="{{ asset('assets/img/instagram.svg')}}" alt="">
                                </a>
                                <a href="https://www.facebook.com/itcenco/" class="icon animate-scale">
                                    <img src="{{ asset('assets/img/facebook.svg')}}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="statistics-container">
            <div class="container">
                <div class="section-statisticas">
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/students.png')}}" alt="students">
                        </figure>
                        <p class="text--number">2350</p>
                        <p class="text--info">Alumnos</p>
                    </div>
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/graduation.png')}}" alt="students">
                        </figure>
                        <p class="text--number">17</p>
                        <p class="text--info">Instructores</p>
                    </div>
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/world.svg')}}" alt="students">
                        </figure>
                        <p class="text--number">5</p>
                        <p class="text--info">Paises</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="institute-itcenco-content color-secondary">
            <div class="container">
                <div class="institute-itcenco">
                    <div class="top">
                        <h3>INSTITUTO ITCENCO</h3>
                        <P> ¡ Calidad de enseñanza a tu alcance ! </P>
                    </div>
                    <div class="button">
                        <div class="box">
                            <figure>
                                <img src="{{ asset('assets/img/content.png')}}" alt="contenido">
                            </figure>
                            <a href="{{route('contenido')}}" class="link animate-scale">
                                <h4>CONTENIDO</h4>
                            </a>
                            <P class="text--info">
                                Accede a todos los contenidos de manera gratuita y con costos mínimos para potenciar tu aprendizaje.
                            </P>
                        </div>
                        <div class="box">
                            <figure>
                                <img src="{{ asset('assets/img/laptop.png')}}" alt="laptop">
                            </figure>
                            <a href="{{route('login')}}" class="link animate-scale">
                                <h4>AULA VIRTUAL</h4>
                            </a>
                            <P class="text--info">
                                Nuestra plataforma esta diseñado para que puedas aprender sin problemas.
                            </P>
                        </div>
                        <div class="box">
                            <figure>
                                <img src="{{ asset('assets/img/certificate.png')}}" alt="certificado">
                            </figure>
                            <a href="{{route('certification')}}" class="link animate-scale">
                                <h4>CERTIFICACIÓN</h4>
                            </a>
                            <P class="text--info">
                                Finaliza tu carrera profesional con nosotros y obten tu certificado.
                            </P>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="courses-content">
            <div class="container">
                <div class="section-courses">
                    <h2 class="text--title-section">Conoce Nuestros Diplomados y Especializaciones</h2>
                    <div class="courses">
                        <div class="list-items">
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/ARCGIS10.7/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        ARCGIS 10.7
                                    </p>
                                    <p class="text--info">
                                       Especialización
                                    </p>
                                </div>
                            </a>    
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/METRADO_EN_EDIFICACIONES/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Metrado en edificaciones
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/COSTO_Y_PRESPUESTO_CON_S10/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Costo y presupuesto con S10
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/PROGRAMACION_Y_CRONOGRAM_DE_OBRA_CON_MS-PROJECT/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Programación y cronograma de obra con ms-project
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/CALCULO_Y_DISEÑO_EN_EDIFICACIONES/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Calculo, Diseño en
                                            Instalaciones Eléctricas
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/INSTALACIONES ELECTRICAS/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Instalaciones Eléctricas
                                        en Edificaciones 
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/REVIT_BIM/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        Modelamiento BIM
                                        con Revit
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/VALORIZACION_Y_LIQUIDACION/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                       Valorización y Liquidación
                                        en Obras publicas 
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                            <a href="#" class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/especializacion/ETABS_Y_SAFE/1.png')}}" alt="">
                                </figure>
                                <div class="description">
                                    <p class="text--bold">
                                        ETABS Y SAFE
                                    </p>
                                    <p class="text--info">
                                        Especialización
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="action-content">
                            <a href="#" class="btn-expand btn btn-initial animate-scale">Ver más</a>
                        </div> 
                    </div>
                </div>
            </div>
        </section>

        <section class="professionals-content">
            <div class="content">
                <div class="section-profesionals">
                    <h2 class="text--title-section">Aprende de los mejores profesionales</h2>
                    <p class="text--info-section">
                        Todos nuestros cursos son creados con profesionales destacados de hablahispana que trabajan en lo que enseñan.
                    </p>
                    <div class="profesionals">
                        <div class="list-items owl-carousel" id="profesionals">
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/GOMEZ.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">GIULIANA NICHO GÓMEZ</p>
                                    <p class="text--info">Arquitecta</p>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/ALEXANDER.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">ALEXANDER LLANCO BALLASCO </p>
                                    <p class="text--info">Arquitecto</p>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/arcg.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">KEVIN ROMERO LA TORRE</p>
                                    <p class="text--info">Ing. Civil</p>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/RUIZ.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">JOAQUIN GABRIEL VARIAS RUIZ</p>
                                    <p class="text--info">Ing. Civil</p>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/BENJAMIN.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">JOB BENJAMIN SIMON CORI</p>
                                    <p class="text--info">Ing. Civil</p>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="{{ asset('assets/img/docentes/ABAL.jpg')}}" alt="">
                                </figure>
                                <div class="text-content">
                                    <p class="text--bold">GERLYN GREGORIO ABAL GARCIA</p>
                                    <p class="text--info">MG.</p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="action-content">
                            <a href="#" class=" btn-expand btn btn-initial animate-scale">Ver todos los docentes</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="testimonials-content color-tertiary">
            <div class="container">
                <div class="section-testimonials">
                    <h2 class="text--title-section">Historias y testimonios</h2>
                    <div class="list-items owl-carousel" id="testimonials">
                        <div class="item">
                            <div class="text--card">
                                <p>Gracias a itcenco pude mejorar mis conocimientos y capaicitarme 
                                    en gestion de proycetos .Y me ascendieron de puesto y desarrollarme
                                     profesionalmente.</p>
                            </div>
                            <div class="profile">
                                <figure>
                                    <img src="{{ asset('assets/img/estudiantes/estudiante1.jpeg')}}" alt="photo">
                                </figure>
                                <div class="box-info">
                                    <p class="text--name">
                                       Yaneth Vieyra
                                    </p>
                                    <p class="text--profession">
                                        Arquitecta
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="text--card">
                                <p>Gracias a itcenco pude mejorar mis conocimientos y capaicitarme 
                                    en gestion de proycetos .Y me ascendieron de puesto y desarrollarme
                                     profesionalmente.</p>
                            </div>
                            <div class="profile">
                                <figure>
                                    <img src="{{ asset('assets/img/estudiantes/estudiante2.jpg')}}" alt="photo">
                                </figure>
                                <div class="box-info">
                                    <p class="text--name">
                                        Katerin Perez
                                    </p>
                                    <p class="text--profession">
                                        Arquitecta
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="text--card">
                                <p>Gracias a itcenco pude mejorar mis conocimientos y capaicitarme 
                                    en gestion de proycetos .Y me ascendieron de puesto y desarrollarme
                                     profesionalmente.</p>
                            </div>
                            <div class="profile">
                                <figure>
                                    <img src="{{ asset('assets/img/estudiantes/estudiante3.jpeg')}}" alt="photo">
                                </figure>
                                <div class="box-info">
                                    <p class="text--name">
                                       Daniel Quijano 
                                    </p>
                                    <p class="text--profession">
                                        Arquitecto
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="text--card">
                                <p>Gracias a itcenco pude mejorar mis conocimientos y capaicitarme 
                                    en gestion de proycetos .Y me ascendieron de puesto y desarrollarme
                                     profesionalmente.</p>
                            </div>
                            <div class="profile">
                                <figure>
                                    <img src="{{ asset('assets/img/1.jpg')}}" alt="photo">
                                </figure>
                                <div class="box-info">
                                    <p class="text--name">
                                        Diana perales
                                    </p>
                                    <p class="text--profession">
                                        Ing. Civil
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="text--card">
                                <p>Gracias a itcenco pude mejorar mis conocimientos y capaicitarme 
                                    en gestion de proycetos .Y me ascendieron de puesto y desarrollarme
                                     profesionalmente.</p>
                            </div>
                            <div class="profile">
                                <figure>
                                    <img src="{{ asset('assets/img/profesional.png')}}" alt="photo">
                                </figure>
                                <div class="box-info">
                                    <p class="text--name">
                                        Marcos perez
                                    </p>
                                    <p class="text--profession">
                                        CEO Coopre Peru
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection
{{--
@push('js')
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/js/carousel.js') }}"></script>
@endpush--}}
