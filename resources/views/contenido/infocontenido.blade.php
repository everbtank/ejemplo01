@extends('base')
@section('title', 'Información de contenido - Instituto Itcenco')
@section('body')

<main id="main" >
        <section class="content-content-info">
            <div class="container">
                <div class="section-content-info">
                    <section class="product__header">
                        <div class="levels">
                            <div class="level__item">
                                <a href="{{ route('home') }}" class="text">Inicio</a>
                                <img src="{{asset('assets/img/arrow_menu.png')}}" alt="Arrow left">
                            </div>
                            <div class="level__item">
                                <a href="../contenido" class="text">Contenido</a>
                                <img src="{{asset('assets/img/arrow_menu.png')}}" alt="Arrow left">
                            </div>
                            <div class="level__item">
                                <a href="{{asset('assets/img/arrow_menu.png')}}" class="text">Lavado Holand</a>
                            </div>
                        </div>
                    </section>

                    <section class="slide-info-content">
                            <div class="slice-items-content owl-carousel" id="info-content-slide">
                                <div class="item">
                                    <figure>
                                        <img src="{{asset($infocontenidos['image'])}}" alt="">
                                    </figure>
                                </div>
                                <div class="item">
                                    <figure>
                                        <img src="{{asset($infocontenidos['image'])}}" alt="">
                                    </figure>
                                </div>
                                <div class="item">
                                    <figure>
                                        <img src="{{asset($infocontenidos['image'])}}" alt="">
                                    </figure>
                                </div>
                            </div>
                            <div class="resource-content-info">
                                <div class="group-text">
                                    <h2>{{$infocontenidos['title']}}</h2>
                                    <p>{{$infocontenidos['body']}}</p>
                                </div>
                                <div class="group-btn">
                                    <span>Gratis</span>
                                    <a href="{{route('content-download', ['id' => $infocontenidos['id'], 'uuid' => $infocontenidos['uuid']])}}" target="_blank" class="btn-download animate-scale">Descargar</a>
                                </div>
                                <div class="group-icons">
                                    <div class="icon">
                                        <figure>
                                            <img src="{{asset('assets/img/contenido/download.svg')}}" alt="icon download">
                                        </figure>
                                        <span>{{$infocontenidos['downloads']}}</span>
                                    </div>
                                    <div class="icon">
                                        <figure>
                                            <img src="{{asset('assets/img/contenido/view.svg')}}" alt="icon view">
                                        </figure>
                                        <span>{{$infocontenidos['downloads']+1}}</span>
                                    </div>
                                </div>
                            </div>
                    </section>

                    <section class="content-item-description">
                        <figure>
                            <img src="{{asset('assets/img/contenido/config.svg')}}" alt="icon config">
                        </figure>
                        <div class="description-text">
                            <h2>Descripción</h2>
                            <p>{{$infocontenidos['body']}}</p>
                        </div>
                    </section>
                </div>
            </div>
        </section>

        <section class="content-recomended-resource">
            <div class="container">
                <div class="slide-content-recomended owl-carousel" id="slide-recomended-info" >
                    @foreach($contenidos as  $contenido)
                        <a href="{{route('infocontenido', ['id' => $contenido['id']])}}" class="item">
                            <div class="top-item">
                                <figure>
                                    <img src="{{asset($contenido['image'])}}" alt="">
                                </figure>
                                <div class="text">
                                    <h3>{{$contenido['title']}}</h3>
                                    <p>{{$contenido['body']}}</p>
                                </div>
                            </div>
                            <div class="bottom-item">
                                <div class="type-resource">
                                    <span class="text--free">Gratis</span>
                                    <span class="text--pricing d-none"> 0</span>
                                </div>
                                <div class="icon-download">
                                    <img src="{{asset('assets/img/contenido/download.svg')}}" alt="">
                                    <span>{{$contenido['downloads']}}</span>
                                </div>
                                <div class="number-view">
                                    <img src="{{asset('assets/img/contenido/view.svg')}}" alt="">
                                    <span>{{$contenido['downloads']+1}}</span>
                                </div>
                            </div>
                        </a>
                    @endforeach
                  
                </div>
            </div>
        </section>
    </main>
@endsection