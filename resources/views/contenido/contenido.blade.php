@extends('base')
@section('title', 'Contenido - Instituto Itcenco')
@section('body')

<main id="main" >
    <div class="container">
        <section class="container-slider-module owl-carousel" id="contenido-slider">
           
            <div class="item-slide">
                <div class="container">
                    <div class="design-search">
                        <div class="top-title">
                            <h2 class="title">
                                Contenidos gratis y de paga</h2>
                            <p class="paragraph">
                                Contamos con contenido de paga y personalizados para que lo pueda descargar y utilizar.
                            </p>
                        </div>
                        <div class="center-search">
                           	<form>
                                <div class="search-resource">
                                    <input name="search"  type="text" placeholder="Buscar modelos">
                                    <button type="submit" class="send-seach">
                                        <figure>
                                            <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                        </figure>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
<!--             <div class="item-slide">
                <div class="container">
                    <div class="design-search">
                        <div class="top-title">
                            <h2 class="title">
                                Todos los contenidos de  ingenieria en un
                                solo lugar</h2>
                            <p class="paragraph">
                                Descarga nuevo contenido de forma gratuita, tambien contamos con contenido de paga.
                            </p>
                        </div>
                        <div class="center-search">
                           	<form>
                                <div class="search-resource">
                                   
                                    <input name="search"  type="text" placeholder="Buscar modelos">
                                       <button type="submit" class="send-seach">
                                            <figure>
                                                <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                            </figure>
                                        </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>-->
        </section>

    </div>

    <section class="container-list-categories">
        <div class="container">
            <div class="section-categories-resource">
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/herramenta.png" alt="">
                    </figure>
                    <span>Modelos 3d</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/pc.png" alt="">
                    </figure>
                    <span>Diseño de planos</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/herramenta.png" alt="">
                    </figure>
                    <span>Manuales</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/pc.png" alt="">
                    </figure>
                    <span>Imagenes</span>
                </a>
                <!--<a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/herramenta.png" alt="">
                    </figure>
                    <span>Aislado</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/pc.png" alt="">
                    </figure>
                    <span>Aislado</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/herramenta.png" alt="">
                    </figure>
                    <span>Aislado</span>
                </a>
                <a href="#" class="item">
                    <figure>
                        <img src="assets/img/contenido/pc.png" alt="">
                    </figure>
                    <span>Aislado</span>
                </a>-->
            </div>

            <div class="content-description-resource">
                <h3>BLOQUES AUTOCAD GRATIS PARA ARQUITECTURA, INGENIERÍA Y CONSTRUCCIÓN.</h3>
                <p>Somos la biblioteca de descarga e intercambio de bloques CAD y BIM más grande del mundo de habla hispana. Una base de datos concebida como un apoyo a tu tarea profesional. Acá podrás descargar e intercambiar bloques AutoCAD y objetos BIM 2D y 3D aplicables en Diseño e Industria de la Construcción.
                </p>
            </div>

        </div>
    </section>

    <section class="container-list-resource-featured">
        <div class="container">
            <div class="section-list-resources">
                <div class="top-title">
                    <h3 class="title-resource">TRABAJOS DESTACADOS</h3>
                    <div class="link-options">
                        <ul>
                            <li>
                                <a href="{{route('contenido', ['sort' => 'nuevos'])}}">Nuevos</a>
                            </li>
                            <li>
                                <a href="{{route('contenido', ['sort' => 'antiguos'])}}">Antiguos</a>
                            </li>
                            <li>
                                <a href="{{route('contenido', ['sort' => 'popular'])}}">Más populares</a>
                            </li>
                            <li>
                                <a href="{{route('contenido', ['sort' => 'descargas'])}}">Más descargados</a>
                            </li>
                        </ul>
                    </div>
                </div>
                @if(count($contents) > 0)
                <div class="container-list-items">
                    @foreach($contents as $key => $content)
                    <a href="{{route('infocontenido', ['id' => $content['id']])}}" class="item">
                        <div class="top-item">
                            <figure>
                                <img src="{{asset($content['image'])}}" alt="{{$content['title']}}">
                            </figure>
                            <div class="text">
                                <h3>{{$content['title']}}</h3>
                                <p>{{$content['body']}}</p>
                            </div>
                        </div>
                        <div class="bottom-item">
                            <div class="type-resource">
                                <span class="text--free">Gratis</span>
                                <span class="text--pricing d-none"> $0</span>
                            </div>
                            <div class="icon-download">
                                <img src="assets/img/contenido/download.svg" alt="">
                                <span>{{$content['downloads']}}</span>
                            </div>
                            <div class="number-view">
                                <img src="assets/img/contenido/view.svg" alt="">
                                <span>{{$content['downloads']+1}}</span>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
                @endif
                <div class="container-button-more">
                    <!--<a href="#" class=" btn btn-primary">
                        Explorar más
                    </a>-->
                </div>
            </div>
        </div>
    </section>

</main>
@endsection