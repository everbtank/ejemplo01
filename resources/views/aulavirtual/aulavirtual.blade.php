
@extends('base')
@section('title', 'Aula virtual - Instituto Itcenco')
@section('body')
<main id="main" >
    <article class="section-profile-module">
        <div class="container">
            <div class="content-dashboard">
                <div class="menu-sidebar-virtual">
                    <div class="header-sidebar">
                        <h3 class="title-sidebar">Servicios</h3>
                    </div>
                    <div class="menu-list-dashboard">
                        <ul>
                            <li>
                                <a href="https://www.bibliotecaspublicas.gob.cl/sitio/" class="link active">
                                    <figure>
                                        <img src="assets/img/virtual/biblioteca.png" alt="">
                                    </figure>
                                    <span>Biblioteca</span>
                                </a>
                            </li>
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=+51 9165782690&text=Hola buenas Instituto itcenco, Deseo realizar una asesoria " class="link">
                                    <figure>
                                        <img src="assets/img/virtual/phone.png" alt="">
                                    </figure>
                                    <span>Asesorias al +51 9165782690 </span>
                                </a>
                            </li>
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=+51 9165782690&text=Hola buenas soy alumno y deseo realizar una consulta y comunicacion" class="link">
                                    <figure>
                                        <img src="assets/img/virtual/logos_whatsapp.png" alt="">
                                    </figure>
                                    <span>Comunicación</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dashboard-body">
                    <div class="content-course-list">
                        <div class="body-virtual-classroom">
                            <div class="list-diplomas">
                                <div class="header-list">
                                    <h3 class="title"> {{count($classrooms)}}  Cursos o diplomados</h3>
                                </div>
                                <div class="list-items">
                                  <?php 
                                    $i = 1;
                                    ?>
                                    @foreach($classrooms as $classroom)
                                      
                                    <a href="{{route('classroom', ['id' => $classroom['classroom_id']])}}" class="item bg-primary">
                                        <div class="order-number">
                                            <p>{{$i++}}</p>
                                        </div> 
                                        <div class="info-diploma">
                                            <h3>{{$classroom['classroom']['title']}}</p>
                                        </div>
                                        <figure>
                                            <img src="{{asset($classroom['classroom']['photo_url'])}}" alt="course">
                                        </figure>
                                    </a>
                                    @endforeach
                                    
                                </div>
                                 
                            </div>
                            <div class="list-resources">
                                    <div class="header-list">
                                        <h3 class="title">Recursos</h3>
                                    </div>
                                    <div class="list-items">
                                        <div class="item bg-tertiary">
                                          
                                                <p class="name-resource">Clases en vivo</p>
                                                   
                                                <figure>
                                                    <img src="assets/img/virtual/akar-icons_zoom-fill1.png" alt="">
                                                </figure>
                                                <a href="#" class="link">
                                                    <p class="medium-resource">Zoom</p>
                                                </a>

                                             
                                                <figure>
                                                    <img src="assets/img/virtual/logos_google-meet.png" alt="">
                                                </figure>
                                                <a href="#" class="link">
                                                <p class="medium-resource">Meet</p>
                                                </a>


                                            
                                        </div>

                                    </div>
                                </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>

@endsection