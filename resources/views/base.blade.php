<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
     <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
   
    <title>@yield('title', 'Instituto Itcenco')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image" href="{{ asset('assets/img/icono.png')}}">
    <meta property="og:title" content="Estudia con nosotros y supera tus limites - Itcenco.com"/>
    <meta property="og:url" content="{{route('home')}}"/>
    <meta property="og:site_name" content="@yield('title', 'Itcenco')"/>
    <meta property="og:description" content="Capacitate y logra certificación internacional en los cursos con mayores demandas, ¡Instituto itcenco cuenta con una enseñanza de calidad que te sorprenderá!" />
   
    <link animate-scale rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owlcarousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" media="(min-width: 560px)" href="{{ asset('assets/css/min-width-560.css')}}" />
    <link rel="stylesheet" media="(min-width: 680px)" href="{{ asset('assets/css/min-width-680.css')}}" />
    <link rel="stylesheet" media="(min-width: 1100px)" href="{{ asset('assets/css/min-width-1100.css')}}" />
    <link rel="stylesheet" media="(min-width: 1366px)" href="{{ asset('assets/css/min-width-1366.css')}}" />
</head>
<body>

    @auth
        @include('layouts.header_auth')
    @endauth 
    @guest
        @include('layouts.header_guest')
    @endguest

    {{--@include('layouts.header_guest')--}}

    @section('content')
    @endsection
    @section('body')
    @show

    @include('layouts.footer')
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/js/carousel.js')}}"></script>
    
    @stack('js')
</body>
</html>