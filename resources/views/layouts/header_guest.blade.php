<input type="checkbox" class="d-none" id="sidebar-mobile">
<header >
    <div class="container">
        <div class="header header-desktop" id="header-height">
            <div class="logo-content">
                <a href="{{route('home')}}">
                    <img src="{{ asset('assets/img/logo2.png')}}" alt="">
                </a>
            </div>
            <div class="menu">
                <ul class="menu-list">
                    <li class="menu-item">
                        <a href="#" class="link animate-scale">Cursos</a>
                        <ul class="list-dropdown">
                            <li class="item-two">
                                <a href="{{route('curso', ['sort' => 'diplomados'])}}" class="link">Diplomados</a>
                            </li>
                            <li class="item-two">
                                <a href="{{route('curso', ['sort' => 'especializacion'])}}" class="link">Especializaciones</a>
                            </li>
                            <li class="item-two">
                                <a href="{{route('curso', ['sort' => 'cursos'])}}" class="link">Cursos</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item">
                        <a href="{{route('contenido')}}" class="link animate-scale">Contenido</a>
                       
                    </li>
                    <li class="menu-item">
                        <a href="{{route('evento')}}" class="link animate-scale">Eventos</a>
                        
                    </li>
                    <li class="menu-item">
                        <a href="{{route('blog')}}" class="link animate-scale">Blog</a>
                        
                    </li>
                </ul>
            </div>
            <div class="search-header">
                <div class="search-input">
                    <form action="{{route('curso')}}" method="GET">
                        <input type="text"  name="search" id="search-header" placeholder="Buscar curso">
                        <button type="submit" class="send-seach">
                            <figure>
                                <img src="{{ asset('assets/img/search_w')}}hite.png" alt="">
                            </figure>
                        </button>
                    </form>
                </div>
            </div>
            <div class="user-action">
                <div class="button-access">
                    <a href="registro"  class="btn btn-initial animate-scale btn-secondary">Regístrate</a>
                    <a href="login"  class="btn btn-primary animate-scale"> Iniciar sesión</a>
                </div>
                <div class="user-access">
                    <ul class="first-list">
                        <li class="first-line">
                            <button class="btn-item" type="button">
                                <figure class="icon">
                                    <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                                </figure>
                                <span class="count-float">45</span>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header header-mobile" id="header-height-mobile">
            <div class="menu-icon" id="icon-menu">
                <label for="sidebar-mobile" class="animate-scale">
                    <figure>
                        <img src="{{ asset('assets/img/menu.png')}}" alt="menu hamburguer">
                    </figure>
                </label>
            </div>
            <div class="menu-icon d-none" id="icon-close">
                <label for="sidebar-mobile" class="animate-scale">
                    <figure>
                        <img src="{{ asset('assets/img/close.png')}}" alt="menu hamburguer">
                    </figure>
                </label>
            </div>
            <div class="container-float d-none">
                <div class="menu">
                    <ul class="menu-list">
                        <li class="menu-item">
                            <a href="{{route('curso')}}" class="link animate-scale">Cursos</a>
                                <ul class="list-dropdown-mobile">
                                    <li class="item-two">
                                        <a href="#" class="link animate-scale">Recomendaciones</a>
                                    </li>
                                    <li class="item-two">
                                        <a href="#" class="link animate-scale">cursitos</a>
                                    </li>
                                </ul>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('contenido')}}" class="link animate-scale">Contenido</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('evento')}}" class="link animate-scale">Eventos</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('blog')}}" class="link animate-scale">Blog</a>
                        </li>

                    </ul>
                </div>
                <div class="search-header">
                    <div class="search-input">
                        <form action="{{route('curso')}}" method="GET">
                            <input type="text"  name="search" id="search-header" placeholder="Buscar curso">
                            <button type="submit" class="send-seach">
                                <figure>
                                    <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                </figure>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="user-action">
                    <div class="button-acces">
                        <a href="{{route('register')}}"  class="btn btn-initial animate-scale btn-secondary">Regístrate</a>
                        <a href="{{route('login')}}"  class="btn btn-primary animate-scale"> Iniciar sesión</a>
                    </div>
                    <div class="user-access">
                        <ul class="first-list">
                            <li class="first-line">
                                <button class="btn-item" type="button">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                                    </figure>
                                    <span class="count-float">45</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="logo-content">
                <a href="#">
                    <img src="{{ asset('assets/img/icono.png')}}" alt="">
                </a>
            </div>
        </div>
    </div>
</header>