<input type="checkbox" class="d-none" id="sidebar-mobile">
    <header >
        <div class="container">
            <div class="header header-desktop" id="header-height">
                <div class="logo-content">
                    <a href="{{route('home')}}">
                        <img src="{{ asset('assets/img/logo2.png')}}" alt="">
                    </a>
                </div>
                <div class="menu">
                    <ul class="menu-list">
                        <li class="menu-item">
                            <a href="#" class="link animate-scale">Cursos</a>
                            <ul class="list-dropdown">
                                <li class="item-two">
                                    <a href="{{route('curso', ['sort' => 'diplomados'])}}" class="link">Diplomados</a>
                                </li>
                                <li class="item-two">
                                    <a href="{{route('curso', ['sort' => 'especializacion'])}}" class="link">Especializaciones</a>
                                </li>
                                <li class="item-two">
                                    <a href="{{route('curso', ['sort' => 'cursos'])}}" class="link">Cursos</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('contenido')}}" class="link animate-scale">Contenido</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('evento')}}" class="link animate-scale">Eventos</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('blog')}}" class="link animate-scale">Blog</a>
                        </li>
                    </ul>
                </div>
                <div class="search-header">
                    <div class="search-input">
                        <form method="{{route('curso')}}" action="GET">
                        <input type="text"  name="search" id="search" placeholder="Buscar curso" >
                        <button type="submit" class="send-seach">
                            <figure>
                                <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                            </figure>
                        </button>
                    </form>
                    </div>
                </div>
                <div class="user-action">
                   <!--  <div class="button-acces">
                        <a href="register.html"  class="btn btn-initial animate-scale btn-secondary">Regístrate</a>
                        <a href="login.html"  class="btn btn-primary animate-scale"> Iniciar sesión</a>
                    </div> -->
                    <div class="user-access">
                         <ul class="first-list">
                    @if(Auth()->user()->hasRole('admin'))
                        <li class="first-line">
                              <a href="{{route('admin-home')}}" class="link-item">
                                <h2>Admin</h2></a>

                        </li>
                    @elseif(Auth()->user()->hasRole('user'))
                       <li class="first-line">
                           <button class="btn-item" type="button">
                               <figure class="icon">
                                   <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                               </figure>
                               <span class="count-float">10</span>
                           </button>
<!--                            <a href="{{route('carrito')}}" class="link-item">
                                <figure class="icon">
                                    <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                                </figure>
                                <span class="count-float">0</span>
                            </a>-->
                        </li>

                    @endif
                        <li class="first-line">
                            <a href="notificacion" class="link-item">
                                <figure class="icon">
                                    <img src="{{ asset('assets/img/contenido/bell.svg')}}" alt="">
                                </figure>
                                <span class="count-float">0</span>
                            </a>
                        </li>
                        <li class="first-line active-dropdown">
                            <input type="checkbox" id="profile-dropdown" class="d-none">
                            <label class="link-item" for="profile-dropdown">
                                <figure class="img-profile">
                                    <img src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}" alt="">
                                </figure>
                            </label>
                            <div class="box-list-dropdown">
                                <div class="triangle-with-shadow"></div>
                                <div class="user-info">
                                    <figure class="img-profile">
                                        <img src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}" alt="">
                                    </figure>
                                    <p>{{Auth::user()->name}}  </p>
                                    <span>{{Auth::user()->email}}</span>
                                </div>
                                <ul class="second-list">
                                    <li class="second-line">
                                        <a href="alumno_perfil" class="link-final">
                                            <img src="{{ asset('assets/img/contenido/mi_cuenta.svg')}}" alt="">
                                            <span>Mi cuenta</span>
                                        </a>
                                    </li>
                                    <li class="second-line">
                                        <a href="alumno_cursos" class="link-final">
                                            <img src="{{ asset('assets/img/contenido/mi_curso.svg')}}" alt="">
                                            <span>Mis cursos</span>
                                        </a>
                                    </li>
                                    <li class="second-line">
                                        <form method="POST"  action="{{route('logout')}}">
                                            @csrf
                                            <button class="btn animate-scale"  type="submit" >
                                                <img src="{{ asset('assets/img/contenido/cerrar.svg')}}" alt="">
                                                <span>Salir</span>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                        
                    </div>
                </div>
            </div>
            <div class="header header-mobile" id="header-height-mobile">
                <div class="menu-icon" id="icon-menu">
                    <label for="sidebar-mobile" class="animate-scale">
                        <figure>
                            <img src="{{ asset('assets/img/menu.png')}}" alt="menu hamburguer">
                        </figure>
                    </label>
                </div>
                <div class="menu-icon d-none" id="icon-close">
                    <label for="sidebar-mobile" class="animate-scale">
                        <figure>
                            <img src="{{ asset('assets/img/close.png')}}" alt="menu hamburguer">
                        </figure>
                    </label>
                </div>
                <div class="container-float d-none">
                    <div class="menu">
                        <ul class="menu-list">
                            <li class="menu-item">
                                <a href="cursos.html" class="link animate-scale">Cursos</a>
                            </li>
                            <li class="menu-item">
                                <a href="certificado.html" class="link animate-scale">Diplomados</a>
                            </li>
                            <li class="menu-item">
                                <a href="" class="link animate-scale">Eventos</a>
                            </li>
                            <li class="menu-item">
                                <a href="" class="link animate-scale">Blog</a>
                            </li>
                        </ul>
                    </div>
                    <div class="search-header">
                        <div class="search-input">
                            <form method="GET" action="{{route('curso')}}">
                                <input type="text"  name="search" id="search-header" placeholder="Buscar curso">
                                <button type="submit" class="send-seach">
                                    <figure>
                                        <img src="{{ asset('assets/img/search_white.png')}}" alt="">
                                    </figure>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="mobil-list-user">
                       <div class="user-info">
                            <figure class="img-profile">
                                <img src="{{asset(Auth::user()->photo_url ?? '/images/avatar.png')}}" alt="">
                            </figure>
                            <p>{{Auth::user()->name}}  </p>
                            <span>{{Auth::user()->email}}</span>
                        </div>
                        <ul class="second-list">
                            <li class="second-line">
                                <a href="alumno_perfil" class="link-final">
                                    <img src="{{ asset('assets/img/contenido/mi_cuenta.svg')}}" alt="">
                                    <span>Mi cuenta</span>
                                </a>
                            </li>
                            <li class="second-line">
                                <a href="alumno_cursos" class="link-final">
                                    <img src="{{ asset('assets/img/contenido/mi_curso.svg')}}" alt="">
                                    <span>Mis cursos</span>
                                </a>
                            </li>
                            <li class="second-line">
                                <form method="POST"  action="{{route('logout')}}">
                                    @csrf
                                    <button class="btn animate-scale"  type="submit" >
                                        <img src="{{ asset('assets/img/contenido/cerrar.svg')}}" alt="">
                                        <span>Salir</span>
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </div>
                    
                    
                </div>
               <!--  <div class="logo-content">
                    <a href="#">
                        <img src="{{ asset('assets/img/logo_sma')}}ll.svg" alt="">
                    </a>
                </div> -->

                <div class="user-action">
                    <!--  <div class="button-access">
                         <a href="register.html"  class="btn btn-initial animate-scale btn-secondary">Regístrate</a>
                         <a href="login.html"  class="btn btn-primary animate-scale"> Iniciar sesión</a>
                     </div> -->
                     <div class="user-access">
                     
                         <ul class="first-list">
                             <li class="first-line">
                                 <button class="btn-item" type="button">
                                     <figure class="icon">
                                         <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                                     </figure>
                                     <span class="count-float">45</span>
                                 </button>

<!--                                 <a href="carrito" class="link-item">
                                     <figure class="icon">
                                         <img src="{{ asset('assets/img/contenido/cart.svg')}}" alt="">
                                     </figure>
                                     <span class="count-float">0</span>
                                 </a>-->
                             </li>
                             <li class="first-line">
                                 <a href="notificacion" class="link-item">
                                     <figure class="icon">
                                         <img src="{{ asset('assets/img/contenido/bell.svg')}}" alt="">
                                     </figure>
                                     <span class="count-float">0</span>
                                 </a>
                             </li>
                             <!-- <li class="first-line active-dropdown">
                                 <input type="checkbox" id="profile-dropdown" class="d-none">
                                 <label class="link-item" for="profile-dropdown">
                                     <figure class="img-profile">
                                         <img src="{{ asset('assets/img/1.jpg" a')}}lt="">
                                     </figure>
                                 </label>
                                 <div class="box-list-dropdown">
                                     <div class="user-info">
                                         <figure class="img-profile">
                                             <img src="{{ asset('assets/img/1.jpg" a')}}lt="">
                                         </figure>
                                         <p>Juan camilo  </p>
                                         <span>juan@gmail.com</span>
                                     </div>
                                     <ul class="second-list">
                                         <li class="second-line">
                                             <a href="#" class="link-final">
                                                 <img src="{{ asset('assets/img/contenid')}}o/mi_cuenta.svg" alt="">
                                                 <span>Mi cuenta</span>
                                             </a>
                                         </li>
                                         <li class="second-line">
                                             <a href="#" class="link-final">
                                                 <img src="{{ asset('assets/img/contenid')}}o/mi_curso.svg" alt="">
                                                 <span>Mis cursos</span>
                                             </a>
                                         </li>
                                         <li class="second-line">
                                             <a href="#" class="link-final">
                                                 <img src="{{ asset('assets/img/contenid')}}o/cerrar.svg" alt="">
                                                 <span>Logout</span>
                                             </a>
                                         </li>
                                     </ul>
                                 </div>
                             </li> -->
                         </ul>
                     </div>
                 </div>
               <!--  <div class="user-access">
                    <ul class="first-list">
                        <li class="first-line">
                            <a href="#" class="link-item">
                                <figure class="icon">
                                    <img src="{{ asset('assets/img/contenid')}}o/cart.svg" alt="">
                                </figure>
                                <span class="count-float">2</span>
                            </a>
                        </li>
                        <li class="first-line">
                            <a href="#" class="link-item">
                                <figure class="icon">
                                    <img src="{{ asset('assets/img/contenid')}}o/bell.svg" alt="">
                                </figure>
                                <span class="count-float">6</span>
                            </a>
                        </li>
                        <li class="first-line active-dropdown">
                            <input type="checkbox" id="profile-dropdown" class="d-none">
                            <label class="link-item" for="profile-dropdown">
                                <figure class="img-profile">
                                    <img src="{{ asset('assets/img/1.jpg" a')}}lt="">
                                </figure>
                            </label>
                            <div class="box-list-dropdown">
                                <div class="user-info">
                                    <figure class="img-profile">
                                        <img src="{{ asset('assets/img/1.jpg" a')}}lt="">
                                    </figure>
                                    <p>Juan camilo  </p>
                                    <span>juan@gmail.com</span>
                                </div>
                                <ul class="second-list">
                                    <li class="second-line">
                                        <a href="#" class="link-final">
                                            <img src="{{ asset('assets/img/contenid')}}o/mi_cuenta.svg" alt="">
                                            <span>Mi cuenta</span>
                                        </a>
                                    </li>
                                    <li class="second-line">
                                        <a href="#" class="link-final">
                                            <img src="{{ asset('assets/img/contenid')}}o/mi_curso.svg" alt="">
                                            <span>Mis cursos</span>
                                        </a>
                                    </li>
                                    <li class="second-line">
                                        <a href="#" class="link-final">
                                            <img src="{{ asset('assets/img/contenid')}}o/cerrar.svg" alt="">
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div> -->
            </div>
        </div>
    </header>