 <footer>
        <div class="container">
            <div class="footer">
                <div class="social-media">
                    <h4>Siguenos</h4>
                    <div class="box-social-media">
                        <a href="https://www.youtube.com/channel/UC5kBsOigJAA2V2EjjHYD9hw" class="social--link animate-scale">
                            <img src="{{ asset('assets/img/youtube_grey.png')}}" alt="youtube">
                        </a>
                        <a href="https://www.instagram.com/instituto_itcenco/?hl=es-la" class="social--link animate-scale">
                            <img src="{{ asset('assets/img/instagram_grey.png')}}" alt="instagram">
                        </a>
                        <a href="https://www.facebook.com/itcenco/" class="social--link animate-scale">
                            <img src="{{ asset('assets/img/facebook_grey.png')}}" alt="facebook">
                        </a>
                    </div>
                    <h3>Suscribete para enviarte información</h3>
                    <div class="subscribe">
                        <div class="box-email">
                            <figure>
                                <img src="{{ asset('assets/img/email.png')}}" alt="icon email">
                            </figure>
                            <input type="email" placeholder="@email.com" name="email"> 
                        </div>
                        <button class="btn btn-primary ">Enviar</button>
                    </div>
                </div>
                <div class="footer-menu">
                    <h4>Menu</h4>
                    <ul class="list-menu">
                        <li class="item-menu"> 
                            <a href="{{route('curso')}}" class="link-menu">Cursos</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('contenido')}}" class="link-menu">Diplomados</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('evento')}}" class="link-menu">Eventos</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('blog')}}" class="link-menu">Blog</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-menu">
                    <h4>Itcenco</h4>
                    <ul class="list-menu">
                        
                        <li class="item-menu"> 
                            <a href="{{route('certification')}}" class="link-menu">Certificados</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('nosotros')}}" class="link-menu">Nosotros</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('aliados')}}" class="link-menu">Aliados</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('contenido')}}" class="link-menu">Contenido</a>
                        </li>
                        <li class="item-menu"> 
                            <a href="{{route('contacto')}}" class="link-menu">Contacto</a>
                        </li>
                        
                    </ul>
                </div>
                <div class="footer-menu">
                    <h4>Itcenco</h4>
                    <ul class="list-menu gap">
                        <li class="item-menu"> 
                            <img class="foo-icon" src="{{ asset('assets/img/phone.png')}}" alt="phone number">
                            <p class="text-footer">
                                +51 9165782690
                            </p>
                        </li>
                        <li class="item-menu"> 
                            <img class="foo-icon" src="{{ asset('assets/img/whatsapp.png')}}" alt="whatsapp">
                            <p class="text-footer">
                                +51 9165782690
                            </p>
                        </li>
                        <li class="item-menu"> 
                            <img class="foo-icon" src="{{ asset('assets/img/email_footer.png')}}" alt="email">
                            <p class="text-footer">
                                itcenco@gmail.com
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-rights">
                <p>© 2021 Instituto Itcenco - Todos los derechos reservados</p>
            </div>
        </div>
    </footer>

    <!-- Messenger plugin de chat Code -->
    <div id="fb-root"></div>

    <!-- Your plugin de chat code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "108730834107374");
      chatbox.setAttribute("attribution", "biz_inbox");

      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v11.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_ES/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>