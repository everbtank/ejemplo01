@extends('base')
@section('title', 'Contacto - Instituto Itcenco')
@section('body')
    <main id="main" >
        <section class="section-content-contact">
            <div class="container">
                <div class="title-contact">
                    <h2>Contácte con nosotros</h2>
                </div>
                <div class="content-data-contact">
                    <div class="left-google-map">
                        <div class="top-info-google-map">
                            <figure>
                                <img src="{{ asset('assets/img/map.png')}}" alt="">
                            </figure>
                            <div class="text-content">
                                <h3>Dirección</h3>
                                <p>Jr. Monseñor Salinas Nº 118 2do Piso 10001 
                                    Pillco marca, Peru</p>
                            </div>
                        </div>
                        <figure class="google-map-image">
                            <img src="{{ asset('assets/img/google_maps.png')}}" alt="localization">
                        </figure>
                    </div>
                    <div class="right-data-form">
                        <div class="top-data-info">
                            <div class="item">
                                <p>Email</p>
                                <div class="text">
                                   <figure class="icon">
                                       <img src="{{ asset('assets/img/email.png')}}" alt="">
                                   </figure>
                                    <span>contacto@itcenco.com</span>
                                </div>
                            </div>
                            <div class="item">
                                <p>Teléfono</p>
                                <div class="text">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/phone.png')}}" alt="">
                                    </figure>
                                    <span>+51 891293891</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-contact">
                            <form action="">
                                <div class="form-group">
                                    <label for="correo">Correo</label>
                                    <input class="input-control" type="email" name="correo" id="correo" placeholder="@example.com">
                                </div>
                                <div class="form-group">
                                    <label for="name">Nombre completo</label>
                                    <input class="input-control" type="text" name="name" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="message">Mensaje</label>
                                    <textarea class="input-control" name="message" id="message" cols="30" rows="4"></textarea>
                                </div>
                                <div class="group-btn">
                                    <button type="submit" class="btn btn-primary">
                                        <figure>
                                            <img src="{{ asset('assets/img/email_white.png')}}" alt="">
                                        </figure>
                                        <span>Enviar</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection