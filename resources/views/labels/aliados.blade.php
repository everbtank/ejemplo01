@extends('base')
@section('title', 'Aliados - Instituto Itcenco')
@section('body')
<main id="main" >
 <section class="container-aliados">
            <div class="container">
                <div class="section-convenios-module">
                    <div class="title-convenios">
                        <h2>Aliados</h2>
                        <p>Cualquier consulta sobre afiliados puedes contactarnos Aqui</p>
                    </div>
                    <div class="list-items owl-carousel" id="aliados-modoule">
                        <div class="item">
                            <figure class="img-aliado">
                                <img src="{{ asset('assets/img/about/COLEGIO.png')}}" alt="photo">
                            </figure>
                            <div class="item-body">
                                <h3>Colegio de Ingenieros del Perú</h3>
                                <p>institución deontológico, sin fines de lucro, que representa y agrupa a los ingenieros profesionales del Perú, de todas las especialidades, que cautela y preserva el comportamiento ético de sus miembros, y debe asegurar al Perú.</p>
                            </div>
                            <div class="item-footer">
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/facebook_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/youtube_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/instagram_grey.png')}}" alt="">
                                    </figure>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <figure class="img-aliado">
                                <img src="{{ asset('assets/img/about/ARQUITECTOS.png')}}" alt="photo">
                            </figure>
                            <div class="item-body">
                                <h3>Colegio de Arquitectos del Perú</h3>
                                <p>Institución autónoma, sin fines de lucro, con personería jurídica de Derecho Público Interno y patrimonio propio. Está integrado por los arquitectos que están oficialmente autorizados para ejercer la profesión y que se encuentren debidamente inscritos.</p>
                            </div>
                            <div class="item-footer">
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/facebook_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/youtube_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/instagram_grey.png')}}" alt="">
                                    </figure>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <figure class="img-aliado">
                                <img src="{{ asset('assets/img/about/CYPE.png')}}" alt="photo">
                            </figure>
                            <div class="item-body">
                                <h3>CYPE Latinoamérica</h3>
                                <p>CYPE Ingenieros, una empresa con más de 30 años de experiencia en Arquitectura, Ingeniería y Construcción que trabaja día a día para ofrecer un software eficaz y fiable, adaptado a las necesidades de los profesionales del sector.</p>
                            </div>
                            <div class="item-footer">
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/facebook_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/youtube_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/instagram_grey.png')}}" alt="">
                                    </figure>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <figure class="img-aliado">
                                <img src="{{ asset('assets/img/about/CYPE.png')}}" alt="photo">
                            </figure>
                            <div class="item-body">
                                <h3>Colegio de Ingenieros del Perú</h3>
                                <p>Institución deontológico, sin fines de lucro, que representa y agrupa a los ingenieros profesionales del Perú, de todas las especialidades.</p>
                            </div>
                            <div class="item-footer">
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/facebook_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/youtube_grey.png')}}" alt="">
                                    </figure>
                                </a>
                                <a href="#" class="animate-scale">
                                    <figure class="icon">
                                        <img src="{{ asset('assets/img/instagram_grey.png')}}" alt="">
                                    </figure>
                                </a>
                            </div>
                        </div>
                    </div>
                       
                </div>
            </div>
        </section>

    </main>

@endsection