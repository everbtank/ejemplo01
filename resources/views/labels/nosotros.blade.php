@extends('base')
@section('title', 'Nosotros - Instituto Itcenco')
@section('body')

<main id="main" >
        
    <section class="container-about">
        <div class="container">
            <div class="section-title-about">
                <h3>¿Quienes somos ?</h3>
                <p>Instituto Itcenco surge como una iniciativa del grupo Allarko
                     para satisfacer la necesidad educativa de estudiantes, profesionales
                      y público general del sector de ingeniería, diseño y 
                      construcción capacitándolos de manera presencial, semipresencial
                       y online a nivel nacional e internacional.</p>
            </div>

            <div class="section-achievements">
                <div class="top-image">
                    <h3>Nuestros Logros</h3>
                    <figure>
                        <img src="{{ asset('assets/img/about/bg_about.svg')}}" alt="image logros">
                    </figure>
                </div>
                <div class="bottom-items">
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/course/icon01.png')}}" alt="icono punche">
                        </figure>
                        <p>Brindamos diplomados y especializaciones
                            de calidad.</p>
                    </div>
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/course/icon02.png')}}" alt="icono graduados">
                        </figure>
                        <p>Contamos  con 
                            más de 1200 alumnos en 3 paises.</p>
                    </div>
                    <div class="item">
                        <figure>
                            <img src="{{ asset('assets/img/Diploma.png')}}" alt="icono certificado">
                        </figure>
                        <p>Nuestros certificados son válidos y reconocidos por el colegio de ingenieros</p>
                    </div>
                </div>
            </div>

            <div class="section-mision-vision">
                <div class="box-content">
                    <figure>
                        <img src="{{ asset('assets/img/about/mision.svg')}}" alt="image mision">
                    </figure>
                    <div class="only-text">
                        <h3>Misión</h3>
                        <p>
                            Capacitar estudiantes y profesionales y público general 
                            del sector ingeniería, diseño y construcción,
                             mediante la gestión de convenios con instituciones públicas y privadas,
                              contribuyendo al desarrollo educativo de la sociedad.
                        </p>
                    </div>
                </div>
                <div class="box-content vision">
                    <figure>
                        <img src="{{ asset('assets/img/about/vision.svg')}}" alt="image vision">
                    </figure>
                    <div class="only-text">

                        <h3>Visión</h3>
                        <p>
                            Ser una Institución educativa innovadora en gestión de diplomados,
                            cursos, seminarios y talleres, reconocida por estudiantes y
                            profesionales a nivel nacional e Internacional.
                        </p>
                    </div>
                </div>
            </div>
          
        </div>
    </section>
        <section class="container-aliados bg-tertiary">
            <div class="container">
                <div class="section-convenios bg-primary">
                    <div class="title-convenios">
                        <h2>Aliados</h2>
                    </div>
                    <div class="list-items owl-carousel" id="aliados">
                        <div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/COLEGIO.png')}}" alt="photo">
                            </figure>
                            <p>Colegio de Ingenieros del Perú</p>
                        </div>
                        <div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/ARQUITECTOS.png')}}" alt="photo">
                            </figure>
                            <p>Colegio de arquitectos del Perú</p>
                        </div>
                        <div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/CYPE.png')}}" alt="photo">
                            </figure>
                            <p>CYPE</p>
                        </div>
                        <div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/universidad.png')}}" alt="photo">
                            </figure>
                            <p>Universidad juan valle</p>
                        </div>
                       <!-- <div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/colegio_ingenieros.png')}}" alt="photo">
                            </figure>
                            <p>Colegio de Ingenieros del Perú</p>
                        </div>-->
                        <!--<div class="item">
                            <figure>
                                <img src="{{ asset('assets/img/about/universidad.png')}}" alt="photo">
                            </figure>
                            <p>Universidad juan valle</p>
                        </div>-->
                        
                    </div>
                </div>
            </div>
        </section>
</main>
@endsection