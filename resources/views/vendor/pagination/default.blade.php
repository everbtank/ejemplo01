@if ($paginator->hasPages())
    <nav class="content-btn-more">
        <ul class="pagination content-paginate">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled btn-left click" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <img src="assets/img/arrow_left.png" alt="">
                </li>
            @else
                <li class="btn-left click active">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                        <img src="{{asset('assets/img/arrow_left_active.png')}}" alt="">
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}

            <div class="items">
<!--                <div class="number">
                    <p>1</p>
                </div>
                <div class="number">
                    <p>2</p>
                </div>
                <div class="point">
                    <p>...</p>
                </div>-->

                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled number" aria-disabled="true"><p>{{ $element }}</p></li>
                    @endif

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li  class="active number" aria-current="page"><p>{{ $page }}</p></li>
                            @else
                                <li class="number"><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="btn-right click active">
                        <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                            <img src="{{asset('assets/img/arrow_right_active.png')}}" alt="">
                        </a>
<!--                        <a href="#">
                            <img src="assets/img/arrow_right_active.png" alt="">
                        </a>-->
                    </li>
                @else
                    <li class="btn-right click" aria-disabled="true" aria-label="@lang('pagination.next')">
<!--                        <span aria-hidden="true">&rsaquo;</span>-->
                        <img src="{{asset('assets/img/arrow_right.png')}}" alt="">

                    </li>
                @endif
            </div>

        </ul>
    </nav>
@endif
