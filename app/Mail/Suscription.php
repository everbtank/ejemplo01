<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\UploadedFile;

class Suscription extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $files;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $files)
    {
        $this->data = $data;
        $this->files = $files;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $message = $this->from("contact@itcencoperu.com", 'Contacto')
        ->subject('Nueva Inscripción')
        ->view('emails.suscription')->with('data', $this->data);
        if(count($this->files) > 0) {
            foreach($this->files as $file) {
                $message->attach($file->getRealPath(), array(
                    'as' => $file->getClientOriginalName(),     
                    'mime' => $file->getMimeType())
            );
            }
        }
        return $message;
    }
}
