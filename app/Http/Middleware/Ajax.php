<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Ajax
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->ajax() || !$request->wantsJson()) {
            return response(['sucess' => false, 'message' => 'Forbidden'], 403);
        }
        return $next($request);
    }
}
