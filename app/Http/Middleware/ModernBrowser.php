<?php

namespace App\Http\Middleware;

use Closure;
use Browser;
use Illuminate\Http\Request;

class ModernBrowser
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Browser::isIE() && !$request->is("old-browser")) {
            return redirect()->route('unsupported');
        }
        return $next($request);
    }
}
