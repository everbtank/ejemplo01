<?php

namespace App\Http\Controllers;
use App\Models\Contenido;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ContenidoController extends Controller
{
    public function create(){
        return view('contenido.contenido');
    }

    public function infoContenido($id){
        $infocontenido = Contenido::find($id);
        $contenido =  Contenido::orderBy('created_at','DESC')->paginate();
      
        if($infocontenido) {
            return view('contenido.infocontenido', ['infocontenidos' => $infocontenido,'contenidos' => $contenido]);
        }
        return redirect()->route('infocontenido');
    }

    
    public function searchContent(Request $request)
    {
        $table = Contenido::where('title','LIKE','%'.$request->search.'%')->orWhere('body','LIKE','%'.$request->search.'%');
        switch ($request->sort) {
            case 'nuevos':
                $table = $table->orderBy('created_at', 'desc')->paginate(8);
                break;
            
            case 'antiguos':
                $table = $table->orderBy('created_at', 'asc')->paginate(8);
                break;

            case 'popular':
                $table = $table->withCount('likes')->orderBy('likes_count', 'desc')->paginate(8);
                break;

            case 'descargas':
                $table = $table->orderBy('downloads', 'desc')->paginate(8);
                break;

            default:
                $table = $table->orderBy('created_at', 'desc')->paginate(8);
                break;
        }

        return view('contenido.contenido')->with('contents', $table);
    }

    

    public function createContent(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'file' => 'required',
        ], [
            'title.required' => 'El nombre del articulo no puede estar vacio.',
            'body.required' => 'La descripción del articulo no puede estar vacio',
            'file.required' => 'El archivo de descarga es obligatorio.'
        ]);
        $data = $request->only(['title', 'body']);
        $file_name = $request->file->getClientOriginalName();
        $file_type = $request->file->getClientMimeType();
        $file_path = $request->file->store('uploads/content/files', 'public');
        $data['file'] = $file_path;
        $file = new Files([
            'file_name' => $file_name,
            'file_type' => $file_type,
            'file_path' => $file_path
        ]);
        
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ], [
                'image' => 'Solo se permiten imagenes menos de 2MB.',
            ]);

            $data['image'] = $request->image->store('uploads/content/images', 'public');
        }
        $table = Contenido::create($data);
        $table->files()->save($file);
        return back()->with('sucess', 'Se publico el articulo!');
    }

    public function removeContent(Request $request, $id)
    {
        Contenido::destroy($id);
        return back();
    }

    public function downloadContent(Request $request, $id, $uuid)
    {
        $table = Contenido::where([['id', $id], ['uuid', $uuid]])->firstOrFail();
        $file = $table->files()->first();
        if (Storage::disk('public')->exists($file->file_path)) {
            $table->addDownload();
            $table->save();
            return Storage::disk('public')->download($file->file_path, $file->file_name);
        }
        return abort(404);
    }
}
