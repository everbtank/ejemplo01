<?php

namespace App\Http\Controllers;

use Browser;
use App\Models\User;
use App\Mail\Contact;
use App\Mail\Suscription;
use App\Models\Files;
use App\Models\Classrooms;
use App\Models\Certificate;
use App\Rules\ValidCaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function __invoke()
    {
      return view('home');  
    }

    public function certificado(){
      return view('certificado.certificado');
    }

    public function home(Request $request)
    {
    	return view('home');
    }

    public function contacto(Request $request)
    {
    	return view('labels.contacto');
    }

    public function aliados(Request $request)
    {
    	return view('labels.aliados');
    }

    public function nosotros(Request $request)
    {
    	return view('labels.nosotros');
    }

    public function createContact(Request $request)
    {
    	$this->validate($request, [
    		'name'     =>  'required',
    		'email'  =>  'required|email',
    		'message' =>  'required|string',
    	], [
    		'required' => 'El campo de :attribute no puede estar vacio.',
    	]);

    	Mail::to(env('MAIL_CONTACT'))->send(new Contact($request->only(['name', 'email', 'message'])));
    	return redirect()->route('contact')->with('success', 'Thank you for contacting us, we will be answering you very soon!');
    }

    public function about(Request $request)
    {
    	return view('labels.about');
    }

    public function privacy(Request $request)
    {
    	return view('labels.privacy');
    }

    public function panel(Request $request)
    {
        return view('panel.panel')->with('classrooms', Classrooms::all())->with('users', User::all())->with('certificates', Certificate::all());
    }

    public function classroom(Request $request)
    {
        return view('panel.classroom');
    }

    public function certificate (Request $request)
    {
        return view('panel.certificate ');
    }

    public function diplomados()
    {
        return view('content.diplomados');
    }

    public function diplomado()
    {
        return view('content.diplomado-bim');
    }

    public function diplomatLiveSuscription()
    {
        return view('content.diplomado-suscription');
    }

    public function diplomatSuscription(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required|min:6|max:120',
            'email'  =>  'required|email',
            'document' =>  'required|numeric|min:6',
            'phone' =>  'required|numeric',
            'country' =>  'required|string|min:3',
            'special' =>  'required|string|min:3',
            'files' => 'mimes:zip,pdf,jpg,jpeg,bmp,png|max:5000',
            'files.*' => 'mimes:zip,pdf,jpg,jpeg,bmp,png|max:5000',
        ], [
            'required' => 'Este campo no puede estar vacío',
            'mimes' => 'Solo se aceptar archivos: :values.',
        ]);
        $files = $request->hasFile('files') ? $request->files : null;
        Mail::to(env('MAIL_CONTACT'))->send(new Suscription($request->only(['name', 'email', 'document', 'phone', 'country', 'special']), $files));
        return redirect()->route('diplomado-suscription')->with('success', 'Thank you for subscribing, we will reply to your email very soon!');
    }

    public function affiliate()
    {
        return view('content.affiliate');
    }

    public function unsupported(Request $request)
    {
        if (!Browser::isIE()) {
            return redirect()->route('home');
        }
        return view('labels.unsupported');
    }

    public function download(Request $request, $id, $uuid)
    {
        $table = Files::where([['id', $id], ['uuid', $uuid]])->firstOrFail();
        $file_name = $table->file_name;
        $headers = array(
          'Content-Type' => $table->file_type,
          'Content-Length' => Storage::size($table->file_path),
          'Content-Disposition' => 'attachment; filename='.$file_name
      );

        return Storage::download($table->file_path, $table->file_name, $headers);
        return response()->download($table->file_path, $table->file_name, [
          'Content-Type: ' . $table->file_type,
          'Content-Length: '. Storage::size($table->file_path),
      ])->header('Content-Type', $table->file_type);
    }

    

}
