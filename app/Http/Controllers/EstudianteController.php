<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Classrooms;
use App\Models\ClassroomsMembers;
use App\Models\Comments;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EstudianteController extends Controller
{
    public function curso(){
        return view('estudiante.estudiante_curso');
    }
    public function perfil(){
        return view('estudiante.perfil');
    }
    public function pago(){
        return view('estudiante.pago');
    }

    public function cursos_estudiante($id)
    {
        
        $classroom = Classrooms::find($id);
        if ($classroom) {
            if (Auth::user()->hasRole('admin')) {
                return view('estudiante.estudiante_curso', ['classroom' => $classroom]);
            }elseif (!empty($classroom->users()->where('user_email', '=', Auth::user()->email)->first())) {
                return view('estudiante.estudiante_curso', ['classroom' => $classroom]);
            }
        }
         return redirect()->route('classrooms');
    }

    public function cursos_estudiantes(Request $request)
    {
        $classrooms = ClassroomsMembers::where('user_email', '=', Auth::user()->email)->with('classroom')->get();
        return view('estudiante.estudiante_curso', ['classrooms' => $classrooms]);
    }

}
