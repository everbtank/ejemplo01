<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    public function __construct(){

        $this->middleware('auth');

    }
    
    public function login(){
        return view('auth.login');
    }

    public function register(){
          return view('auth.register');
    }
  
    public function recover(){
          return view('auth.recover');
  
    }

    public function store(){
        $user = User::create(request(['name','email','password']));
        auth()->login($user);
        return redirect()->to('/');
    }
}
