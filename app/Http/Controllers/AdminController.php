<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use App\Models\Tipo;
use App\Models\Certificate;
use App\Models\Classrooms;
use App\Models\Contenido;
use App\Models\Productos;
use App\Models\Categorias;
use App\Models\Niveles;
use App\Models\Modalidades;
use App\Models\Productos_cursos;
use App\Models\Files;
use App\Models\Comments;
use App\Models\ClassroomsMembers;
use App\Models\ClassroomsPosts;
use App\Models\ClassroomsLessons;
use App\Rules\ValidCaptcha;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class AdminController extends Controller
{   
    public function panel(){
        return view('admin.admin-home');
        $disponibilidad = Productos::where('estado_pedidos',3)->count();
    }

    public function createAulavirtual(Request $request){
        $texto=trim($request->get('search'));
        $classrooms=DB::table('classrooms')
        ->select('id','title','description','photo_url','class_end','class_start')
        ->where('title','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(10);
        
        return view('admin.admin-aulavirtual',compact('classrooms'));
     
        //return view('admin.admin-aulavirtual',['classrooms' => $classrooms]);

    }
    public function storeAulavirtual(Request $request){
        $classrooms = new Classrooms();
        $classrooms->categoria_id = $request->input('categoria');
        $classrooms->nivel_id = $request->input('nivel');
        $classrooms->modalidad_id = $request->input('modalidad');
        $classrooms->tipo_id = $request->input('tipo');
        $classrooms->title = $request->input('title');
        $classrooms->description = $request->input('description');
        
        $photo_url1 = $this->getPhoto($request);
        if ($photo_url1) {
            $classrooms->photo_url= $photo_url1;
        }
        //$classrooms->photo_url = $request->input('potho_url');
        $classrooms->inicio = $request->input('inicio');
        $classrooms->precio = $request->input('precio');
        $classrooms->moneda = $request->input('moneda');
        $classrooms->save();
        return redirect()->route('admin-productos');

    }
    
    public function putAulavirtual(Request $request, $id){
       
        $producto = Classrooms::findOrFail($id);
        return $producto;

        $producto->categoria_id = $request->input('categoria');
        $producto->nivel_id = $request->input('nivel');
        $producto->modalidad_id = $request->input('modalidad');
        $producto->tipo_id = $request->input('tipo');
        $producto->title = $request->input('title');
        $producto->description = $request->input('description');
        $producto->photo_url = $request->input('potho_url');
        $producto->inicio = $request->input('inicio');
        $producto->precio = $request->input('precio');
        $producto->moneda = $request->input('moneda');
        $producto->save();
        return redirect()->route('admin-productos');
    
    }

    public function editAulavirtual($id){
        $classrooms = Classrooms::findOrFail($id);
        return view('admin.admin-editaulavirtual',['classrooms' => $classrooms ]);

    }

    public function destroyAulavirtual($id){
        $classrooms  = Classrooms::findOrFail($id);
        $classrooms ->delete();
        return redirect()->route('admin-productos');   

    }

    public function cursos(Request $request){
        $texto=trim($request->get('search'));
        $cursos=DB::table('productos_cursos')
        ->select('id','title','description','imagen','class_start','class_end','downloads','links','meet','zoom')
        ->where('title','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(10);
        $productos =  Productos::orderBy('created_at','DESC')->paginate();
        //return $productos;
        return view('admin.admin-cursos',['cursos'=>$cursos,'productos'=>$productos]);
    }

    public function storeCursos(Request $request){
        $cursos = new Productos_cursos();
        $cursos->productos_id = $request->input('producto');
        $cursos->title = $request->input('title');
        $cursos->description = $request->input('description');
   
        if($request->hasFile("imagen")){
            $imageName = time().'.'.$request->imagen->extension();  
            $request->imagen->move(public_path('uploads/classroom/images'), $imageName);
            $cursos->imagen = '/uploads/classroom/images/'.$imageName;
        }
    
        $cursos->class_start = $request->input('class_start');
        $cursos->class_end= $request->input('class_end');
        $cursos->links = $request->input('link');
        $cursos->zoom = $request->input('zoom');
        $cursos->meet = $request->input('meet');
        $cursos->save();
        return redirect()->route('admin-cursos');

    }

    public function putcursos(Request $request, $id){
       
        $producto = Productos_cursos::findOrFail($id);
        $producto->categoria_id = $request->input('categoria');
        $producto->nivel_id = $request->input('nivel');
        $producto->modalidad_id = $request->input('modalidad');
        $producto->tipo_id = $request->input('tipo');
        $producto->title = $request->input('title');
        $producto->description = $request->input('description');
        $producto->photo_url = $request->input('potho_url');
        $producto->inicio = $request->input('inicio');
        $producto->precio = $request->input('precio');
        $producto->moneda = $request->input('moneda');
        $producto->save();
        return redirect()->route('admin-cursos');
    
    }

    public function editcursos($id){
        $producto = Productos::findOrFail($id);
        $categorias =  Categorias::orderBy('created_at','DESC')->paginate();
        $modalidades =  Modalidades::orderBy('created_at','DESC')->paginate();
        $niveles =  Niveles::orderBy('created_at','DESC')->paginate();
        $tipos =  Tipo::orderBy('created_at','DESC')->paginate();
        return view('admin.admin-editproductos',['producto' => $producto,'categorias' => $categorias,'niveles' => $niveles,'modalidades' => $modalidades,'tipos' => $tipos]);

    }

    public function destroycursos($id){
        $producto = Productos_cursos::findOrFail($id);
        $producto->delete();
        return redirect()->route('admin-cursos');   

    }

  

    public function clases(Request $request){
        $texto=trim($request->get('search'));
        $clases=DB::table('clases')
        ->select('id','titulo','description')
        ->where('titulo','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(10);
        return view('admin.admin-clases',compact('clases'));
    }

    public function docentes(Request $request){
        $texto=trim($request->get('search'));
        $rol=3;
        $docentes=DB::table('users')
        ->select('users.id','users.photo_url','users.name','users.email','users.identify','users.created_at')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->where('users.name','LIKE','%'.$texto.'%')
        ->where('role_user.role_id','LIKE','%'.$rol.'%')
        ->orderBy('id','asc')
        ->paginate(10);
        return view('admin.admin-docentes',compact('docentes'));
    }


    public function productos(Request $request){
        $texto=trim($request->get('search'));
        $productos=DB::table('productos')
        ->select('id','title','description','photo_url','inicio','precio','moneda','duracion')
        ->where('title','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(10);
        $id=$productos['id'];
        $categorias =  Categorias::orderBy('created_at','DESC')->paginate();
        $modalidades =  Modalidades::orderBy('created_at','DESC')->paginate();
        $niveles =  Niveles::orderBy('created_at','DESC')->paginate();
        $tipos =  Tipo::orderBy('created_at','DESC')->paginate();
        return view('admin.admin-productos',['productos' => $productos,'categorias' => $categorias,'niveles' => $niveles,'modalidades' => $modalidades,$niveles,'tipos' => $tipos]);
    }

    public function storeProductos(Request $request){
        $producto = new Productos();
        $producto->categoria_id = $request->input('categoria');
        $producto->nivel_id = $request->input('nivel');
        $producto->modalidad_id = $request->input('modalidad');
        $producto->tipo_id = $request->input('tipo');
        $producto->title = $request->input('title');
        $producto->description = $request->input('description');
   
        if($request->hasFile("imagen")){
            $imageName = time().'.'.$request->imagen->extension();  
            $request->imagen->move(public_path('uploads/classroom/images'), $imageName);
            $producto->photo_url = '/uploads/classroom/images/'.$imageName;
        }
    
        $producto->inicio = $request->input('inicio');
        $producto->duracion= $request->input('time');
        $producto->diplomas= $request->input('school');
        $producto->precio = $request->input('precio');

        if($request->input('precio')>0){
            $producto->tipo_precio = "precio";
        }else{
            $producto->tipo_precio = "gratis";
        }

        $producto->moneda = $request->input('moneda');
        $producto->save();
        return redirect()->route('admin-productos');

    }

   
    
    public function putProductos(Request $request, $id){
       
        $producto = Productos::findOrFail($id);
        $producto->categoria_id = $request->input('categoria');
        $producto->nivel_id = $request->input('nivel');
        $producto->modalidad_id = $request->input('modalidad');
        $producto->tipo_id = $request->input('tipo');
        $producto->title = $request->input('title');
        $producto->description = $request->input('description');
        $producto->photo_url = $request->input('potho_url');
        $producto->inicio = $request->input('inicio');
        $producto->precio = $request->input('precio');
        $producto->moneda = $request->input('moneda');
        $producto->save();
        return redirect()->route('admin-productos');
    
    }

    public function editProductos($id){
        $producto = Productos::findOrFail($id);
        $categorias =  Categorias::orderBy('created_at','DESC')->paginate();
        $modalidades =  Modalidades::orderBy('created_at','DESC')->paginate();
        $niveles =  Niveles::orderBy('created_at','DESC')->paginate();
        $tipos =  Tipo::orderBy('created_at','DESC')->paginate();
        return view('admin.admin-editproductos',['producto' => $producto,'categorias' => $categorias,'niveles' => $niveles,'modalidades' => $modalidades,'tipos' => $tipos]);

    }

    public function destroyProductos($id){
        $producto = Productos::findOrFail($id);
        $producto->delete();
        return redirect()->route('admin-productos');   

    }

    public function contenido(Request $request){
        $texto=trim($request->get('search'));
        $contenidos=DB::table('contenidos')
        ->select('id','title','body','image','created_at','downloads')
        ->where('title','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(5);
        return view('admin.admin-contenido',compact('contenidos'));

    }



    public function storeContenido(Request $request){
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'file' => 'required',
        ], [
            'title.required' => 'El nombre del articulo no puede estar vacio.',
            'body.required' => 'La descripción del articulo no puede estar vacio',
            'file.required' => 'El archivo de descarga es obligatorio.'
        ]);
        $data = $request->only(['title', 'body']);
        $file_name = $request->file->getClientOriginalName();
        $file_type = $request->file->getClientMimeType();
        $file_path = $request->file->store('uploads/content/files', 'public');
        $data['file'] = $file_path;
        $file = new Files([
            'file_name' => $file_name,
            'file_type' => $file_type,
            'file_path' => $file_path
        ]);
        
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ], [
                'image' => 'Solo se permiten imagenes menos de 2MB.',
            ]);

            $data['image'] = $request->image->store('uploads/content/images', 'public');
        }
        $table = Contenido::create($data);
        $table->files()->save($file);
        return redirect()->route('admin-contenidos');

    }

    public function editContenido($id){
        $contenidos = Contenido::findOrFail($id);
        return view('admin.admin-editcontenido',['contenidos' => $contenidos]);

    }

    public function destroyContenido($id){
        $contenido = Contenido::findOrFail($id);
        $contenido->delete();
        return redirect()->route('admin-contenidos');   

    }


   

    public function certificado(Request $request){
        $texto=trim($request->get('search'));
        $certificados=DB::table('certificates')
        ->select('id','student_name','student_dni','course_code','course_name','course_end','course_score','course_time','url_download','school')
        ->where('student_name','LIKE','%'.$texto.'%')
        ->orderBy('id','asc')
        ->paginate(6);
     
        return view('admin.admin-certificado',['certificados' => $certificados]);
    }

    public function storeCertificados(Request $request){

        $certificado = new Certificate();
        $certificado->student_name = $request->input('student_name');
        $certificado->student_dni = $request->input('student_dni');
        $certificado->course_name = $request->input('course_name');
        $certificado->course_code= $request->input('course_code');
        $certificado->course_end = $request->input('course_end');
        $certificado->course_score = $request->input('course_score');
        $certificado->course_time = $request->input('course_time');
        $certificado->url_download = $request->input('url_download');
        $certificado->school = $request->input('school');
        $certificado->save();
         //return  $certificado;
        return redirect()->route('admin-certificado');
        
    }

    public function putCertificado(Request $request, $id){
       
        $producto = Certificate::findOrFail($id);
        return $producto;
        /*$certificado->student_name = $request->input('student_name');
        $certificado->student_dni = $request->input('student_dni');
        $certificado->course_name = $request->input('course_name');
        $certificado->course_code= $request->input('course_code');
        $certificado->course_end = $request->input('course_end');
        $certificado->course_score = $request->input('course_score');
        $certificado->course_time = $request->input('course_time');
        $certificado->url_download = $request->input('url_download');
        $certificado->school = $request->input('school');
        $certificado->save();
        $certificado->save();
        return redirect()->route('admin-certificado');*/
    
    }

    public function editCertificado($id){
        $certificado = Certificate::findOrFail($id);
        return view('admin.admin-editcertificado',['certificados' => $certificado]);

    }

    public function destroyCertificado($id){
        $producto = Certificate::findOrFail($id);
        $producto->delete();
        return redirect()->route('admin-certificado');   

    }

   

    public function alumnos(Request $request){
     
       $texto=trim($request->get('search'));
       $rol=2;
       $alumnos=DB::table('users')
       ->select('users.id','users.photo_url','users.name','users.email','users.identify','users.created_at')
       ->join('role_user', 'role_user.user_id', '=', 'users.id')
       ->where('users.name','LIKE','%'.$texto.'%')
       ->where('role_user.role_id','LIKE','%'.$rol.'%')
       ->orderBy('id','asc')
       ->paginate(10);
       return view('admin.admin-alumnos',['alumnos' => $alumnos]);
    }


   
    public function createDocente(){
        return view('admin.admin-docenteform');
    }
    public function adminform(){
        return view('admin.admin-form');
    }
    public function storeDocente(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->identify = $request->identify;
        $user->password = $request->password;

        $user->save();
        return redirect()->route('admin-docentes');
    }


    public function getClassroom(Request $request)
    {
        return Classrooms::with('users', 'lessons')->orderBy('created_at', 'desc')->paginate(10);
    }

    public function createClassroom(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'meet_url' => 'string|nullable',
            'zoom_url' => 'string|nullable',
            'mstm_url' => 'string|nullable',
            'class_start' => 'string|nullable',
            'class_time' => 'string|nullable',
            'users' => 'string|nullable',
            'lessons' => 'string|nullable',
        ], [
            'required' => 'No se permiten campos vacios',
        ]);

        $datas = $request->all();
        $photo_url = $this->getPhoto($request);
        if ($photo_url) {
            $datas['photo_url'] = $photo_url;
        }
        $table = Classrooms::create($datas);
        foreach (json_decode($request->users) as $user) {
            ClassroomsMembers::firstOrCreate([
                'classroom_id' => $table->id,
                'user_email' => $user,
            ]);
        }
        foreach (json_decode($request->lessons, true) as $lesson) {
            ClassroomsLessons::firstOrCreate([
                'classroom_id' => $table->id,
                'lesson_name' => $lesson['lesson_name'],
                'lesson_url' => $lesson['lesson_url']
            ]);
        }
        return $table;
    }

    

    public function removeClassroom(Request $request)
    {
        $table = Classrooms::find($request->id);
        if ($table) {
            foreach ($table->posts as $post) {
                foreach ($post->files as $file) {
                    File::delete($file->file_url);
                }
            }
        }
        $table = Classrooms::destroy($request->id);
        return $table;
    }

    public function updateClassroom(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'meet_url' => 'string|nullable',
            'zoom_url' => 'string|nullable',
            'mstm_url' => 'string|nullable',
            'class_start' => 'string|nullable',
            'class_time' => 'string|nullable',
            'users' => 'string|nullable',
            'lessons' => 'string|nullable',
        ], [
            'required' => 'No se permiten campos vacios',
        ]);
        $table = Classrooms::findOrFail($id);
        
        $datas = $request->all();
        $photo_url = $this->getPhoto($request);
        if ($photo_url) {
            $datas['photo_url'] = $photo_url;
        }
        
        $table->update($datas);
        $table->users()->delete();
        $table->lessons()->delete();
        foreach (json_decode($request->users) as $user) {
            ClassroomsMembers::firstOrCreate([
                'classroom_id' => $table->id,
                'user_email' => $user,
            ]);
        }
        foreach (json_decode($request->lessons, true) as $lesson) {
            ClassroomsLessons::firstOrCreate([
                'classroom_id' => $table->id,
                'lesson_name' => $lesson['lesson_name'],
                'lesson_url' => $lesson['lesson_url']
            ]);
        }
        return $table;
    }

    private function getPhoto(Request $request)
    {
        if ($request->hasFile('photo_file')) {
            $request->validate([
                'photo_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ], [
                'photo_file' => 'Solo se permiten imagenes menos de 2MB.',
            ]);
            $imageName = time().'.'.$request->photo_file->extension();  
            $request->photo_file->move(public_path('uploads/classroom/images'), $imageName);
            return '/uploads/classroom/images/'.$imageName;
        }
        return null;
    }

    public function createPost(Request $request)
    {
        $request->validate([
            'id' => 'required,integer',
            'title' => 'required',
            'body' => 'required',
        ], [
            'title.required' => 'El titulo no puede estar vacio.',
            'body.required' => 'El mensaje no puede estar vacio.'
        ]);

        if (!isset($request->id) || !Classrooms::find($request->id)) {
            return redirect()->back();
        }
        $table = ClassroomsPosts::create(['classroom_id' => $request->id, 'title' => $request->title, 'body' => $request->body]);
        if ($request->hasFile('files')) {
            foreach ($request->file('files') as $file) {
                if ($file->isValid()) {
                    $file_name = $file->getClientOriginalName();
                    $file_type = $file->getClientMimeType();
                    $file_path = $file->store('classroom');
                    $file = new Files([
                        'file_name' => $file_name,
                        'file_type' => $file_type,
                        'file_path' => $file_path
                    ]);
                    $table->files()->save($file);
                    
                }
            } 
        }
        return redirect()->back();
        
    }

    public function removePost(Request $request)
    {
        $table = ClassroomsPosts::find($request->id);
        $table = ClassroomsPosts::destroy($request->id);
        return redirect()->back()->with('status', $table);
    }


    public function searchContent(Request $request)
    {
        $table = Contenido::where('title','LIKE','%'.$request->search.'%')->orWhere('body','LIKE','%'.$request->search.'%');
        switch ($request->sort) {
            case 'nuevos':
                $table = $table->orderBy('created_at', 'desc')->paginate(10);
                break;
            
            case 'antiguos':
                $table = $table->orderBy('created_at', 'asc')->paginate(10);
                break;

            case 'popular':
                $table = $table->withCount('likes')->orderBy('likes_count', 'desc')->paginate(10);
                break;

            case 'descargas':
                $table = $table->orderBy('downloads', 'desc')->paginate(10);
                break;

            default:
                $table = $table->orderBy('created_at', 'desc')->paginate(10);
                break;
        }

        return view('content.contenido')->with('contents', $table);
    }

    public function createContent(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'file' => 'required',
        ], [
            'title.required' => 'El nombre del articulo no puede estar vacio.',
            'body.required' => 'La descripción del articulo no puede estar vacio',
            'file.required' => 'El archivo de descarga es obligatorio.'
        ]);
        $data = $request->only(['title', 'body']);
        $file_name = $request->file->getClientOriginalName();
        $file_type = $request->file->getClientMimeType();
        $file_path = $request->file->store('uploads/content/files', 'public');
        $data['file'] = $file_path;
        $file = new Files([
            'file_name' => $file_name,
            'file_type' => $file_type,
            'file_path' => $file_path
        ]);
        
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ], [
                'image' => 'Solo se permiten imagenes menos de 2MB.',
            ]);

            $data['image'] = $request->image->store('uploads/content/images', 'public');
        }
        $table = Contenido::create($data);
        $table->files()->save($file);
        return back()->with('sucess', 'Se publico el articulo!');
    }

    public function removeContent(Request $request, $id)
    {
        Contenido::destroy($id);
        return back();
    }

    public function downloadContent(Request $request, $id, $uuid)
    {
        $table = Contenido::where([['id', $id], ['uuid', $uuid]])->firstOrFail();
        $file = $table->files()->first();
        if (Storage::disk('public')->exists($file->file_path)) {
            $table->addDownload();
            $table->save();
            return Storage::disk('public')->download($file->file_path, $file->file_name);
        }
        return abort(404);
    }
}
