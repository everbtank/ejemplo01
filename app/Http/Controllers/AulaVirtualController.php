<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Classrooms;
use App\Models\ClassroomsMembers;
use App\Models\Comments;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AulaVirtualController extends Controller
{
    public function create(){
        return view('aulavirtual.aulavirtual');
    }
    public function classroom($id)
    {
        
        $classroom = Classrooms::find($id);
        if ($classroom) {
            if (Auth::user()->hasRole('admin')) {
                return view('profile.classroom', ['classroom' => $classroom]);
            }elseif (!empty($classroom->users()->where('user_email', '=', Auth::user()->email)->first())) {
                return view('profile.classroom', ['classroom' => $classroom]);
            }
        }
         return redirect()->route('classrooms');
    }

    public function classrooms(Request $request)
    {
        $classrooms = ClassroomsMembers::where('user_email', '=', Auth::user()->email)->with('classroom')->get();
        return view('aulavirtual.aulavirtual', ['classrooms' => $classrooms]);
    }

    public function createMessage(Request $request)
    {
        $request->validate([
            'id' => 'required,integer',
            'message' => 'required|string|max:1024'
        ], [
            'message.required' => 'El mensaje no puede estar vacio.',
            'message.max' => 'El mensaje es demasiado largo.'
        ]);
        if (($table = Classrooms::find($request->id))) {
            $table->chats()->save(new Comments(['user_id' => Auth::user()->id, 'body' => $request->message]));
            return redirect()->to(url()->previous());
        }
        return redirect()->to(url()->previous());
    }

    public function removeMessage(Request $request)
    {
        $table = Comments::find($request->id);
        if ($table && ($table->user_id == Auth::user()->id || Auth::user()->hasRole('admin'))) {
            $table->delete();
        }
        return redirect()->back();
    }

    public function favoriteClassroom(Request $request, $id)
    {
        $classroom = Classrooms::find($id);
        if ($classroom) {
            $existing_like = Favorite::withTrashed()->whereFavoritableType(Classrooms::class)->whereFavoritableId($id)->whereUserId(Auth::id())->first();

            if (is_null($existing_like)) {
                Favorite::create([
                    'user_id'       => Auth::id(),
                    'favoritable_id'   => $id,
                    'favoritable_type' => Classrooms::class,
                ]);
            } else {
                if (is_null($existing_like->deleted_at)) {
                    $existing_like->delete();
                } else {
                    $existing_like->restore();
                }
            }
        }
        return back();
    }
}
