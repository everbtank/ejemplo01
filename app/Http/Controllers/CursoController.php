<?php

namespace App\Http\Controllers;

use App\Models\Cursos;
use App\Models\User;
use App\Models\Role;
use App\Models\Productos;
use App\Models\Docente_curso;
use App\Models\Docentes;
use App\Models\Productos_cursos;
use App\Models\Classrooms;
use App\Models\ClassroomsMembers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CursoController extends Controller
{
    
    
    public function diplomado(){
        return view('cursos.diplomados');
    }

    public function carrito(){
        return view('cursos.carrito');
    }

    public function infoCurso($id)
    {
        $infocursos = Productos::find($id);
        $cursos = Productos::orderBy('created_at','DESC')->paginate();
        $productos_cursos = Productos_cursos::orderBy('created_at','DESC')->paginate();
        
        $user = User::orderBy('created_at','DESC')->paginate();
        $role = Role::orderBy('created_at','DESC')->paginate();
      
        if ($infocursos) {
            return view('cursos.info', ['infocursos' => $infocursos,'cursos' => $cursos,'productos_cursos' => $productos_cursos]);
           
        }
         return redirect()->route('cursos');
    }

    

    public function searchCurso(Request $request)
    {

        $buscar = $request->search;

        if($buscar == ''){

            $cursos = Productos::orderBy('created_at','DESC')->paginate();
            $tipos="";

            //$table = Productos::where('title','LIKE','%'.$request->search.'%')->orWhere('description','LIKE','%'.$request->search.'%');
            switch ($request->sort) {
                case 'nuevos':
                    $cursos = Productos::orderBy('created_at', 'desc')->paginate(10);
                    break;
                case 'antiguos':
                    $cursos = Productos::orderBy('created_at', 'asc')->paginate(10);
                    break;
                case 'popular':
                    $cursos = Productos::orderBy('updated_at', 'desc')->paginate(10);
                    break;
                case 'diplomados':
                    $cursos = Productos::where('tipo_id', 1)->paginate(10);
                    $tipos="Diplomados";
                    break;
                case 'especializacion':
                    $cursos = Productos::where('tipo_id', 2)->paginate(10);
                    $tipos="Especializaciones";
                    break;
                case 'cursos':
                    $cursos = Productos::where('tipo_id', 3)->paginate(10);
                    $tipos="cursos";
                    break;
                case 'online':
                    $cursos = Productos::where('modalidad_id', 1)->paginate(10);
                    break;
                case 'presencial':
                    $cursos = Productos::where('modalidad_id', 2)->paginate(10);
                    break;
                case 'vivo':
                    $cursos = Productos::where('modalidad_id', 3)->paginate(10);
                    break;
                case 'arquitectura':
                    $cursos = Productos::where('categoria_id', 1)->paginate(10);
                    break;
                case 'civil':
                    $cursos = Productos::where('categoria_id', 2)->paginate(10);
                    break;
                case 'alfabetico':
                    $cursos = Productos::orderBy('title', 'asc')->paginate(10);
                        break;
                case 'destacados':
                    $cursos = Productos::orderBy('vistas', 'desc')->paginate(10);
                    break;
                case 'gratis':
                    $cursos = Productos::where('tipo_precio', 'gratis')->paginate(10);
                    break;
                default:
                    $cursos = Productos::orderBy('created_at', 'desc')->paginate(10);
                    break;
            }

        }else{
            $cursos = Productos::orderBy('created_at','DESC')->where('title','like','%'.$buscar.'%')->paginate();
        }

        return view('cursos.cursos',['cursos'=>$cursos,'tipos'=>$tipos]);
    }

    public function cursosinfo($id)
    {
        
        $classroom = Classrooms::find($id);
        if ($classroom) {
            if (Auth::user()->hasRole('admin')) {
                return view('profile.classroom', ['classroom' => $classroom]);
            }elseif (!empty($classroom->users()->where('user_email', '=', Auth::user()->email)->first())) {
                return view('profile.classroom', ['classroom' => $classroom]);
            }
        }
         return redirect()->route('classrooms');
    }

    public function downloadCurso(Request $request, $id, $uuid)
    {
        $table = Contenido::where([['id', $id], ['uuid', $uuid]])->firstOrFail();
        $file = $table->files()->first();
        if (Storage::disk('public')->exists($file->file_path)) {
            $table->addDownload();
            $table->save();
            return Storage::disk('public')->download($file->file_path, $file->file_name);
        }
        return abort(404);
    }

    
}
