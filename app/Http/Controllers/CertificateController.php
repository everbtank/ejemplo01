<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Certificate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\CertificateCollection;
use Illuminate\Support\Facades\DB;

class CertificateController extends Controller
{

    public function index(Request $request)
    {
        return [];
    }

    public function search(Request $request)
    {
        $request->validate([
            'by' => 'required'
        ], [
            'by.required' => 'Hay campos vacios en la busqueda.'
        ]);

        $search = $request->input('by');

        return Certificate::where('student_name', 'LIKE', '%' . $search . '%')
        ->orWhere('student_dni', 'LIKE', '%' . $search . '%')
        ->paginate(10)
        ->appends(['by' => $search]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'school' => 'required',
            'student_name' => 'required',
            'student_dni' => 'required',
            'course_code' => 'required',
            'course_name' => 'required',
            'course_end' => 'required|date|date_format:Y-m-d',
            'course_score' => 'required|numeric|min:0|max:20',
            'course_time' => 'required|numeric',
            'url_download' => 'string|nullable',
        ], [
            'required' => 'Hay campos requeridos sin completar.',
            'numeric' => 'Solo se permiten numeros.',
            'course_end.date' => 'Solo se permiten fechas exactas.',
            'url_download.string' => 'Solo cadena de texto esta permitido.',
            'course_score.min' => 'La nota minima permitido es :min.',
            'course_score.max' => 'La nota maxima permitido es :max.',
        ]);
        $table = Certificate::create($request->all());
        return $request->ajax() ? [
            'success' => true,
            'data' => $table
        ] : redirect()->to(url()->previous());
    }

    public function show(Request $request)
    {
        return Certificate::orderBy('created_at', 'desc')->paginate(10);
    }

    public function update(Request $request, $id)
    {
        $table = Certificate::findOrFail($request->id);
        $table->update($request->all());
        return $table;
    }

    public function destroy(Request $request, $id)
    {
        return Certificate::destroy($id);
    }
    
    public function buscador(Request $request)
    {   

        $valor=trim($request->get('school'));
        $texto=trim($request->get('search'));
        
        if($texto!="") {
        $certificados = DB::table('certificates')
        ->select('id','student_dni','student_name','course_name','course_code','course_time','course_score','url_download')
        ->where('student_dni','LIKE','%'.$texto.'%')
        ->where('school', '=',  $valor)
        ->orderBy('id','asc')
        ->paginate(10);
        }else{$certificados=null;}

        return view('certificado.certificado',compact('certificados'));
           
    }

    public function check(Request $request, $school = null)
    {
        $schools = [
            'ecca-peru' => ['title' => 'Ecca Peru', 'school' => 'Ecca Peru'],
            'cip-peru' => ['title' => 'Colegio de Ingenieros del Peru', 'school' => 'CIP'],
            'cype-peru' => ['title' => 'Cype Peru', 'school' => 'Cype Peru'],
            'default' => ['title' => 'Instituto Itcenco', 'school' => 'Instituto Itcenco']
        ];
        $cert = $schools[strtolower($school)] ?? $schools['default'];

        if ($request->has('search')) {
            $request->validate([
                'search' => 'required|numeric'
            ], [
                'search.required' => 'Verifica que el documento de identidad sea correcto.',
                'search.numeric' => 'El documento de identidad no es validó.'
            ]);

            $student = Certificate::where('student_dni', $request->search)->where('school', 'LIKE', '%'.$cert['school'].'%')->get();
            if ($student->isNotEmpty()) {
                return view('certificado.certificado')->with('certificates', $student)->with('student', $student->first())->with('title', $cert['title']);
            }
            return view('certificado.certificado')->withErrors(['search' => 'No se encontro ningun alumno con este dni.'])->with('title', $cert['title']);
        }
        return view('certificado.certificado')->with('title', $cert['title']);
    }
}
