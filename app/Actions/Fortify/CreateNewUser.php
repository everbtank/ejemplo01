<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'min:5', 'max:50'],
            'identify' => ['required', 'numeric', 'min:5'],
            'email' => [
                'required',
                'string',
                'email',
                'max:40',
                Rule::unique(User::class),
            ],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            
        ], $this->messages())->validate();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'identify' => $input['identify'],
        ]);
        $user->roles()->attach(Role::where('name', 'user')->first());
        return $user;
    }

    public function messages(){
        return [
            'required' => 'Este campo no puede estar vacio.',
            'min' => 'Debe tener como minimo :min caracteres.',
            'max' => 'Debe tener como maximo :max caracteres.',
            'numeric' => 'El documento de identidad solo permite numeros.',
            'email.required' => 'El correo electronico es necesario.',
            'email.unique' => 'El correo ya se encuentra registrado.',
            'password.confirmed' => 'Las contraseñas no coinciden.',
            'password.regex' => 'La contraseña debe tener numeros y letras.'
        ];
    }
}
