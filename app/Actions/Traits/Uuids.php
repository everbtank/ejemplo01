<?php
namespace App\Actions\Traits;

use Illuminate\Support\Str;

trait Uuids
{
   public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->uuid = (string) Str::orderedUuid()->getHex();
        });
    }
}