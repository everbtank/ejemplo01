<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Classrooms extends Model
{
    use HasFactory;


    protected  $table = 'classrooms';

    protected $fillable = ['title', 'description', 'photo_url', 'meet_url', 'zoom_url', 'mstm_url', 'class_start', 'class_time'];

    public function users()
    {
        return $this->hasMany(ClassroomsMembers::class, 'classroom_id');
    }

    public function posts()
    {
    	return $this->hasMany(ClassroomsPosts::class, 'classroom_id');
    }

    public function chats()
    {
        return $this->morphMany(Comments::class, 'commentable');
    }

    public function lessons()
    {
        return $this->hasMany(ClassroomsLessons::class, 'classroom_id');
    }

    public function postWithFiles()
    {
        return $this->posts()->with('files');
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favoritable')->whereDeletedAt(null);
    }

    public function getIsFavoriteAttribute()
    {
        $like = $this->favorites()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    public function recomendations($user_id, $ignore_id = null)
    {
        return ClassroomsMembers::recomend($user_id, $ignore_id)->get();
    }
}
