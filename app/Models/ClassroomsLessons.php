<?php

namespace App\Models;

use App\Models\Classrooms;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassroomsLessons extends Model
{
    use HasFactory;

    protected $fillable = ['classroom_id', 'lesson_name', 'lesson_url'];

    public function classroom()
    {
        return $this->belongsTo(Classrooms::class);
    }
}
