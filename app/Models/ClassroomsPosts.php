<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassroomsPosts extends Model
{
    use HasFactory;

    protected $fillable = ['classroom_id', 'title', 'body'];

    protected $with = ['files'];

    public function classroom()
    {
        return $this->belongsTo(Classrooms::class);
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'filestable');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $model->files->each(function($file){$file->delete();});
        });

    }
}
