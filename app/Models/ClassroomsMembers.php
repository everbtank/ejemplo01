<?php

namespace App\Models;

use App\Models\Classrooms;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassroomsMembers extends Model
{
    use HasFactory;

    protected $fillable = ['classroom_id', 'user_email'];

    public function classroom()
    {
        return $this->belongsTo(Classrooms::class);
    }

    public function scopeRecomend($query, $user_email, $ignore_id = null)
    {
        if ($ignore_id != null) {
            return $query->where('user_email', '=', $user_email)->where('classroom_id', '<>', $ignore_id);
        }
        return $query->where('user_email', '=', $user_email);
    }
}
