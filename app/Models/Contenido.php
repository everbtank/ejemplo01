<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'file', 'image', 'downloads'];

    public function favorites()
    {
    	return $this->morphMany(Favorites::class, 'favoritable');
    }

    public function likes()
    {
    	return $this->morphMany(Likes::class, 'likeable');
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'filestable');
    }

    public function addDownload()
    {
    	$this->downloads++;
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->uuid = (string) Str::orderedUuid()->getHex();
        });
        static::deleting(function ($model) {
            $model->files()->delete();
            Storage::disk('public')->delete($model->file);
            Storage::disk('public')->delete($model->image);
        });
    }
}
