<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model 
{
	use HasFactory;

	protected $fillable = ['school', 'student_name', 'student_dni', 'course_code', 'course_name', 'course_end', 'course_score', 'course_time', 'url_download'];

}