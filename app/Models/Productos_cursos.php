<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos_cursos extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'photo_url', 'meet_url', 'zoom_url', 'mstm_url', 'class_start', 'class_time'];
    public function curso()
    {
        return $this->belongsTo(Classrooms::class);
    }
}
