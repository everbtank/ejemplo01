<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Files extends Model
{
	use HasFactory;

	protected $fillable = ['uuid', 'file_name', 'file_type', 'file_path'];

	public function filestable()
	{
		return $this->morphTo();
	}

	protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->uuid = (string) Str::orderedUuid()->getHex();
        });
        static::deleting(function ($model) {
            if ($model->file_path) {
            	Storage::delete($model->file_path);
            }
        });
    }
}
