<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use HasFactory;

    protected $fillable = ['body', 'user_id'];

    protected $touches = ['user'];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function files()
    {
        return $this->morphMany(Files::class, 'filestable');
    }

    public function favorites()
    {
    	return $this->morphMany(Favorites::class, 'favoritable');
    }

    public function likes()
    {
    	return $this->morphMany(Likes::class, 'likeable');
    }
}
