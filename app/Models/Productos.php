<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'inicio', 'tipo', 'precio', 'photo_url', 'moneda', 'class_time'];
    public function curso()
    {
        return $this->belongsTo(Classrooms::class);
    }
}
