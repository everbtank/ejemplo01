<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CertificateController;
use App\Actions\Fortify\UpdateUserProfileInformation;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\AulaVirtualController;
use App\Http\Controllers\ContenidoController;
use App\Http\Controllers\NosotrosController;
use App\Http\Controllers\EstudianteController;
use App\Http\Controllers\EventoController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\PagosController;

use Laravel\Fortify\Features;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;
use Laravel\Fortify\Http\Controllers\ConfirmablePasswordController;
use Laravel\Fortify\Http\Controllers\ConfirmedPasswordStatusController;
use Laravel\Fortify\Http\Controllers\EmailVerificationNotificationController;
use Laravel\Fortify\Http\Controllers\EmailVerificationPromptController;
use Laravel\Fortify\Http\Controllers\NewPasswordController;
use Laravel\Fortify\Http\Controllers\PasswordController;
use Laravel\Fortify\Http\Controllers\PasswordResetLinkController;
use Laravel\Fortify\Http\Controllers\ProfileInformationController;
use Laravel\Fortify\Http\Controllers\RecoveryCodeController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;
use Laravel\Fortify\Http\Controllers\TwoFactorAuthenticatedSessionController;
use Laravel\Fortify\Http\Controllers\TwoFactorAuthenticationController;
use Laravel\Fortify\Http\Controllers\TwoFactorQrCodeController;
use Laravel\Fortify\Http\Controllers\VerifyEmailController;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/old-browser', [PageController::class, 'unsupported'])->name('unsupported');
Route::get('/', [PageController::class, 'home'])->name('home');
Route::get('/home', HomeController::class);
Route::get('recuperar',[AuthController::class,'recover']);

Route::get('alumno_cursos',[EstudianteController::class,'curso'])->name('alumno_virtual');
Route::get('alumno_perfil',[EstudianteController::class,'perfil'])->name('alumno_perfil');
Route::get('admin-home',[AdminController::class,'panel'])->name('admin-home');

Route::get('/admin-productos',[AdminController::class,'productos'])->name('admin-productos');
Route::post('/admin-productos',[AdminController::class,'storeProductos'])->name('storeproductos');
Route::get('/admin-productos/{id}',[AdminController::class,'editProductos'])->name('editproductos');
Route::post('/admin-productos/{id}',[AdminController::class,'putProductos'])->name('putproductos');
Route::delete('/admin-productos/{id}',[AdminController::class,'destroyProductos'])->name('destroyproductos');

Route::get('/admin-certificados',[AdminController::class,'certificado'])->name('admin-certificados');
Route::post('/admin-certificados',[AdminController::class,'storeCertificados'])->name('storecertificados');
Route::get('/admin-certificados/{id}',[AdminController::class,'editCertificado'])->name('editcertificados');
Route::put('/admin-certificados',[AdminController::class,'putCertificado'])->name('putcertificados');
Route::delete('/admin-certificados/{id}',[AdminController::class,'destroyCertificado'])->name('destroycertificados');

Route::get('/admin-aulavirtual',[AdminController::class,'createAulavirtual'])->name('admin-aulavirtual');
Route::post('/admin-aulavirtual',[AdminController::class,'storeAulavirtual'])->name('storeaulavirtual');
Route::get('/admin-aulavirtual/{id}',[AdminController::class,'editAulavirtual'])->name('editaulavirtual');
Route::put('/admin-aulavirtual',[AdminController::class,'putAulavirtual'])->name('putaulavirtual');
Route::delete('/admin-aulavirtual/{id}',[AdminController::class,'destroyAulavirtual'])->name('destroyaulavirtual');

Route::get('/admin-contenido',[AdminController::class,'contenido'])->name('admin-contenidos');
Route::post('/admin-contenido',[AdminController::class,'storeContenido'])->name('storecontenidos');
Route::get('/admin-contenido/{id}',[AdminController::class,'editContenido'])->name('editcontenidos');
Route::put('/admin-contenido',[AdminController::class,'putContenido'])->name('putcontenidos');
Route::delete('/admin-contenido/{id}',[AdminController::class,'destroyContenido'])->name('destroycontenidos');

Route::get('/admin-cursos',[AdminController::class,'cursos'])->name('admin-cursos');
Route::post('/admin-curso',[AdminController::class,'storeCursos'])->name('storecursos');
Route::get('/admin-curso/{id}',[AdminController::class,'editCursos'])->name('editcursos');
Route::put('/admin-curso',[AdminController::class,'putCursos'])->name('putcursos');
Route::delete('/admin-curso/{id}',[AdminController::class,'destroyCursos'])->name('destroycursos');

Route::get('/admin-clases',[AdminController::class,'clases'])->name('admin-clases');
Route::get('/admin-alumnos',[AdminController::class,'alumnos'])->name('admin-alumnos');

Route::get('/admin-docentes',[AdminController::class,'docentes'])->name('admin-docentes');
Route::post('/admin-docentes',[AdminController::class,'storeDocente'])->name('createdocentes');
Route::get('/admin-docente/{id}',[AdminController::class,'editDocente'])->name('editdocentes');
Route::put('/admin-docente',[AdminController::class,'putDocente'])->name('putdocentes');
Route::delete('/admin-docente/{id}',[AdminController::class,'destroyDocente'])->name('destroydocentes');

Route::get('/contenido', [ContenidoController::class, 'searchContent'])->name('contenido');
Route::get('/contenido/{id}', [ContenidoController::class, 'infoContenido'])->name('infocontenido');
Route::get('/contenido/{id}/{uuid}', [ContenidoController::class, 'downloadContent'])->name('content-download');

Route::get('/infodocente',[CursoController::class,'infocurso'])->name('infocurso');
Route::get('/carrito',[CursoController::class,'carrito'])->name('carrito');
Route::get('/notificacion',[PanelController::class,'notificacion'])->name('notificacion');
Route::get('/eventos',[EventoController::class,'create'])->name('evento');
Route::get('/blog',[BlogController::class,'create'])->name('blog');
Route::get('/proceso_pago',[PagosController::class,'proceso_pago'])->name('proceso_pago');
Route::get('/nosotros',[HomeController::class,'nosotros'])->name('nosotros');
Route::get('/aliados',[HomeController::class,'aliados'])->name('aliados');
Route::get('/contacto',[HomeController::class,'contacto'])->name('contacto');


Route::get('/aulavirtual1', [AulaVirtualController::class, 'classrooms'])->name('aulavirtual');
Route::get('/aulavirtual1/{id}', [AulaVirtualController::class, 'classroom'])->name('aulavirtual');
Route::get('/alumno_cursos', [EstudianteController::class, 'cursos_estudiantes'])->name('estudiante');
Route::get('/alumno_cursos/{id}', [EstudianteController::class, 'cursos_estudiante'])->name('estudiante');

Route::get('/aulavirtual', [ProfileController::class, 'classrooms'])->name('classrooms');
Route::get('/aulavirtual/{id}', [ProfileController::class, 'classroom'])->name('classroom');
Route::post('/aulavirtual/{id}/create/message', [ProfileController::class, 'createMessage'])->name('room-create-message');
Route::post('/aulavirtual/{id}/remove/message', [ProfileController::class, 'removeMessage'])->name('room-remove-message');
Route::post('/aulavirtual/{id}/favorite', [ProfileController::class, 'favoriteClassroom'])->name('room-favorite');

Route::get('/cursos', [CursoController::class, 'searchCurso'])->name('curso');
Route::get('/cursos/{id}', [CursoController::class, 'infoCurso'])->name('cursos');
Route::get('/consulta-certificados/{school?}', [CertificateController::class, 'buscador'])->name('certification');
Route::get('download/{id}/{uuid}', [PageController::class, 'download']);

Route::get('/diplomado-bim', [PageController::class, 'diplomado'])->name('diplomado-bim');
Route::get('/diplomado-inscripcion', [PageController::class, 'diplomatLiveSuscription'])->name('diplomado-suscription');
Route::post('/diplomado-inscripcion', [PageController::class, 'diplomatSuscription']);

/*
Route::get('/privacidad', [PageController::class, 'privacy'])->name('privacy');
Route::get('/contacto', [PageController::class, 'contact'])->name('contact');
Route::post('/contacto', [PageController::class, 'createContact'])->name('contact');
Route::get('/afiliados', [PageController::class, 'affiliate'])->name('affiliate');
Route::get('/nosotros', [PageController::class, 'about'])->name('about');
Route::get('/diplomados', [PageController::class, 'diplomados'])->name('diplomados');
Route::get('/diplomado-bim', [PageController::class, 'diplomado'])->name('diplomado-bim');
Route::get('/diplomado-inscripcion', [PageController::class, 'diplomatLiveSuscription'])->name('diplomado-suscription');
Route::post('/diplomado-inscripcion', [PageController::class, 'diplomatSuscription']);

*/

Route::group(['prefix' => 'perfil'], function(){
    Route::get('/', [ProfileController::class, 'profile'])->name('profile');
    Route::get('/actualizar-informacion', [ProfileController::class, 'updateAccount'])->name('profile-account');
    Route::get('/cambiar-contraseña', [ProfileController::class, 'updatePassword'])->name('profile-password');
});

Route::group(['prefix' => 'panel', 'middleware' => ['web', 'auth', 'role:admin']], function(){
    Route::get('/', [PageController::class, 'panel'])->name('panel');
    Route::get('certificados', [PageController::class, 'certificate'])->name('panel-certificate');
    Route::get('aulavirtual', [PageController::class, 'classroom'])->name('panel-classroom');
    Route::group(['prefix' => 'classroom', 'middleware' => ['api']], function(){
        Route::get('/', [AdminController::class, 'getClassroom']);
        Route::post('/', [AdminController::class, 'createClassroom']);
        Route::post('/{id}', [AdminController::class, 'updateClassroom']);
        Route::delete('/{id}', [AdminController::class, 'removeClassroom']);
        Route::post('/{id}/create/post', [AdminController::class, 'createPost'])->name('room-create-post');
        Route::post('/{id}/remove/post', [AdminController::class, 'removePost'])->name('room-remove-post');
    });
    Route::group(['prefix' => 'certificado', 'middleware' => ['api', 'ajax']], function () {
        Route::get('/', [CertificateController::class, 'show']);
        Route::get('/search', [CertificateController::class, 'search']);
        Route::post('/', [CertificateController::class, 'store']);
        Route::put('/{id}', [CertificateController::class, 'update']);
        Route::delete('/{id}', [CertificateController::class, 'destroy']);
    });
    Route::group(['prefix' => 'content'], function () {
        Route::post('/', [AdminController::class, 'createContent'])->name('content-create');
        Route::delete('/{id}', [AdminController::class, 'removeContent'])->name('content-remove');
    });
});


Route::group(['middleware' => config('fortify.middleware', ['web'])], function () {
    // Authentication...
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])->middleware(['guest'])->name('login');

    $limiter = config('fortify.limiters.login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])->middleware(array_filter([
            'guest',
            $limiter ? 'throttle:'.$limiter : null,
        ]));

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

    // Password Reset...
    if (Features::enabled(Features::resetPasswords())) {
        Route::get('/forgot', [PasswordResetLinkController::class, 'create'])->middleware(['guest'])->name('forgot');
        Route::post('/forgot', [PasswordResetLinkController::class, 'store'])->middleware(['guest'])->name('forgot.request');
        Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])->middleware(['guest'])->name('password.reset');
        Route::post('/reset-password', [NewPasswordController::class, 'store'])->middleware(['guest'])->name('password.update');
    }

    // Registration...
    if (Features::enabled(Features::registration())) {
        Route::get('/registro', [RegisteredUserController::class, 'create'])->middleware(['guest'])->name('register');
        Route::post('/registro', [RegisteredUserController::class, 'store'])->middleware(['guest']);
    }

    // Email Verification...
    if (Features::enabled(Features::emailVerification())) {
        Route::get('/email/verify', [EmailVerificationPromptController::class, '__invoke'])->middleware(['auth'])->name('verification.notice');
        Route::get('/email/verify/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->middleware(['auth', 'signed', 'throttle:6,1'])->name('verification.verify');

        Route::post('/email/verification', [EmailVerificationNotificationController::class, 'store'])->middleware(['auth', 'throttle:6,1'])->name('verification.send');
    } 

    // Profile Information...
    if (Features::enabled(Features::updateProfileInformation())) {
        Route::put('/perfil/update', [UpdateUserProfileInformation::class, 'update'])->middleware(['auth'])->name('user-profile-information.update');
    }

    // Passwords...
    if (Features::enabled(Features::updatePasswords())) {
        Route::put('/perfil/update/password', [PasswordController::class, 'update'])
            ->middleware(['auth'])
            ->name('user-password.update');
    }

    // Password Confirmation...
    Route::get('/perfil/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware(['auth'])
        ->name('password.confirm');

    Route::post('/perfil/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware(['auth']);

    Route::get('/perfil/confirmed-password', [ConfirmedPasswordStatusController::class, 'show'])
        ->middleware(['auth'])
        ->name('password.confirmation');

    // Two Factor Authentication...
    if (Features::enabled(Features::twoFactorAuthentication())) {
        Route::get('/two-factor-challenge', [TwoFactorAuthenticatedSessionController::class, 'create'])
            ->middleware(['guest'])
            ->name('two-factor.login');

        Route::post('/two-factor-challenge', [TwoFactorAuthenticatedSessionController::class, 'store'])
            ->middleware(['guest']);

        $twoFactorMiddleware = Features::optionEnabled(Features::twoFactorAuthentication(), 'confirmPassword')
            ? ['auth', 'password.confirm']
            : ['auth'];

        Route::post('/user/two-factor-authentication', [TwoFactorAuthenticationController::class, 'store'])
            ->middleware($twoFactorMiddleware);

        Route::delete('/user/two-factor-authentication', [TwoFactorAuthenticationController::class, 'destroy'])
            ->middleware($twoFactorMiddleware);

        Route::get('/user/two-factor-qr-code', [TwoFactorQrCodeController::class, 'show'])
            ->middleware($twoFactorMiddleware);

        Route::get('/user/two-factor-recovery-codes', [RecoveryCodeController::class, 'index'])
            ->middleware($twoFactorMiddleware);

        Route::post('/user/two-factor-recovery-codes', [RecoveryCodeController::class, 'store'])
            ->middleware($twoFactorMiddleware);
    }
});


Route::get('oauth/google', [SocialController::class, 'redirectToGoogle'])->name('google');
Route::get('oauth/signin/google', [SocialController::class, 'handleGoogleCallback']);
Route::get('oauth/facebook', [SocialController::class, 'redirectToFacebook'])->name('facebook');
Route::get('oauth/signin/facebook', [SocialController::class, 'handleFacebookCallback']);
