<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_cursos', function (Blueprint $table) {
           
            $table->id();
            $table->unsignedBigInteger('productos_id');
            $table->string('title');
            $table->string('description');
            $table->string('imagen')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_name')->nullable();
            $table->date('class_start')->nullable();
            $table->date('class_end')->nullable();
            $table->integer('downloads')->default(0);
            $table->string('links')->nullable();
            $table->string('zoom')->nullable();
            $table->string('meet')->nullable();
            $table->foreign('productos_id')
            ->references('id')
            ->on('productos')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_cursos');
    }
}
