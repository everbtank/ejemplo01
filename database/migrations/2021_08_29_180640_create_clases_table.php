<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('productos_curso_id');
            $table->foreign('productos_curso_id')
            ->references('id')
            ->on('productos_cursos')
            ->onDelete('cascade');
            $table->string('titulo');
            $table->string('description');
            $table->string('materias')->nullable();
            $table->string('clases_grabadas')->nullable();
            $table->string('tareas')->nullable();
            $table->json('links')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_name')->nullable();
            $table->date('class_start')->nullable();
            $table->date('class_end')->nullable();
            $table->integer('downloads')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clases');
    }
}
