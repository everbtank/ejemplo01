<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('description');
            $table->string('photo_url')->nullable();
            $table->string('meet_url')->nullable();
            $table->string('zoom_url')->nullable();
            $table->string('mstm_url')->nullable();
            $table->date('class_start')->nullable();
            $table->date('class_end')->nullable();
            $table->string('class_time')->nullable();
            /*$table->unsignedBigInteger('cursos_id');
            $table->unsignedBigInteger('docente_id');*/
            /*$table->foreign('cursos_id')
                ->references('id')
                ->on('cursos')
                ->onDelete('cascade');
            $table->foreign('docente_id')
                ->references('id')
                ->on('docentes')
                ->onDelete('cascade');*/

            $table->timestamps();

        });

        Schema::create('classrooms_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('classroom_id');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->string('lesson_name')->nullable();
            $table->string('lesson_url')->nullable();
            $table->timestamps();
        });

        Schema::create('classrooms_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('classroom_id');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->string('user_email');
            $table->timestamps();
        });

        Schema::create('classrooms_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('classroom_id');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->text('title');
            $table->text('body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
        Schema::dropIfExists('classrooms_members');
        Schema::dropIfExists('classrooms_lessons');
        Schema::dropIfExists('classrooms_posts');
    }
}
