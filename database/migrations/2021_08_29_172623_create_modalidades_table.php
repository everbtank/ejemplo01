<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modalidades', function (Blueprint $table) {
            $table->id();
            $table->string('modalidad');
            $table->integer('estado');
            $table->integer('oferta')->nullable();
            $table->integer('precio_oferta')->nullable();
            $table->integer('desc_oferta')->nullable();
            $table->date('oferta_start')->nullable();
            $table->date('oferta_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modalidades');
    }
}
