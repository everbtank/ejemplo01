<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categoria_id');
            $table->unsignedBigInteger('nivel_id');
            $table->unsignedBigInteger('modalidad_id');
            $table->unsignedBigInteger('tipo_id');
            $table->foreign('categoria_id')
            ->references('id')
            ->on('productos')
            ->onDelete('cascade');
            $table->foreign('nivel_id')
            ->references('id')
            ->on('niveles')
            ->onDelete('cascade');
            $table->foreign('modalidad_id')
            ->references('id')
            ->on('modalidades')
            ->onDelete('cascade');
            $table->foreign('tipo_id')
            ->references('id')
            ->on('tipos')
            ->onDelete('cascade');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('photo_url')->nullable();
            $table->date('inicio')->nullable();
            $table->string('duracion');
            $table->float('precio')->nullable();
            $table->string('moneda');
            $table->string('tipo_precio')->nullable();
            $table->string('vistas')->nullable();
            $table->integer('calificacion')->nullable();
            $table->string('diplomas')->nullable();
            $table->string('contacto')->nullable();
            $table->float('descuento')->nullable();
            $table->float('estudiantes')->nullable();
            $table->string('file_name')->nullable();
            $table->string('file_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
