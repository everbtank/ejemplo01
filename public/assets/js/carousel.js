$(document).ready(function(){
    jQuery.fn.exists = function(){return this.length>0;}
    if ($('#profesionals').exists()) {

        $('#profesionals').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            navText : ["",""],
            rewindNav : true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                800:{
                    items:2,
                },
                1100:{
                    items:3
                }
            }
        });

    }


    if ($('#testimonials').exists()) {

        $('#testimonials').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            margin:24,
            navText : ["",""],
            rewindNav : true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:true
                },
                800:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:4,
                    nav:true
                }
            }
        });
    }


    if ($('#aliados').exists()) {

        $('#aliados').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            margin:24,
            navText : ["",""],
            rewindNav : true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                },
                800:{
                    items:3,
                },
                1000:{
                    items:4,
                    nav:true
                }
            }
        });
    }


    if ($('#aliados-modoule').exists()) {

        $('#aliados-modoule').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            margin:24,
            navText : ["",""],
            rewindNav : true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                750:{
                    items:2,
                    nav:true
                },
                1000:{
                    items:3,
                    nav:true
                }
            }
        });
    }


    if ($('#contenido-slider').exists()) {

        $('#contenido-slider').owlCarousel({
            responsiveClass:true,
            loop:true,
            autoplay:true,
            autoplayTimeout:5000,
            margin: 10,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                }
            }
        });
    }

    if ($('#info-content-slide').exists()) {

        $('#info-content-slide').owlCarousel({
            responsiveClass:true,
            dots:false,
            responsive:{
                0:{
                    items:1,
                    nav:true
                }
            }
        });
    }

    if ($('#slide-recomended-info').exists()) {

        $('#slide-recomended-info').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            margin:24,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:true
                },
                800:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:4,
                    nav:true
                }
            }
        });
    }

    if ($('#slide-course-info').exists()) {

        $('#slide-course-info').owlCarousel({
            responsiveClass:true,
            dots:false,
            slideBy:4,
            margin: 16,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:true
                },
                800:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:4,
                    nav:true
                }
            }
        });
    }

});