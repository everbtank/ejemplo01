/* variables */
const header_contain_desktop = document.querySelector('#header-height');
const header_contain_mobile = document.querySelector('#header-height-mobile');
const main = document.querySelector('#main');

const header_height_desktop= header_contain_desktop.clientHeight;
const header_height_mobile= header_contain_mobile.clientHeight;

marginTop();


/* Header desktop - header mobile */

const header_desktop = document.querySelector('.header-desktop');
const header_mobile = document.querySelector('.header-mobile');
const checkbox_sidebar = document.querySelector('#sidebar-mobile');
const icon_close = document.querySelector('#icon-close');
const icon_menu = document.querySelector('#icon-menu');
const container_float = document.querySelector('.container-float');

//table responsive

const table_responsive = document.querySelector('#table-responsive');


window.addEventListener('resize',function(){
    if(window.innerWidth < 860){
        if(table_responsive){
            console.log(window.innerWidth);
            table_responsive.style.width = `${window.innerWidth - 40}px `;
            table_responsive.style.height = `320px`;
        }
    }else{
        if(table_responsive){
            table_responsive.style.width = `100%`;
            table_responsive.style.height = `initial`;
        }
    }

});


window.addEventListener('resize',function(){

    //mostart header mobile o desktop
    headerShow();

    //margin top si es mobile o desktop
    marginTop();});


headerShow();

function headerShow(){
    if(window.innerWidth < 1180){
        header_desktop.classList.add('d-none');
        header_mobile.classList.remove('d-none');
    }else{
        header_desktop.classList.remove('d-none');
        header_mobile.classList.add('d-none');
    }
}

function marginTop(){
    if(window.innerWidth  < 1180){
        main.style.marginTop = `${header_height_mobile}px`
    }else{
        main.style.marginTop = `${header_height_desktop}px`
    }
}

checkbox_sidebar.addEventListener('change',function(){
    if(checkbox_sidebar.checked){
        icon_close.classList.remove('d-none');
        icon_menu.classList.add('d-none');
        container_float.classList.remove('d-none');
    }else{
        icon_close.classList.add('d-none');
        icon_menu.classList.remove('d-none');
        container_float.classList.add('d-none');
    }
});

//filtro
const open_container_filter = document.querySelector('#open-container-filter');
const btn_cancle_filter_content = document.querySelector('#btn-cancle-filter-content');
const contenedor_filtro_producto = document.querySelector('#contenedor-filtro-producto');

if(open_container_filter){

    open_container_filter.addEventListener('click',function(){
        contenedor_filtro_producto.classList.remove('d-none');
    });
}

if(btn_cancle_filter_content){
    btn_cancle_filter_content.addEventListener('click',function(){
        contenedor_filtro_producto.classList.add('d-none');
    });
}

if(contenedor_filtro_producto){
    if(window.innerWidth < 1100){
        contenedor_filtro_producto.classList.add('d-none');
    }else{
        contenedor_filtro_producto.classList.remove('d-none');
    }
}
    
//dropdown active section course info

const listCourseInfo = document.querySelectorAll('#list_content_theme_course .list-content .link-active');

listCourseInfo.forEach(item=>{
    item.addEventListener('click',event =>{
        event.preventDefault();
        let link_active = item.nextElementSibling;
        if(link_active){
            link_active.classList.toggle('active');
        }
        //console.log(event);
    });
});


//abrir ventana modal
const modal_form_aula_virtual = document.querySelector('#modal_aula_virtual');
const open_form_aula_virtual = document.querySelector('#open-aula-virtual-modal');
const close_form_aula_virtual = document.querySelector('#close_form_aula_virtual');

if(open_form_aula_virtual){

    open_form_aula_virtual.addEventListener('click',function(e){
        e.preventDefault();
        abrirModal(modal_form_aula_virtual);
    });
}

if(close_form_aula_virtual){
    
    close_form_aula_virtual.addEventListener('click',function(e){
        e.preventDefault();
        closeModal(modal_form_aula_virtual);
    });
    
}

function closeModal(id_modal){
    id_modal.classList.add('close-modal');
    id_modal.classList.remove('show-modal');
}

function abrirModal(id_modal){
    id_modal.classList.remove('close-modal');
    id_modal.classList.add('show-modal');
}